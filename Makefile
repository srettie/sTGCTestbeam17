include common.mk

all:
	cd src && $(MAKE) -f Makefile.dict
	$(MAKE) -f Makefile.build all
	
.PHONY=:utils
utils: $(CPPFILES)
	$(foreach cppfile,$(CPPFILES), eval $$($(CXX) $(CXXFLAGS)  $(LDFLAGS) $(LIBS) -lsTGCTestBeamTools -Llib $(cppfile) -o $(cppfile:.cpp=.exe))  )	
	
clean:
	rm -vf $(OBJDIR)/*.o
	rm -vf $(OBJDIR)/*.d
	rm lib/*
	rm util/*.exe
purge:	
	@if [ -d lib ];then \
		rm -r lib; \
	fi	
	@if [ -d obj ];then \
	rm -r obj; \
	fi
	$(foreach f, src/_Dictionaries*, $(shell rm $f))
