NAME="__sTGC Test Beam Tools 2018__"

XERCES=external/xerces-c-3.2.1/install
CLHEP=external/2.4.1.0/CLHEP/install/


CXXFLAGS=-I$(XERCES)/include -I$(CLHEP) -Isrc $(shell root-config --cflags) -fPIC -std=c++14 -Wall
LDFLAGS=-L$(XERCES)/lib -L$(CLHEP)/lib  $(shell root-config --glibs)
LIBS=-lxerces-c -lCLHEP-Evaluator-2.4.1.0 
CXX = g++ 


CPPFILES=$(wildcard util/*.cpp)


OBJDIR=obj
LIBDIR=lib
EXEDIR=util
