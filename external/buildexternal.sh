#!/bin/bash
currdir=$PWD

tar xvzf clhep-2.4.1.0.tgz
tar  xvzf xerces-c-3.2.1.tar.gz

cd xerces-c-3.2.1;mkdir install
./configure --prefix=$PWD/install
make -j4;make install
cd $currdir
cd 2.4.1.0/CLHEP;mkdir install;cd install;cmake ..;make -j4
cd $currdir
