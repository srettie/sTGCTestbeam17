/*
 * Reco_MC_comparator.cpp
 *
 *  Created on: Oct 3, 2017
 *      Author: lucamoleri
 */


#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

#include "TH1F.h"

#define nEvents 20000

void TestBeam_tracker_MC_comparator(){

	TH1F * h_track_reconstruction_efficiency = new TH1F("h_track_reconstruction_efficiency", "h_track_reconstruction_efficiency", 12, -6, 6);

	std::string in_log_fname = "../data/MC_data_angle0_noise0_mult10_active_log.txt";
	std::string out_log_fname = "../results/MC_data_angle0_noise0_mult10_active_out_log.txt";
	std::string comparator_log_fname = "../results/MC_data_angle0_noise0_mult10_active_comp_log.txt";

	std::vector<int> n_tracks_real;
	std::vector<int> n_tracks_reco;
	std::vector<double> track_intercept_real;
	std::vector<double> track_intercept_reco;

	//std::vector<std::vector<double>> track_intercept_real_vec;
	//std::vector<std::vector<double>> track_intercept_reco_vec;


	std::ifstream ifs_in(in_log_fname);
	std::ifstream ifs_out(out_log_fname);
	std::ofstream ifs_comp(comparator_log_fname);


		std::string line;

	    if (!ifs_in.is_open())
	    {
	    		std::cout << "Failed to open the input file - closing" << std::endl;
	    }


	    int i_event = -1;
	    int i_n_tracks = -1;
	    double i_track_intercept = -1;

	    int i_currentEvent = -1;

		while (!ifs_in.eof())
		 {
			 std::getline (ifs_in,line);

			 //std::cout << "line " << line<< std::endl;

			 const char *c_line = line.c_str();
			 sscanf(c_line, "%i %i %lf", &i_event, &i_n_tracks, &i_track_intercept);
			 track_intercept_real.push_back(i_track_intercept);

			 if (i_currentEvent == -1)
				 i_currentEvent = i_event;
			 	 n_tracks_real.push_back(i_n_tracks);
			 if (i_currentEvent != i_event)
			 {

				 n_tracks_real.push_back(i_n_tracks);
				 i_currentEvent = i_event;

				 //cout<< "current_event: " << i_currentEvent << " n_tracks_real: " << n_tracks_real[i_currentEvent-1] << endl;


			 }
		 }

		ifs_in.close();

		std::cout << "input file closed " << std::endl;

		if (!ifs_out.is_open())
			    {
			    		std::cout << "Failed to open the output file - closing" << std::endl;
			    }

		std::getline (ifs_out,line); // to remove title

			    i_event = -1;
			    i_n_tracks = -1;
			    i_track_intercept = -1;

			    i_currentEvent = -1;

				for(int i_events = 0; i_events < nEvents; i_events++)
				 {
					 if(!ifs_out.eof()) {
						 std::getline (ifs_out,line);


						// std::cout << "line " << line<< std::endl;

						 const char *c_line = line.c_str();
						 sscanf(c_line, "%i %i %lf", &i_event, &i_n_tracks, &i_track_intercept);
						 track_intercept_reco.push_back(i_track_intercept);


						 if (i_currentEvent == -1){
							 i_currentEvent = i_event;
						 	 n_tracks_reco.push_back(i_n_tracks);
						 	 //cout<< "n_tracks " << n_tracks_reco[0] <<endl;
						 }



						//cout<< " i_event  " << i_event <<endl;

						 	 if (i_currentEvent != i_event)
						 	 {

						 		 if(i_currentEvent+1 != i_event){

						 			 //cout<< "current event: " << i_currentEvent << " i_event " << i_event << endl;
						 			 int gap = i_event - i_currentEvent;

						 			 for(int i_gap = 0; i_gap< gap-1; i_gap++){
						 				 n_tracks_reco.push_back(0);
						 				 //cout<< "current_event: " << i_currentEvent+i_gap+1 << " n_tracks_reco: " << n_tracks_reco[i_currentEvent+i_gap-1] << endl;
						 			 }
						 			 i_currentEvent = i_event;
						 			 n_tracks_reco.push_back(i_n_tracks);

						 		 }else{
						 			 i_currentEvent = i_event;
						 			 n_tracks_reco.push_back(i_n_tracks);
						 			 //cout<< "current_event: " << i_currentEvent << " n_tracks_reco: " << n_tracks_reco[i_currentEvent-1] << endl;
						 		 }
						 	 }
					 }else{
						 n_tracks_reco.push_back(0);
					 }

				 }

		ifs_in.close();

		std::cout << "output file closed " << std::endl;

		for(int i_event = 0; i_event < nEvents; i_event++){

			h_track_reconstruction_efficiency->Fill(n_tracks_reco[i_event] - n_tracks_real[i_event]);

			//cout<< "event " << i_event << " n_tracks_real: " << n_tracks_real[i_event] << " n_tracks_reco: " << n_tracks_reco[i_event] << endl;

			//TODO implement intercept comparison

		 }

		h_track_reconstruction_efficiency->GetXaxis()->SetName("tracks_reco - tracks_real");
		h_track_reconstruction_efficiency->Draw();

}


