import ROOT
import sys
import os
import ctypes

ROOT.gROOT.SetBatch(1)

debug = False

doFastPlots = False

doPads = True
doStrips = True

singleLayer = 1

singlePadLog = 'max_pad_eff.txt'

max_pad = {}

pdo_rebin = 16
tdo_rebin = 4

bad_runs = [
    'run_181027_mu_2900V_At2-2_NikScint',
    'run_181027_mu_2900V_At2-2_extScint',#??
    'run_181027_mu_3000V_At2-2_extScint',#??
    'run_181027_mu_2900V_noSource_New181027OptThreshold'
]

def calc_muon_S(run_name):
    if run_name in bad_runs:
        return
    if 'noBeam' in run_name:
        return
    if '_NeighborOn' in run_name:
        return
    if '_padToT' in run_name:
        return
    if '_3aBits' in run_name:
        return
    if '_TailCanc' in run_name:
        return
    if 'extScint' in run_name:
        return

    print('Looking at run:', run_name)
    photonFile = ROOT.TFile.Open('results/PhotonBackground/out_'+run_name+'_decoded.root')
    beamFile = ROOT.TFile.Open('results/IsolateStripsClusters/out_'+run_name+'_decoded.root')
    
    h = beamFile.Get('h_strips_hitsProfile_layer1')
    
    width = 20
    beamMin = 260
    sidebandMin = 298
    nTot    = h.Integral(h.FindBin(beamMin),h.FindBin(beamMin+width))
    nPhoton = h.Integral(h.FindBin(sidebandMin),h.FindBin(sidebandMin+width))
    sTot = beamFile.Get('h_strips_stripsPerCluster_layer1').GetMean()
    sPhoton = photonFile.Get('h_strips_stripsPerCluster_layer1').GetMean()

    if debug:
        print('Entries:', h.GetEntries())
        print('Entries in beam:', nTot)
        print('Entries in sideband:', nPhoton)
        print('In beam strips multiplicity:', sTot)
        print('Photon strip multiplicity:', sPhoton)

    sMu = (nTot*sTot - nPhoton*sPhoton)/(nTot - nPhoton)

    if debug:
        print('Entries:', h.GetEntries())
        print('Entries in beam:', nTot)
        print('Entries in sideband:', nPhoton)
        print('In beam strips multiplicity:', sTot)
        print('Photon strip multiplicity:', sPhoton)
        print('Muon strip multiplicity:', sMu)


    return sMu
    
def make_plots(filename,last=True):
    outfilename = 'plots_'+filename.split('/')[-1].replace('.root','')
    f = ROOT.TFile.Open(filename)
    keys = f.GetListOfKeys()
    histograms = {}
    tot_events = -1
    
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextColor(ROOT.kBlack)

    resdir = 'plots/'
    # Get histograms from file
    for i, k in enumerate(keys):
        k_name = k.GetName()
        hist = f.Get(k_name)
        k_class = hist.IsA()
        classes = [ROOT.TH1F.Class(), ROOT.TH2F.Class()]
        # Only consider histograms for now
        if k_class not in classes:
            if debug:
                print('Unable to handle ' + k_class.GetName() + ', skipping!')
            continue
        # Don't consider the offset plots for now
        if '_offset_' in k_name:
            continue

        histograms[k_name] = hist

        if debug:
            print(k_name)

        if k_name == 'h_pads_nFiringLayers':
            tot_events = histograms[k_name].GetEntries()

    # Now plot stuff much more easily
    cnv_singleLayer = ROOT.TCanvas()
    cnv_singleLayer.SetLeftMargin(0.12)

    cnv = ROOT.TCanvas()
    cnv.SetLeftMargin(0.12)

    if doPads:
        # Number of pad layers fired
        histograms['h_pads_nFiringLayers'].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        # Number of hits per pad layer
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_pads_nHits_layer'+str(layer)].Draw()
            num = histograms['h_pads_nHits_layer'+str(layer)].Integral(histograms['h_pads_nHits_layer'+str(layer)].FindBin(1), histograms['h_pads_nHits_layer'+str(layer)].GetNbinsX())
            denom = histograms['h_pads_nHits_layer'+str(layer)].Integral()
            try:
                eff = 100.0 * num / denom
            except ZeroDivisionError:
                eff = 0
            print('Layer '+ str(layer) + ' pad efficiency is ' + str(eff) + '%')
            latex.DrawLatex(0.45, 0.8, '#varepsilon = {0:.2f}%'.format(eff))
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_pads_nHits_layer'+str(singleLayer)].Draw()
            num = histograms['h_pads_nHits_layer'+str(singleLayer)].Integral(histograms['h_pads_nHits_layer'+str(singleLayer)].FindBin(1), histograms['h_pads_nHits_layer'+str(singleLayer)].GetNbinsX())
            denom = histograms['h_pads_nHits_layer'+str(singleLayer)].Integral()
            try:
                eff = 100.0 * num / denom
            except ZeroDivisionError:
                eff = 0
            print('Layer '+ str(singleLayer) + ' pad efficiency is ' + str(eff) + '%')
            latex.DrawLatex(0.45, 0.8, '#varepsilon = {0:.2f}%'.format(eff))
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')
        

        # PDO per pad layer
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_pads_pdo_layer'+str(layer)].Rebin(pdo_rebin)
            histograms['h_pads_pdo_layer'+str(layer)].Draw()
            histograms['h_pads_pdo_layer'+str(layer)].Fit('landau','','',histograms['h_pads_pdo_layer'+str(layer)].GetBinCenter(histograms['h_pads_pdo_layer'+str(layer)].FindFirstBinAbove()),histograms['h_pads_pdo_layer'+str(layer)].GetBinCenter(histograms['h_pads_pdo_layer'+str(layer)].GetNbinsX()-1))
            ROOT.gStyle.SetOptFit(111)
            histograms['h_pads_pdo_layer'+str(layer)].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_pads_pdo_layer'+str(singleLayer)].Draw()
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

        # Calibrated TDO per pad layer
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_pads_tdo_calibrated_layer'+str(layer)].Rebin(tdo_rebin)
            histograms['h_pads_tdo_calibrated_layer'+str(layer)].Draw()
            mean = histograms['h_pads_tdo_calibrated_layer'+str(layer)].GetMean()
            rms = histograms['h_pads_tdo_calibrated_layer'+str(layer)].GetRMS()
            
            histograms['h_pads_tdo_calibrated_layer'+str(layer)].Fit('gaus','','',mean-3*rms,mean+3*rms)
            ROOT.gStyle.SetOptFit(111)
            histograms['h_pads_tdo_calibrated_layer'+str(layer)].Draw()
            if histograms['h_pads_tdo_calibrated_layer'+str(layer)].GetFunction('gaus'):
                sigma = histograms['h_pads_tdo_calibrated_layer'+str(layer)].GetFunction('gaus').GetParameter(2)
                latex.DrawLatex(0.2, 0.4, '#sigma = {0:.2f} ns'.format(sigma))
            
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_pads_tdo_calibrated_layer'+str(singleLayer)].Draw()
            if histograms['h_pads_tdo_calibrated_layer'+str(singleLayer)].GetFunction('gaus'):
                sigma = histograms['h_pads_tdo_calibrated_layer'+str(singleLayer)].GetFunction('gaus').GetParameter(2)
                latex.DrawLatex(0.2, 0.4, '#sigma = {0:.2f} ns'.format(sigma))
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

        # Relative BCID
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_pads_bcid_rel_layer'+str(layer)].Draw()
            if layer == 1:
                with open(resdir+'bcid_rel_pads.txt', 'a+') as outfile:
                    outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_pads_bcid_rel_layer'+str(layer)].GetRMS()) + '\n')
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_pads_bcid_rel_in_time_peak_layer'+str(layer)].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_pads_bcid_rel_layer'+str(singleLayer)].Draw()
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_pads_bcid_rel_in_time_peak_layer'+str(singleLayer)].Draw()
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')


        # Pads hit profile
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h2_pads_hitsProfile_layer'+str(layer)].Draw('colz')
            maxpadx, maxpady, maxpadz = ctypes.c_int(0),ctypes.c_int(0),ctypes.c_int(0)
            histograms['h2_pads_hitsProfile_layer'+str(layer)].GetBinXYZ(histograms['h2_pads_hitsProfile_layer'+str(layer)].GetMaximumBin(),maxpadx,maxpady,maxpadz)
            max_pad[layer] = 'l' + str(layer) + '_x' + str(maxpadx.value) + '_y' + str(maxpady.value)
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h2_pads_hitsProfile_layer'+str(singleLayer)].Draw('colz')

        # PDO vs TDO
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            mean = histograms['h2_pads_pdo_vs_tdo_calibrated_layer'+str(layer)].GetMean()
            rms = histograms['h2_pads_pdo_vs_tdo_calibrated_layer'+str(layer)].GetRMS()
            histograms['h2_pads_pdo_vs_tdo_calibrated_layer'+str(layer)].RebinY(pdo_rebin)
            histograms['h2_pads_pdo_vs_tdo_calibrated_layer'+str(layer)].GetXaxis().SetRangeUser(mean-5*rms, mean+5*rms)
            histograms['h2_pads_pdo_vs_tdo_calibrated_layer'+str(layer)].Draw('colz')

        if not doFastPlots:
            cnv.SaveAs(resdir+outfilename+'.pdf(')
            # Single pad plots
            cnv.Clear()
            cnv.Divide(2, 1)
            for h_name in sorted(histograms):
                if 'h_pads_pdo_l' not in h_name:
                    continue
                if 'layer' in h_name:
                    continue
                if histograms[h_name].GetEntries() == 0:
                    continue
                cnv.cd(1)
                histograms[h_name].Rebin(pdo_rebin)
                histograms[h_name].Draw()
                num_pad = histograms[h_name].GetEntries()
                eff_pad = 100.0 * num_pad / tot_events
                latex.DrawLatex(0.45, 0.8, '#varepsilon = {0:.2f}%'.format(eff_pad))

                if max_pad[1] in h_name:
                    with open(resdir+singlePadLog, 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(eff_pad) + '\n')
                cnv.cd(2)
                mean = histograms[h_name.replace('_pdo_', '_tdo_calibrated_')].GetMean()
                rms = histograms[h_name.replace('_pdo_', '_tdo_calibrated_')].GetRMS()
                histograms[h_name.replace('_pdo_', '_tdo_calibrated_')].GetXaxis().SetRangeUser(mean-10*rms, mean+10*rms)
                histograms[h_name.replace('_pdo_', '_tdo_calibrated_')].Rebin(tdo_rebin)
                histograms[h_name.replace('_pdo_', '_tdo_calibrated_')].Draw()
                cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Leading pad pdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_pads_pdo_maxHit_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_pads_pdo_maxHit_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Leading pad tdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                mean = histograms['h_pads_tdo_calibrated_maxHit_layer'+str(layer)].GetMean()
                rms = histograms['h_pads_tdo_calibrated_maxHit_layer'+str(layer)].GetRMS()
                histograms['h_pads_tdo_calibrated_maxHit_layer'+str(layer)].GetXaxis().SetRangeUser(mean-10*rms, mean+10*rms)
                histograms['h_pads_tdo_calibrated_maxHit_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Subleading pad pdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_pads_pdo_subLead_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_pads_pdo_subLead_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Subleading pad tdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                mean = histograms['h_pads_tdo_calibrated_subLead_layer'+str(layer)].GetMean()
                rms = histograms['h_pads_tdo_calibrated_subLead_layer'+str(layer)].GetRMS()
                histograms['h_pads_tdo_calibrated_subLead_layer'+str(layer)].GetXaxis().SetRangeUser(mean-10*rms, mean+10*rms)
                histograms['h_pads_tdo_calibrated_subLead_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Subsubleading pad pdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_pads_pdo_subSubLead_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_pads_pdo_subSubLead_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Subsubleading pad tdo
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                mean = histograms['h_pads_tdo_calibrated_subSubLead_layer'+str(layer)].GetMean()
                rms = histograms['h_pads_tdo_calibrated_subSubLead_layer'+str(layer)].GetRMS()
                histograms['h_pads_tdo_calibrated_subSubLead_layer'+str(layer)].GetXaxis().SetRangeUser(mean-10*rms, mean+10*rms)
                histograms['h_pads_tdo_calibrated_subSubLead_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Charge ratio plots
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_pads_pdo_ratio_subLeadOverMaxHit_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_pads_pdo_ratio_subSubLeadOverMaxHit_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Number of hits vs maxHit PDO
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h2_pads_nHits_vs_pdo_maxHit_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h2_pads_nHits_vs_pdo_maxHit_layer'+str(layer)].Draw('colz')

                
            
        # Still want to print this even if moving on to strips...
        if doStrips:
            cnv.SaveAs(resdir+outfilename+'.pdf(')
            if singleLayer in range(1, 5):
                cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

    if doStrips:
        # Now look at strips
        cnv.Clear()

        # Number of strip layers fired
        histograms['h_strips_nFiringLayers'].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        # Number of hits per strip layer
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_nHits_layer'+str(layer)].SetTitle(filename.split('/')[-1].strip('_decoded.root').strip('out_'))
            histograms['h_strips_nHits_layer'+str(layer)].Draw()
            num = histograms['h_strips_nHits_layer'+str(layer)].Integral(histograms['h_strips_nHits_layer'+str(layer)].FindBin(1), histograms['h_strips_nHits_layer'+str(layer)].GetNbinsX())
            denom = histograms['h_strips_nHits_layer'+str(layer)].Integral()
            try:
                eff = 100.0 * num / denom
            except ZeroDivisionError:
                eff = 0
            print('Layer '+ str(layer) + ' strip efficiency is ' + str(eff) + '%')
            latex.DrawLatex(0.45, 0.8, '#varepsilon = {0:.2f}%'.format(eff))
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if last:
            cnv.SaveAs(resdir+'stripsMultiplicity.pdf)')
        else:
            cnv.SaveAs(resdir+'stripsMultiplicity.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_strips_nHits_layer'+str(singleLayer)].Draw()
            num = histograms['h_strips_nHits_layer'+str(singleLayer)].Integral(histograms['h_strips_nHits_layer'+str(singleLayer)].FindBin(1), histograms['h_strips_nHits_layer'+str(singleLayer)].GetNbinsX())
            denom = histograms['h_strips_nHits_layer'+str(singleLayer)].Integral()
            try:
                eff = 100.0 * num / denom
            except ZeroDivisionError:
                eff = 0
            print('Layer '+ str(singleLayer) + ' strip efficiency is ' + str(eff) + '%')
            latex.DrawLatex(0.45, 0.8, '#varepsilon = {0:.2f}%'.format(eff))
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')
            
        # PDO per strip layer
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_pdo_layer'+str(layer)].Rebin(pdo_rebin)
            histograms['h_strips_pdo_layer'+str(layer)].Draw()
            #histograms['h_strips_pdo_layer'+str(layer)].Fit('landau','','',histograms['h_strips_pdo_layer'+str(layer)].GetBinCenter(histograms['h_strips_pdo_layer'+str(layer)].FindFirstBinAbove()),histograms['h_strips_pdo_layer'+str(layer)].GetBinCenter(histograms['h_strips_pdo_layer'+str(layer)].GetNbinsX()-1))
            #ROOT.gStyle.SetOptFit(111)
            #histograms['h_strips_pdo_layer'+str(layer)].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_strips_pdo_layer'+str(singleLayer)].Draw()
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

        # Calibrated TDO
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_tdo_calibrated_layer'+str(layer)].Rebin(tdo_rebin)
            histograms['h_strips_tdo_calibrated_layer'+str(layer)].Draw()
            mean = histograms['h_strips_tdo_calibrated_layer'+str(layer)].GetMean()
            rms = histograms['h_strips_tdo_calibrated_layer'+str(layer)].GetRMS()
            histograms['h_strips_tdo_calibrated_layer'+str(layer)].Fit('gaus','','',mean-3*rms,mean+3*rms)
            ROOT.gStyle.SetOptFit(111)

            histograms['h_strips_tdo_calibrated_layer'+str(layer)].Draw()
            if histograms['h_strips_tdo_calibrated_layer'+str(layer)].GetFunction('gaus'):
                sigma = histograms['h_strips_tdo_calibrated_layer'+str(layer)].GetFunction('gaus').GetParameter(2)
                latex.DrawLatex(0.2, 0.4, '#sigma = {0:.2f} ns'.format(sigma))

        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_strips_tdo_calibrated_layer'+str(singleLayer)].Draw()
            if histograms['h_strips_tdo_calibrated_layer'+str(singleLayer)].GetFunction('gaus'):
                sigma = histograms['h_strips_tdo_calibrated_layer'+str(singleLayer)].GetFunction('gaus').GetParameter(2)
                latex.DrawLatex(0.2, 0.4, '#sigma = {0:.2f} ns'.format(sigma))
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

        # Relative BCID
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_bcid_rel_layer'+str(layer)].Draw()
            if layer == 1:
                with open(resdir+'bcid_rel_strips.txt', 'a+') as outfile:
                    outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_bcid_rel_layer'+str(layer)].GetRMS()) + '\n')
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_bcid_rel_in_time_peak_layer'+str(layer)].Draw()
        cnv.SaveAs(resdir+outfilename+'.pdf(')

        if singleLayer in range(1, 5):
            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_strips_bcid_rel_layer'+str(singleLayer)].Draw()
            cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

            cnv_singleLayer.Clear()
            cnv_singleLayer.cd()
            histograms['h_strips_bcid_rel_in_time_peak_layer'+str(singleLayer)].Draw()

        # Strips hit profile
        cnv.Clear()
        cnv.Divide(2, 2)
        for layer in range(1, 5):
            cnv.cd(layer)
            histograms['h_strips_hitsProfile_layer'+str(layer)].SetTitle(filename.split('/')[-1].strip('_decoded.root').strip('out_'))
            histograms['h_strips_hitsProfile_layer'+str(layer)].Draw()
        if last:
            cnv.SaveAs(resdir+'strips_profile.pdf)')
        else:
            cnv.SaveAs(resdir+'strips_profile.pdf(')
            
        if not doFastPlots:
            cnv.SaveAs(resdir+outfilename+'.pdf(')
            if singleLayer in range(1, 5):
                cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')


            # Number of clusters
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_nClusters_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'nclusters.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_nClusters_layer'+str(layer)].GetMean()) + '\n')

            cnv.SaveAs(resdir+outfilename+'.pdf(')
            
            # Clusters per track
            cnv.Clear()
            histograms['h_strips_nClustersPerTrack'].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Clusters distance
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_clusterDistance_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'cluster_distance.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_clusterDistance_layer'+str(layer)].GetMean()) + '\n')

            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Strips per cluster vs. N clusters
            # axis = 2 --> Y axis
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h2_strips_stripsPerCluster_vs_nClusters_layer'+str(layer)].SetTitle(filename.split('/')[-1].strip('_decoded.root').strip('out_'))
                histograms['h2_strips_stripsPerCluster_vs_nClusters_layer'+str(layer)].Draw('colz')
                if layer == 1:
                    with open(resdir+'strips_vs_clusters_3D.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h2_strips_stripsPerCluster_vs_nClusters_layer'+str(layer)].GetMean()) + '\t' +  str(histograms['h2_strips_stripsPerCluster_vs_nClusters_layer'+str(layer)].GetMean(2)) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')


            # Strips per cluster vs. N clusters (weighted)
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h2_strips_stripsPerCluster_vs_nClusters_weighted_layer'+str(layer)].Draw('colz')
                if layer == 1:
                    with open(resdir+'strips_vs_clusters_weighted_3D.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h2_strips_stripsPerCluster_vs_nClusters_weighted_layer'+str(layer)].GetMean()) + '\t' +  str(histograms['h2_strips_stripsPerCluster_vs_nClusters_weighted_layer'+str(layer)].GetMean(2)) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Strips per cluster
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_stripsPerCluster_layer'+str(layer)].SetTitle(filename.split('/')[-1].strip('_decoded.root').strip('out_'))
                histograms['h_strips_stripsPerCluster_layer'+str(layer)].Draw()
                
                if layer == 1:
                    with open(resdir+'strips_per_cluster.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_stripsPerCluster_layer'+str(layer)].GetMean()) + '\n')

            cnv.SaveAs(resdir+outfilename+'.pdf(')

            if last:
                cnv.SaveAs(resdir+'stripsPerCluster.pdf)')
            else:
                cnv.SaveAs(resdir+'stripsPerCluster.pdf(')

            if singleLayer in range(1, 5):
                cnv_singleLayer.Clear()
                cnv_singleLayer.cd()
                histograms['h_strips_stripsPerCluster_layer'+str(singleLayer)].Draw()
                cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf(')

            # Cluster profile
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_clustersProfile_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')
            
            # PDO per strip cluster
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_chargePerCluster_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_strips_chargePerCluster_layer'+str(layer)].GetXaxis().SetRangeUser(0, 3000)
                histograms['h_strips_chargePerCluster_layer'+str(layer)].Draw()
                #histograms['h_strips_chargePerCluster_layer'+str(layer)].Fit('landau','','',histograms['h_strips_chargePerCluster_layer'+str(layer)].GetBinCenter(histograms['h_strips_chargePerCluster_layer'+str(layer)].FindFirstBinAbove()),histograms['h_strips_chargePerCluster_layer'+str(layer)].GetBinCenter(histograms['h_strips_chargePerCluster_layer'+str(layer)].GetNbinsX()-1))
                #ROOT.gStyle.SetOptFit(111)
                #histograms['h_strips_chargePerCluster_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'chargePerCluster.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_chargePerCluster_layer'+str(layer)].GetMean()) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            if singleLayer in range(1, 5):
                cnv_singleLayer.Clear()
                cnv_singleLayer.cd()
                histograms['h_strips_chargePerCluster_layer'+str(singleLayer)].Draw()

            
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_chargePerCluster_1strip_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_strips_chargePerCluster_1strip_layer'+str(layer)].GetXaxis().SetRangeUser(0, 3000)
                histograms['h_strips_chargePerCluster_1strip_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'chargePerCluster_1strip.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_chargePerCluster_1strip_layer'+str(layer)].GetMean()) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')


            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_chargePerCluster_2strip_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_strips_chargePerCluster_2strip_layer'+str(layer)].GetXaxis().SetRangeUser(0, 3000)
                histograms['h_strips_chargePerCluster_2strip_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'chargePerCluster_2strip.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_chargePerCluster_2strip_layer'+str(layer)].GetMean()) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_chargePerCluster_multstrip_layer'+str(layer)].Rebin(pdo_rebin)
                histograms['h_strips_chargePerCluster_multstrip_layer'+str(layer)].GetXaxis().SetRangeUser(0, 3000)
                histograms['h_strips_chargePerCluster_multstrip_layer'+str(layer)].Draw()
                if layer == 1:
                    with open(resdir+'chargePerCluster_multstrip.txt', 'a+') as outfile:
                        outfile.write(filename.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(histograms['h_strips_chargePerCluster_multstrip_layer'+str(layer)].GetMean()) + '\n')
            cnv.SaveAs(resdir+outfilename+'.pdf(')            


            # Track information
            cnv.Clear()
            cnv.Divide(2, 1)
            cnv.cd(1)
            mean = histograms['h_strips_trackSlope'].GetMean()
            rms = histograms['h_strips_trackSlope'].GetRMS()
            histograms['h_strips_trackSlope'].GetXaxis().SetRangeUser(mean-rms, mean+rms)
            histograms['h_strips_trackSlope'].Draw()
            cnv.cd(2)
            histograms['h_strips_trackIntercept'].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Biased residuals
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_biasedResidual_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Global residuals
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h_strips_globalResidual_layer'+str(layer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Local residuals
            cnv.Clear()
            cnv.Divide(2, 2)
            for layer in range(1, 5):
                cnv.cd(layer)
                histograms['h2_strips_localResidual_layer'+str(layer)].Draw('colz')
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Pairwise residuals
            cnv.Clear()
            cnv.Divide(3, 2)
            ctr = 1
            for ilayer in range(1, 5):
                for jlayer in range(ilayer+1, 5):
                    cnv.cd(ctr)
                    ctr += 1
                    histograms['h_strips_pairwiseResidual_layer'+str(ilayer)+str(jlayer)].Draw()
            cnv.SaveAs(resdir+outfilename+'.pdf(')

            # Cluster correlations
            cnv.Clear()
            cnv.Divide(3, 2)
            ctr = 1
            for ilayer in range(1, 5):
                for jlayer in range(ilayer+1, 5):
                    cnv.cd(ctr)
                    ctr += 1
                    meanx = histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetMean()
                    rmsx = histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetRMS()
                    meany = histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetMean(2)
                    rmsy = histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetRMS(2)
                    
                    histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetXaxis().SetRangeUser(meanx-20*rmsx, meanx+20*rmsx)
                    histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].GetYaxis().SetRangeUser(meany-20*rmsy, meany+20*rmsy)

                    histograms['h2_strips_clustersCorrelation_'+str(ilayer)+'vs'+str(jlayer)].Draw('colz')
            
                    
                    
            
    # Make sure the last 'SaveAs' statement has the closing bracket to properly close the .pdf file
    cnv.SaveAs(resdir+outfilename+'.pdf)')
    if singleLayer in range(1, 5):
        cnv_singleLayer.SaveAs(resdir+outfilename+'_layer'+str(singleLayer)+'.pdf)')



if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('Usage: python make_plots.py <name_of_log_file>')
        sys.exit()

    else:
        if(not os.path.isdir('plots')):
            print('Making plots directory...')
            os.mkdir('plots')
        for arg in sys.argv:
            if 'make_plots.py' in arg:
                continue
            print('Producing plots for ' + arg + '...')

            mult = calc_muon_S(arg.split('/')[-1].replace('_decoded.root','').replace('out_',''))
            if mult != None:
                with open('plots/muon_strips_per_cluster.txt', 'a+') as outfile:
                    outfile.write(arg.split('/')[-1].replace('_decoded.root','').replace('out_','') + '\t' + str(mult) + '\n')
            
            if arg == sys.argv[len(sys.argv)-1]:
                make_plots(arg,True)
            else:
                make_plots(arg,False)
