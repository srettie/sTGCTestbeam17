import ROOT
import sys
import os
import array

ROOT.gROOT.SetBatch(1)

debug = False

# Irradiated pad area (cm^2)
pad_area = 8.14*8.14*2

bad_runs = [
    'run_181027_mu_2900V_At2-2_NikScint',
    'run_181027_mu_2900V_At2-2_extScint',#??
    'run_181027_mu_3000V_At2-2_extScint'#??
]

selections = ['2900_25','2800_50','2900_50','3000_50']

colors = {
    '2900_50' : ROOT.kBlack,
    '2900_25' : ROOT.kRed,
    '2800_50' : ROOT.kBlue,
    '3000_50' : ROOT.kGreen+1,
}
titles = {
    '2900_50' : '2.9 kV (50 ns)',
    '2900_25' : '2.9 kV (25 ns)',
    '2800_50' : '2.8 kV (50 ns)',
    '3000_50' : '3.0 kV (50 ns)',
}
# From run log: https://docs.google.com/spreadsheets/d/1wbWgpTJEi8hLIQC9d21LeSXfNLX9naPzsd5vNcKBnO4/edit#gid=1192045683
photon_rates = {
    '181027_mu_2800V_At1' : 13000,
    '181027_mu_2800V_At1_extScint' : 13000,
    '181027_mu_2900V_At1_extScint' : 13000,
    '181027_mu_3000V_At1_extScint' : 13000,
    '181027_mu_3000V_At2-2_extScint' : 6904,
    '181027_mu_2900V_At2-2_extScint' : 6904,
    '181027_mu_2800V_At2-2_extScint' : 7062,
    '181027_mu_2800V_At2-2_NikScint' : 7062,
    '181027_mu_2900V_At2-2_NikScint' : 6904,
    '181027_mu_3000V_At2-2_NikScint' : 6761,
    '181027_mu_2900V_At6-8_NikScint' : 3000,
    '181027_mu_3000V_At6-8_NickScint' : 3000,
    '181027_mu_3000V_At4-6_NickScint' : 4172,
    '181027_mu_2900V_At4-6_NickScint' : 4172,
    '181027_mu_2800V_At4-6_NikScint' : 4172,
    '181027_mu_3000V_At10_NikScint' : 2263,
    '181027_mu_2900V_At10_NickScint' : 2263,
    '181027_mu_2800V_At10_NickScint' : 2263,
    '181027_mu_3000V_At10_NickScint_2' : 2263,
    '181027_mu_2900V_At10_NickScint_2' : 2263,
    '181027_mu_2900V_NoSource_NikScint' : 0,
    '181027_mu_3000V_NoSource_NikScint' : 0,
    '181027_mu_2800V_NoSource_NikScint' : 0,
    '181027_mu_2900V_At22_NickScint' : 1100,
    '181027_mu_3000V_At22_NickScint' : 1100,
    '181027_mu_2900V_At22_NickScint_NeighborOn' : 1100,
    '181027_mu_2900V_At22_NickScint_padToTmode' : 1100,
    '181027_mu_2800V_At22_NickScint' : 1100,
    '181028_mu_2800V_At22_TailCanc' : 1100,
    '181028_mu_2900V_At100_TailCanc' : 290,
    '181028_mu_2900V_At100' : 290,
    '181028_mu_2900V_At100_3aBits' : 290,
    '181028_mu_3000V_At100' : 290,
    '181028_mu_2800V_At100' : 290,
    '181028_mu_2800V_At1-5' : 11000,
    '181028_mu_2900V_At1-5' : 11000,
    '181028_mu_3000V_At1-5' : 11000,
    '181028_mu_2900V_At1-5_TailCanc' : 11000,
    '181028_mu_2900V_At1-5_3aBits' : 11000,
    '181028_mu_2800V_At46' : 565,
    '181028_mu_2900V_At46' : 565,
    '181028_mu_2900V_At46_TailCanc' : 565,
    '181029_mu_2900_noSource_pad17_25ns' : 0,
    '181029_mu_2900_noSource_pad17_50ns' : 0,
    '181029_mu_3000_noSource_pad17_25ns' : 0,
    '181029_mu_2900V_At46' : 581.7642857,
    '181029_mu_2900V_At46_padToT' : 581.7642857,
    '181029_mu_2900V_At46_25ns-pad17' : 581.7642857,
    '181029_mu_2900V_At2-2_25ns-pad17' : 7500,
    '181029_mu_2900V_At2-2' : 7500,
    '181029_mu_2900V_At10_25ns-pad17' : 2300,
    '181029_mu_2900V_At22_25ns-pad17 ' : 1150,
}


def parse_line(l, is3D=False):
    if debug:
        print(l)
    qx = float(l.split('\t')[-1])
    qy = 0
    if is3D:
        qx = float(l.split('\t')[-2])
        qy = float(l.split('\t')[-1])
    tag = l.split('\t')[0]
    peaking_time = 50
    if '25ns' in tag:
        peaking_time = 25
    voltage = float(tag.split('_mu_')[1][0:4])
    date = tag.split('run_')[1].split('_')[0]
    attenuation = 0
    if 'noSource' not in l:
        attenuation = float(tag.split('At')[1].split('_')[0].replace('-','.'))
    return date,voltage,peaking_time,attenuation,qx,qy
    
def summarize_plots(filename, is3D=False):

    isEfficiency = ('max_pad_eff' in filename)
    isBCIDRel = ('bcid_rel' in filename)
    isStripsPerCluster = ('strips_per_cluster' in filename)
    isNClusters = ('nclusters' in filename)
    isDistance = ('cluster_distance' in filename)
    isClusterPDO_1strip = ('chargePerCluster_1strip' in filename)
    isClusterPDO_2strip = ('chargePerCluster_2strip' in filename)
    isClusterPDO_multstrip = ('chargePerCluster_multstrip' in filename)

    # Gather relevant information
    # Key = [HV]_[PeakingTime]
    quantity = {}
    rates = {}
    invatt = {}

    tg3d = ROOT.TGraph2D()#'h3', 'h3', 14,1,15, 5,1,6, 1000,0,10000)
    ipoint3d = 0
    
    # Initialize maps
    for s in selections:
        quantity[s] = []
        rates[s] = []
        invatt[s] = []
        
    with open(filename, 'r') as logfile:
        for line in logfile:
            if line.split('\t')[0] in bad_runs:
                continue
            if 'noBeam' in line:
                continue
            if '_NeighborOn' in line:
                continue
            if '_padToT' in line:
                continue
            if '_3aBits' in line:
                continue
            if '_TailCanc' in line:
                continue
            if 'extScint' in line:
                continue
            
            
            date,volt,peaking_time,att,qx,qy = parse_line(line.strip('\n'), is3D)
            identifier = line.strip('run_').split('\t')[0]
            
            if debug:
                print('\n',identifier)
                print('Date:',date)
                print('Voltage:',volt)
                print('Peaking time:',peaking_time)
                print('Attenuation:',att)
                print('Quantity x:',qx)
                print('Quantity y:',qy)
                print('Photon rate:',photon_rates[identifier])
                print('Normalized rate:',photon_rates[identifier]*pad_area)

            # att == 0 means no source
            plot_key = ''
            if volt == 2900 and peaking_time == 50:
                plot_key = '2900_50'
            if volt == 2900 and peaking_time == 25:
                plot_key = '2900_25'
            if volt == 2800 and peaking_time == 50:
                plot_key = '2800_50'
            if volt == 3000 and peaking_time == 50:
                plot_key = '3000_50'

            if plot_key == '':
                continue
            
            if att == 0:
                invatt[plot_key].append(0)
                rates[plot_key].append(0)
            else:
                invatt[plot_key].append(1.0/att)
                #rates[plot_key].append(photon_rates[identifier]*pad_area)
                rates[plot_key].append(photon_rates[identifier])
            quantity[plot_key].append(qx)

            if is3D:
                if att == 0:
                    tg3d.SetPoint(ipoint3d, qx, qy, 0)
                else:
                    tg3d.SetPoint(ipoint3d, qx, qy, photon_rates[identifier]*pad_area)
                ipoint3d += 1

    cnv = ROOT.TCanvas()
    cnv.cd()

    if is3D:
        tg3d.SetTitle(';Number of clusters;Strips per cluster;Photon Rate [Hz]')
        tg3d.SetMarkerSize(1)
        tg3d.SetMarkerStyle(20)
        tg3d.Draw('pcol')
        cnv.SaveAs('plots/'+filename.split('/')[-1].replace('.txt','')+'_comparison.pdf')
        cnv.SaveAs('plots/'+filename.split('/')[-1].replace('.txt','')+'_comparison.root')
        return
    
    leg = ROOT.TLegend(0.65,0.68,0.85,0.88)

    tgraphs = {}

    first = True
    for s in selections:
        tgraphs[s]= ROOT.TGraph(len(rates[s]), array.array('d',rates[s]), array.array('d',quantity[s]))
        tgraphs[s].SetTitle()
        #tgraphs[s].GetXaxis().SetTitle('Photon Rate [Hz]')
        tgraphs[s].GetXaxis().SetTitle('Photon Rate [Hz/cm^{2}]')
        tgraphs[s].SetMarkerColor(colors[s])
        tgraphs[s].SetMinimum(0)
        tgraphs[s].SetMaximum(1800)

        if isEfficiency:
            tgraphs[s].GetYaxis().SetTitle('Single-Pad Efficiency [%]')
            tgraphs[s].SetMinimum(0)
            tgraphs[s].SetMaximum(100)
        elif isBCIDRel:
            tgraphs[s].GetYaxis().SetTitle('BCID_{rel} RMS')
        elif isStripsPerCluster:
            tgraphs[s].GetYaxis().SetTitle('Strips Per Cluster Mean')
        elif isNClusters:
            tgraphs[s].GetYaxis().SetTitle('Cluster Multiplicity Mean')
        elif isDistance:
            tgraphs[s].GetYaxis().SetTitle('\"Distance\" Mean')
            #tgraphs[s].SetMinimum(6.5)
            #tgraphs[s].SetMaximum(9)
        elif isClusterPDO_1strip:
            tgraphs[s].GetYaxis().SetTitle('ADC charge per cluster (1 strip) mean')
        elif isClusterPDO_2strip:
            tgraphs[s].GetYaxis().SetTitle('ADC charge per cluster (2 strips) mean')
        elif isClusterPDO_multstrip:
            tgraphs[s].GetYaxis().SetTitle('ADC charge per cluster (>2 strips) mean')

            
        #tgraphs[s].GetXaxis().SetLimits(0, 5.2)
        leg.AddEntry(tgraphs[s], titles[s],"P")
        if first:
            tgraphs[s].Draw('AP*')
            first = False
        else:
            tgraphs[s].Draw('same,P*')
    leg.Draw('same')

    cnv.SaveAs('plots/'+filename.split('/')[-1].replace('.txt','')+'_comparison.pdf')

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('Usage: python summarize_plots.py <name_of_log_file>')
        sys.exit()
    else:
        if(not os.path.isdir('plots')):
            print('Making plots directory...')
            os.mkdir('plots')
        for arg in sys.argv:
            if 'summarize_plots.py' in arg:
                continue
            print('Producing plots for ' + arg + '...')
            if '_3D' in arg:
                summarize_plots(arg,True)
            else:
                summarize_plots(arg)

good_runs = [
    'run_181027_mu_2800V_At10_NickScint_decoded.root',
    'run_181027_mu_2800V_At2-2_NikScint_decoded.root',
    'run_181027_mu_2800V_At22_NickScint_decoded.root',
    'run_181027_mu_2800V_noSource_NickScint_decoded.root',
    'run_181027_mu_2900V_At10_NickScint_2_decoded.root',
    'run_181027_mu_2900V_At10_NickScint_decoded.root',
    'run_181027_mu_2900V_At2-2_NikScint_decoded.root',
    'run_181027_mu_2900V_At22_NickScint_decoded.root',
    'run_181027_mu_2900V_At4-6_NickScint_decoded.root',
    'run_181027_mu_2900V_At6-8_NikScint_decoded.root',
    'run_181027_mu_2900V_noSource_New181027OptThreshold_decoded.root',
    'run_181027_mu_2900V_noSource_NickScint_decoded.root',
    'run_181027_mu_3000V_At10_NickScint_2_decoded.root',
    'run_181027_mu_3000V_At2-2_NikScint_decoded.root',
    'run_181027_mu_3000V_At22_NickScint_decoded.root',
    'run_181027_mu_3000V_At4-6_NickScint_decoded.root',
    'run_181027_mu_3000V_At6-8_NickScint_decoded.root',
    'run_181027_mu_3000V_noSource_NickScint_decoded.root',
    'run_181028_mu_2800V_At46_decoded.root',
    'run_181028_mu_2900V_At100_decoded.root',
    'run_181028_mu_2900V_At46_decoded.root',
    'run_181028_mu_3000V_At1-5_decoded.root',
    'run_181028_mu_3000V_At100_decoded.root',
    'run_181029_mu_2900V_At10_25ns-pad17_decoded.root',
    'run_181029_mu_2900V_At2-2_25ns-pad17_decoded.root',
    'run_181029_mu_2900V_At2-2_decoded.root',
    'run_181029_mu_2900V_At46_25ns-pad17_decoded.root',
    'run_181029_mu_2900V_At46_decoded.root',
    'run_181029_mu_2900_noSource_pad17_25ns_decoded.root',
    'run_181029_mu_2900_noSource_pad17_50ns_decoded.root',
    'run_181029_mu_3000_noSource_pad17_25ns_decoded.root'
]
