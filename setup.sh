platform=$(uname -s)

EXTDIR=$PWD/external
CLHEP=$EXTDIR/2.4.1.0//CLHEP/install
XERCES=$EXTDIR/xerces-c-3.2.1/install/

#I guess PATH is PATH in osx
export PATH=$PATH:$PWD
export PATH=$PATH:$PWD/python
export PATH=$PATH:$PWD/util
export PATH=$PATH:external/xerces-c-3.2.1/install/bin

echo "Appending to paths for "$platform
echo $PWD/lib
echo $CLHEP
echo $XERCES

if [ "$platform"=="Linux" ];then 
    #the first one is necessary for .pcm files
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/src
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/lib
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CLHEP/lib
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$XERCES/lib
elif [ "$platform"=="Darwin" ];then
    export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$PWD/src
    export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$PWD/lib
    export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$CLHEP/lib
    export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$XERCES/lib

else
    echo "Unknown platform"
fi    


odirs='data results'

for d in $odirs;do
  if [ ! -d $d ];then
	mkdir $d
  fi
done
