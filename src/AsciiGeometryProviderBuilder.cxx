/*
 * GeometryBuilder.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: ekajomov and Luca M
 */

#include "AsciiGeometryProviderBuilder.h"
#include <iostream>
#include <fstream>

AsciiGeometryProviderBuilder::AsciiGeometryProviderBuilder() {
	// TODO Auto-generated constructor stub

}

AsciiGeometryProviderBuilder::~AsciiGeometryProviderBuilder() {
	// TODO Auto-generated destructor stub
}

GeometryProvider* AsciiGeometryProviderBuilder::Build(std::string fname)
{
	GeometryProvider* gp = new GeometryProvider();
	int feb, vmm, channel, layer, type;
	double x, y, z;

	std::ifstream ifs(fname);
	std::string line;

	std::cout << "INFO: Loading geometry from ASCII file - " << fname << std::endl;
	if (!ifs.is_open())
	{
			std::cout << "ERROR: Failed to open the geometry file - closing" << std::endl;
	}


	 while (!ifs.eof())
	 {
		 std::getline (ifs,line);
		 const char *c_line = line.c_str();
		 sscanf(c_line, "%i %i %i %lf %lf %lf %i %i", &feb, &vmm,	 &channel, &x, &y, &z, &layer, &type);
		 //std::cout << "Adding component: " << feb << " " << vmm << " " << channel << " " << x << " " << y << " " << z << " "<< layer << std::endl;
		 gp->AddComponent(feb, vmm, channel, x, y, z, layer, type);

	 }
	 std::cout << "INFO: Geometry loaded" << std::endl;
	return gp;
}
