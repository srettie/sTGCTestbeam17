/*
 * GeometryBuilder.h
 *
 *  Created on: Sep 20, 2017
 *      Author: ekajomov and Luca M
 */

#ifndef ASCIIGEOMETRYPROVIDERBUILDER_H_
#define ASCIIGEOMETRYPROVIDERBUILDER_H_

#include <string>
#include "GeometryProvider.h"

class AsciiGeometryProviderBuilder {
public:
	AsciiGeometryProviderBuilder();
	virtual ~AsciiGeometryProviderBuilder();

	GeometryProvider* Build(std::string fname);


};

#endif /* ASCIIGEOMETRYPROVIDERBUILDER_H_ */
