/*
 * Cluster.cpp
 *
 *  Created on: Sep 6, 2017
 *      Author: ekajomov and Luca M
 */

#include "Cluster.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFitResult.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "TMath.h"

Cluster::Cluster():
  totalAdc(0),x(-1),y(-1),y_aligned(-1),y_corrected(-1),z(-1),
sigma_x(-1),sigma_y(-1),sigma_z(-1)
{
	// TODO Auto-generated constructor stub

}

Cluster::~Cluster() {
	// TODO Auto-generated destructor stub
}

bool Cluster::AddHit(Hit hit)
{
	if (hit.GetAdc() == -1)
		return false;
	totalAdc += hit.GetAdc();
	hits.push_back(hit);
	return true;
}

int Cluster::GetNHits() const
{
	return hits.size();
}

bool Cluster::CorrectForAlignment(std::vector<std::vector<float>> in_parameters)
{
  int layer = this->GetLayer();

  // Correct wrt layer 1

  if(layer<1)
    return false;
  
  y_aligned = in_parameters[0][layer-1]*y + in_parameters[1][layer-1];

  return true;
  
}

bool Cluster::CorrectForOffset()
{
  int layer = this->GetLayer();

  if((layer == 2) || (layer == 4))
    y += 0.5;
  
}

bool Cluster::ComputeCentroid()
{
	if (hits.size() < 1)
		return false;

	x = 0;
	y = 0;
	z = 0;
	for (unsigned int i_hit = 0; i_hit < hits.size(); i_hit++)
	{
		x += hits[i_hit].GetAdc() * (hits[i_hit].GetComponent()).GetX();
		y += hits[i_hit].GetAdc() * (hits[i_hit].GetComponent()).GetY();
		z += hits[i_hit].GetAdc() * (hits[i_hit].GetComponent()).GetZ();
	}

	float invTotalAdc = 1./totalAdc;
	x *= invTotalAdc;
	y *= invTotalAdc;
	z *= invTotalAdc;
	sigma_x = -1;
	sigma_y = -1;
	sigma_z = -1;


	return true;
}

bool Cluster::IsSaturated() const{
	bool flag_saturated = 0;
	for (unsigned int i_hit = 0; i_hit < hits.size(); i_hit++){
		if(hits[i_hit].GetAdc() > 1000)
			flag_saturated = 1;
	}
	return flag_saturated;
}

Hit Cluster::GetMaxHit() const{
	Hit MaxHit;
	int MaxAdc = 0;

	for (unsigned int i_hit = 0; i_hit < hits.size(); i_hit++){
		if(hits[i_hit].GetAdc() > MaxAdc){
			MaxAdc = hits[i_hit].GetAdc();
			MaxHit = hits[i_hit];
		}
	}
	return MaxHit;
}

bool Cluster::ComputeGaussianFit(GeometryProvider* gp){
  
  // Start by computing centroid by default
  this->ComputeCentroid();

  if(hits.size() < 1)
    return false;

  // Get histograms
  int nbins_strips = gp->GetNStrips();

  /*
    int layer = this->GetLayer();
    if(layer == 1 || layer == 3){
    binmin = -0.5*3.2;
    binmax = 364.5*3.2;
    }else{
    binmin = 0;
    binmax = 365*3.2;
    }
  */
  //std::cout<< "new cluster in layer " << layer << " with " << hits.size() <<" hits"<<std::endl;

  TH1F* hy = new TH1F("hy", "hy", nbins_strips, 1, nbins_strips+1);//binmin, binmax);
  for (unsigned int i_hit = 0; i_hit < hits.size(); i_hit++){
    hy->Fill((hits[i_hit].GetComponent()).GetY(), hits[i_hit].GetAdc());
  }

  // Fit histograms
  double mean_y = hy->GetMean();
  double rms_y = hy->GetRMS();
  //std::cout << "cluster histogram mean: " << mean_y << " cluster histogram sigma: " << rms_y << std::endl;
  TF1 *fGaus = new TF1("myGaus", "gaus", mean_y-2*rms_y, mean_y+2*rms_y);
  fGaus->SetParameters(mean_y, 2*rms_y);
  if(fGaus){
    TFitResultPtr fit_results = hy->Fit(fGaus, "SQ0", "" , mean_y-2*rms_y, mean_y+2*rms_y);
    Int_t fitStatus1 = fit_results->IsEmpty();
    Int_t fitStatus2 = fit_results->IsValid();
    // Fill values appropriately if fit was successful
    if(fitStatus1 == 0 && fitStatus2 == 1){
      y = fit_results->Value(1);
      sigma_y = fit_results->ParError(1);
      //std::cout << "cluster fit mean: " << y << " cluster fit sigma: " << sigma_y << std::endl;
    }
    else{
      std::cout<< "!!!failed computing cluster Gaussian" << std::endl;
      delete hy;
      delete fGaus;
      return false;
    }
    delete hy;
    delete fGaus;
    return true;
  }
  else{
    std::cout << "no function found to fit cluster" << std::endl;
    return false;
  }
}

bool Cluster::ApplySWaveCorrection(){

  bool debug = false;

  if(debug)
    std::cout<<""<<std::endl;
  
  // Need to work in strip coordinates for this correction...
  double pitch = 3.2;
  
  // Start with aligned cluster position in strip coordinates
  if(debug)
    std::cout<<"Initial aligned y is "<<y_aligned<<std::endl;
  
  double y_strip = y_aligned / pitch;

  if(debug)
    std::cout<<"In strip coordinates, this is "<<y_strip<<std::endl;

  // Relative y position
  double y_rel = y_strip - TMath::Nint(y_strip);
  if(debug)
    std::cout<<"Relative y position is "<<y_rel<<std::endl;

  // y position relative to strip center
  double y_rel_strip_center = y_rel;
  
  // y position relative to gap center
  double y_rel_gap_center;
  if(y_rel >= 0.0)
    y_rel_gap_center = y_rel - 0.5;
  else
    y_rel_gap_center = y_rel + 0.5;


  int cluster_size = this->GetNHits();
  if(debug)
    std::cout<<"Size of cluster is "<<cluster_size<<" in layer "<<this->GetLayer()<<std::endl;

  if(cluster_size < 3 || cluster_size > 5){
    if(debug)
      std::cout<<"s-wave correction only defined for clusters of size 3-4-5!"<<std::endl;
    y_corrected = y_aligned;
    return false;
  }
  
  
  double A = s_corr_universal[cluster_size][this->GetLayer() - 1]/1000;
  if(debug)
    std::cout<<"Amplitude is "<<A<<std::endl;

  // Actual correction
  double corr = -A*TMath::Sin(TMath::Pi() * y_rel_strip_center / 0.5);
  if(debug)
    std::cout<<"Correction is "<<corr<<std::endl;

  // Convert correction from mm to strip number
  corr = corr / (pitch);
  if(debug)
    std::cout<<"In strip numbers, this is "<<corr<<std::endl;
  
  y_strip = y_strip + corr;

  if(debug)
    std::cout<<"The corrected y position is then "<<y_strip<<std::endl;

  // Convert back to mm
  y_corrected = y_strip * pitch;

  // Temporary for now...need to change plots in reco manager to use y_corrected and remove the below line
  y_aligned = y_strip * pitch;

  if(debug)
    std::cout<<"In mm, this is "<<y_aligned<<std::endl;
    
  return true;
}
