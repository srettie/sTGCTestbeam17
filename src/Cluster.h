/*
 * Cluster.h
 *
 *  Created on: Sep 6, 2017
 *      Author: ekajomov and Luca M
 */
#include "Hit.h"
#include "GeometryProvider.h"
#include <vector>
#include <iostream>
#include <map>

#ifndef CLUSTER_H_
#define CLUSTER_H_

class Cluster {
 public:
  Cluster();
  virtual ~Cluster();

  bool AddHit(Hit hit);

  Hit GetMaxHit() const;

  const std::vector<Hit>& GetHits() const {
    return hits;
  }

  int GetNHits() const;

  bool IsSaturated() const;

  bool CorrectForAlignment(std::vector<std::vector<float>> in_parameters);
	
  bool ComputeCentroid();

  bool ComputeGaussianFit(GeometryProvider* gp);

  bool ApplySWaveCorrection();

  bool CorrectForOffset();

  float GetSigmaX() const {
    return sigma_x;
  }

  void SetSigmaX(float sigmaX) {
    sigma_x = sigmaX;
  }

  float GetSigmaY() const {
    return sigma_y;
  }

  float GetSigmaZ() const {
    return sigma_z;
  }

  void SetSigmaZ(float sigmaZ) {
    sigma_z = sigmaZ;
  }

  int GetTotalAdc() const {
    return totalAdc;
  }

  float GetX() const {
    return x;
  }

  float GetY() const {
    return y;
  }

  float GetYaligned() const {
    return y_aligned;
  }

  float GetYcorrected() const {
    return y_corrected;
  }

  float GetZ() const {
    return z;
  }
  int GetLayer() const {
    int layer = -1;
    if(hits.size()>0)
      layer = (hits[0].GetComponent()).GetLayer();
    return layer;
  }
  void Dump() const{
    std::cout << "Cluster dump: (x,y,z) = (" << x << ", " << y << ", " << z << ")" <<std::endl;
    std::cout << " y_aligned: " << y_aligned << std::endl;
    std::cout << " y_corrected: " << y_corrected << std::endl;
    std::cout << " totalAdc: " << totalAdc << " nHits: " << hits.size() << std::endl;
    std::cout<< "hits: " <<std::endl;
    for(int i_hit = 0; i_hit< (int)hits.size(); i_hit++){
      hits[i_hit].Dump();
    }
  }
 protected:
  std::vector<Hit> hits;
  int totalAdc;
  float x;
  float y;
  float y_aligned;
  float y_corrected;
  float z;
  float sigma_x;
  float sigma_y;
  float sigma_z;
  std::map<int, std::vector<double>> s_corr_universal = {
    // Note that these amplitudes are in microns
    {3, {205.0, 205.0, 205.0, 205.0}},// 3-strip clusters, [L1, L2, L3, L4]
    {4, {206.0, 206.0, 206.0, 206.0}},// 4-strip clusters, [L1, L2, L3, L4]
    {5, {211.0, 211.0, 211.0, 211.0}}// 5-strip clusters, [L1, L2, L3, L4]
  };

};

#endif /* CLUSTER_H_ */
