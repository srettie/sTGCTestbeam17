/*
 * ClusterBuilder.cpp
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov and Luca M
 *
 */

#include "ClusterBuilder.h"
#include <algorithm>
#include <iostream>
#include <cmath>
#include "Hit.h"


bool cmpY(Hit& i, Hit& j) // comparison function to sort vector
{
  return (i.GetComponent().GetY() < j.GetComponent().GetY());
}

ClusterBuilder::ClusterBuilder(): minHits(1), maxHoles(0), nHitNeighborWindow(1), neighborDistance(3.2), tree(0),
				  tb_clusterNhits(0), tb_clusterLayer(0), tb_clusterX(0), tb_clusterY(0), tb_clusterYaligned(0), tb_clusterZ(0),
				  doSWaveCorrection(false), doGaussianClustering(false)
{

}

ClusterBuilder::~ClusterBuilder() {
}

bool ClusterBuilder::Init() {
  std::cout << "INFO: Initializing the cluster builder." << std::endl;
  std::cout << "INFO: Cluster builder setup. minHits: " << minHits << " maxHoles: " << maxHoles;
  std::cout << " nHitNeighborWindow: " << nHitNeighborWindow << " neighborDistance: " << neighborDistance << " mm " << std::endl;
  if(doSWaveCorrection)
    std::cout << "INFO: S-Wave correction will be applied." << std::endl;
  else
    std::cout << "INFO: S-Wave correction will not be applied." << std::endl;
  if(doGaussianClustering)
    std::cout << "INFO: Gaussian clustering will be used." << std::endl;
  else
    std::cout << "INFO: Centroid clustering will be used." << std::endl;

  tb_clusterNhits = new std::vector<int>;
  tree->Branch("clusterNhits", tb_clusterNhits);

  tb_clusterLayer = new std::vector<int>;
  tree->Branch("clusterLayer", tb_clusterLayer);

  //tb_clusterX
  tb_clusterX = new std::vector<float>;
  tree->Branch("clusterX", tb_clusterX);

  tb_clusterY = new std::vector<float>;
  tree->Branch("clusterY", tb_clusterY);

  tb_clusterYaligned = new std::vector<float>;
  tree->Branch("clusterYaligned", tb_clusterYaligned);

  tb_clusterZ = new std::vector<float>;
  tree->Branch("clusterZ", tb_clusterZ);

  tb_clusterTotalADC = new std::vector<int>;
  tree->Branch("clusterTotalADC", tb_clusterTotalADC);
  return true;
}

bool ClusterBuilder::ComputeCentroid(Cluster cl)
{
  cl.ComputeCentroid();
  return true;
}

bool ClusterBuilder::ComputeGaussianFit(Cluster cl)
{
  cl.ComputeGaussianFit(gp);
  return true;
}

bool ClusterBuilder::ApplySWaveCorrection(Cluster cl)
{
  cl.ApplySWaveCorrection();
  return true;
}

bool ClusterBuilder::Finalize() {
  return true;
}

std::vector<Cluster> ClusterBuilder::BuildClustersForLayer(std::vector<Hit>& hits){//, std::vector<std::vector<float>> in_parameters){

  // TODO - MAKE THIS FAIL IF CALLED WITH PAD HITS...
  std::vector<Cluster> clusters;
  std::vector<Hit>::iterator hits_itr =  hits.begin();
  std::vector<Hit>::iterator hits_end =  hits.end();

  std::sort(hits_itr, hits_end, cmpY);

  hits_itr =  hits.begin();
  hits_end =  hits.end();

  float y_previous = -9999;
  float y_current = 0;
  float pitch = 1;//3.2;
  //std::cout<< "TOTAL HITS LIST: " << hits.size() << "hits" <<std::endl;
  for (; hits_itr != hits_end; ++hits_itr)
    {
      //(*hits_itr).Dump();
      y_current = (*hits_itr).GetComponent().GetY();
      if (fabs(y_current - y_previous) <= (pitch + (1.0e-4)))
	{
	  Cluster& clr = (clusters.back());
	  clr.AddHit((*hits_itr));
	  y_previous = y_current;
	  continue;
	}

      Cluster cl;
      cl.AddHit((*hits_itr));
      clusters.push_back(cl);
      y_previous = y_current;

    }

  for (unsigned int i_cl = 0; i_cl < clusters.size(); i_cl++)
    {
      // NEED TO VALIDATE...
      if(GetDoGaussianClustering())
	clusters[i_cl].ComputeGaussianFit(gp);
      else
	clusters[i_cl].ComputeCentroid();

      // Correct for alignment
      //clusters[i_cl].CorrectForAlignment(in_parameters);

      // Correct for sine wave effect
      if(GetDoSWaveCorrection())
	clusters[i_cl].ApplySWaveCorrection();

      // Correct for relative strip shift between layers
      clusters[i_cl].CorrectForOffset();
      
      std::vector<Hit> cl_h = clusters[i_cl].GetHits();
      int layer = cl_h[0].GetComponent().GetLayer();
      tb_clusterLayer->push_back(layer);
      tb_clusterNhits->push_back(clusters[i_cl].GetNHits());
      tb_clusterX->push_back(clusters[i_cl].GetX());
      tb_clusterY->push_back(clusters[i_cl].GetY());
      tb_clusterYaligned->push_back(clusters[i_cl].GetYaligned());
      tb_clusterZ->push_back(clusters[i_cl].GetZ());
      tb_clusterTotalADC->push_back(clusters[i_cl].GetTotalAdc());

      //clusters[i_cl].Dump();
    }

  /*
  // TEMPORARY; ONLY USE CLUSTERS IN BEAM...
  std::vector<Cluster> clusters_inBeam;
  for (unsigned int i_cl = 0; i_cl < clusters.size(); i_cl++){
    double y = clusters[i_cl].GetY();
    //if(y > 245 && y < 280)
    if(!(y > 260 && y < 280))
      continue;
    clusters_inBeam.push_back(clusters[i_cl]);
  }
  return clusters_inBeam;
  */
  return clusters;
}



void ClusterBuilder::ClearTreeBranches() {
  tb_clusterNhits->clear();
  tb_clusterLayer->clear();
  tb_clusterX->clear();
  tb_clusterY->clear();
  tb_clusterYaligned->clear();
  tb_clusterZ->clear();
  tb_clusterTotalADC->clear();

}


void ClusterBuilder::FillTreeBranches() {
}

void ClusterBuilder::PreEventProcessing(){
  ClearTreeBranches();
}
void ClusterBuilder::PostEventProcessing(){
}
