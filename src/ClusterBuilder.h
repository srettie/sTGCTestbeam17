/*
 * ClusterBuilder.h
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov and Luca M
 */

#ifndef CLUSTERBUILDER_H_
#define CLUSTERBUILDER_H_

#include "TTree.h"

#include "IRecoTool.h"
#include "Cluster.h"
#include <vector>
#include <map>

class ClusterBuilder : public IRecoTool {
public:
	ClusterBuilder();
	virtual ~ClusterBuilder();

	bool Init();
	bool Finalize();

	std::vector<Cluster> BuildClustersForLayer(std::vector<Hit>& hits);//, std::vector<std::vector<float>> in_parameters);

	bool ComputeCentroid(Cluster);

	bool ComputeGaussianFit(Cluster);

	bool ApplySWaveCorrection(Cluster);

	int GetMaxHoles() const {
		return maxHoles;
	}

	void SetMaxHoles(int maxHoles) {
		this->maxHoles = maxHoles;
	}

	int GetMinHits() const {
		return minHits;
	}

	void SetMinHits(int minHits) {
		this->minHits = minHits;
	}

	float GetNeighborDistance() const {
		return neighborDistance;
	}

	void SetNeighborDistance(float neighborDistance) {
		this->neighborDistance = neighborDistance;
	}

	int GetHitNeighborWindow() const {
		return nHitNeighborWindow;
	}

	void SetHitNeighborWindow(int hitNeighborWindow) {
		nHitNeighborWindow = hitNeighborWindow;
	}

	bool GetDoSWaveCorrection() const {
	  return doSWaveCorrection;
	}
	
	void SetDoSWaveCorrection(bool doSWaveCorrection){
	  this->doSWaveCorrection = doSWaveCorrection;
	}

	bool GetDoGaussianClustering() const {
	  return doGaussianClustering;
	}

	void SetDoGaussianClustering(bool doGaussianClustering){
	  this->doGaussianClustering = doGaussianClustering;
	}

	TTree* GetTree() const {
		return tree;
	}

	void SetTree(TTree* tree) {
		this->tree = tree;
	}

	void SetGeometryProvider(GeometryProvider* in_gp){
	  this->gp = in_gp;
	}

	void PreEventProcessing();
	void PostEventProcessing();

protected:

	int minHits;
	int maxHoles;
	int nHitNeighborWindow;
	float neighborDistance;
	bool doSWaveCorrection;
	bool doGaussianClustering;

	void ClearTreeBranches();
	void FillTreeBranches();

	TTree* tree;

	GeometryProvider* gp;

	std::vector<int>* tb_clusterNhits;
	std::vector<int>* tb_clusterLayer;
	std::vector<float>* tb_clusterX;
	std::vector<float>* tb_clusterY;
	std::vector<float>* tb_clusterYaligned;
	std::vector<float>* tb_clusterZ;
	std::vector<int>* tb_clusterTotalADC;

};

#endif /* CLUSTERBUILDER_H_ */
