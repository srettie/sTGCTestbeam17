#include "QuadrupletGeometryProviderBuilder.h"
#include "ComponentDefs.h"

// layer		board pads	board strips
// 1			0			4
// 2			1			5
// 3			2			6
// 4			3			7

/////////////////////////////////////////////////////////////////////////////////////////////////////
// USING A RIGHT-HANDED COORDINATE SYSTEM WHERE THE BEAM GOES INTO THE POSITIVE Z DIRECTION
// THE ORIGIN OF THE COORDINATE SYSTEM IS IN THE BOTTOM RIGHT CORNER OF THE QUAD, AS SEEN BY THE BEAM
// FOR A GIVEN QUADRUPLET, BEAM HITS L1P/L1S/L2S/L2P L3P/L3S/L4S/L4P
/////////////////////////////////////////////////////////////////////////////////////////////////////

// General idea: start from max pad number (in excel mapping file) and go down.
// L1/L3: Loop from left to right in x and bottom to top in y.
// L2/L4: Loop from right to left in x and bottom to top in y.

void QL1PLYR1_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QL1PLYR1 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta12();
  int PHIMAX=gp->GetNPadsPhi12();
  int VMMBMIN=18;
  int VMMBMAX=63;
  int VMMCMIN=0;
  int VMMCMAX=55;
  int currVMM=VMMTYPEB;
  int LYR=1;
  int BOARD=LYR+5;//LYR-1;//TEMPORARY FOR 2.3a BOARDS
  int curr_channel=VMMBMIN;
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=1;iphi<=PHIMAX;iphi++){
      if(curr_channel==VMMBMAX+1){//VMM transition
	currVMM=VMMTYPEC;
	curr_channel=VMMCMIN;
      }
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //auto k=gp->padCorners(std::make_pair(ieta,iphi),LYR);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel++; 
    }
  }
}

void QL1PLYR2_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QL1PLYR2 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta12();
  int PHIMAX=gp->GetNPadsPhi12();
  int VMMBMIN=8;
  int VMMBMAX=63;
  int VMMCMIN=0;
  int VMMCMAX=45;
  int currVMM=VMMTYPEC;
  int curr_channel=VMMCMAX;
  int LYR=2;
  int BOARD=LYR+5;//LYR-1;//TEMPORARY FOR 2.3a BOARDS
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=PHIMAX;iphi>=1;iphi--){
      if(curr_channel==VMMCMIN){
	//jump to BMAX;
	currVMM=VMMTYPEB;
	curr_channel=VMMBMAX;
      }
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel--;
    }
  }
}


void QL1PLYR3_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QL1PLYR3 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta34();
  int PHIMAX=gp->GetNPadsPhi34();
  int VMMBMIN=8;
  int VMMBMAX=63;
  int VMMCMIN=0;
  int VMMCMAX=55;
  int currVMM=VMMTYPEC;
  int LYR=3;
  int BOARD=LYR+5;//LYR-1;//TEMPORARY FOR 2.3a BOARDS
  int curr_channel=VMMCMAX;
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=1;iphi<=PHIMAX;iphi++){
      if(curr_channel==VMMCMIN-1){//VMM transition
	currVMM=VMMTYPEB;
	curr_channel=VMMBMAX;
      }
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel--; 
    }
  }
}

void QL1PLYR4_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QL1PLYR4 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta34();
  int PHIMAX=gp->GetNPadsPhi34();
  int VMMBMIN=8;
  int VMMBMAX=63;
  int VMMCMIN=0;
  int VMMCMAX=55;
  int currVMM=VMMTYPEB;
  int LYR=4;
  int BOARD=LYR+5;//LYR-1;//TEMPORARY FOR 2.3a BOARDS
  int curr_channel=VMMBMIN;
  for(int ieta=1;ieta<=ETAMAX;ieta++){//rows
    for(int iphi=PHIMAX;iphi>=1;iphi--){//columns
      if(curr_channel==VMMBMAX+1){//VMM transition
	currVMM=VMMTYPEC;
	curr_channel=VMMCMIN;
      }
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel++;
    }
  }
}

void QL1PLYR14_strips( GeometryProvider* gp, int layer, double z_strips){
  std::cout<<"========== Adding QL1PLYR"<<layer<<" Strips =========="<<std::endl;
  int board = layer + 6;//layer + 3;//TEMPORARY FOR 2.3a BOARDS
  double x_ch = 0;
  double z_ch = z_strips;  
  // Insert strips by increasing strip number (bottom to top)
  // Strip number will increase only when VMM/CH combination is used
  int curr_strip = 1;
  // Need to reject strip channels that are not connected...
  std::map<int, int> vmm_offChansStart;
  std::map<int, int> vmm_offChansEnd;
  //vmm, {offChan_start, offChan_end}
  vmm_offChansStart[0] = -1;
  vmm_offChansEnd[0] = -1;
  vmm_offChansStart[1] = -1;
  vmm_offChansEnd[1] = -1;
  vmm_offChansStart[2] = -1;
  vmm_offChansEnd[2] = -1;
  vmm_offChansStart[3] = -1;
  vmm_offChansEnd[3] = -1;
  vmm_offChansStart[4] = -1;
  vmm_offChansEnd[4] = -1;
  vmm_offChansStart[5] = 16;
  vmm_offChansEnd[5] = 47;
  vmm_offChansStart[6] = 16;
  vmm_offChansEnd[6] = 47;
  vmm_offChansStart[7] = 24;
  vmm_offChansEnd[7] = 63;
  // VMM numbers will decrease from 7 --> 0
  for(int i_vmm = 7; i_vmm >= 0; i_vmm--){
    // For each VMM, channel number will decrease from 63 --> 0
    for(int i_ch = 63; i_ch >= 0; i_ch--){
      // Check if this channel/VMM is needed
      if(!((i_ch >= vmm_offChansStart[i_vmm]) && (i_ch <= vmm_offChansEnd[i_vmm]))){
	//std::cout<<"Adding board = "<<board<<", i_vmm = "<<i_vmm<<", i_ch = "<<i_ch<<", x_ch = "<<x_ch<<", y_ch = "<<curr_strip<<", z_ch = "<<z_ch<<", layer = "<<layer<<std::endl;
	gp->AddComponent(board, i_vmm, i_ch, x_ch, curr_strip, z_ch, layer, StripComponent);
	curr_strip++;
      }
    }
  }
}

void QL1PLYR23_strips( GeometryProvider* gp, int layer, double z_strips){
  std::cout<<"========== Adding QL1PLYR"<<layer<<" Strips =========="<<std::endl;
  int board = layer + 6;//layer + 3;//TEMPORARY FOR 2.3a BOARDS
  double x_ch = 0;
  double z_ch = z_strips;  
  // Insert strips by increasing strip number (bottom to top)
  // Strip number will increase only when VMM/CH combination is used
  int curr_strip = 1;
  // Need to reject strip channels that are not connected...
  std::map<int, int> vmm_offChansStart;
  std::map<int, int> vmm_offChansEnd;
  //vmm, {offChan_start, offChan_end}
  vmm_offChansStart[0] = 0;
  vmm_offChansEnd[0] = 39;
  vmm_offChansStart[1] = 16;
  vmm_offChansEnd[1] = 47;
  vmm_offChansStart[2] = 16;
  vmm_offChansEnd[2] = 47;
  vmm_offChansStart[3] = -1;
  vmm_offChansEnd[3] = -1;
  vmm_offChansStart[4] = -1;
  vmm_offChansEnd[4] = -1;
  vmm_offChansStart[5] = -1;
  vmm_offChansEnd[5] = -1;
  vmm_offChansStart[6] = -1;
  vmm_offChansEnd[6] = -1;
  vmm_offChansStart[7] = -1;
  vmm_offChansEnd[7] = -1;
  // VMM numbers will increase from 0 --> 7
  for(int i_vmm = 0; i_vmm <= 7; i_vmm++){
    // For each VMM, channel number will increase from 0 --> 63
    for(int i_ch = 0; i_ch <= 63; i_ch++){
      // Check if this channel/VMM is needed
      if(!((i_ch >= vmm_offChansStart[i_vmm]) && (i_ch <= vmm_offChansEnd[i_vmm]))){
	//std::cout<<"Adding board = "<<board<<", i_vmm = "<<i_vmm<<", i_ch = "<<i_ch<<", x_ch = "<<x_ch<<", y_ch = "<<curr_strip<<", z_ch = "<<z_ch<<", layer = "<<layer<<std::endl;
	gp->AddComponent(board, i_vmm, i_ch, x_ch, curr_strip, z_ch, layer, StripComponent);
	curr_strip++;
      }
    }
  }
}

// layer		board pads	board strips
// 1			4			0
// 2			5			1
// 3			6			2
// 4			7			3

void QS3PLYR1_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QS3PLYR1 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta12();
  int PHIMAX=gp->GetNPadsPhi12();
  int VMMCMIN=9;
  int VMMCMAX=32;
  int currVMM=VMMTYPEC;
  int LYR=1;
  int BOARD=LYR+3;
  int curr_channel=VMMCMIN;
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=1;iphi<=PHIMAX;iphi++){
      // No VMM transition; all on VMMC
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //auto k=gp->padCorners(std::make_pair(ieta,iphi),LYR);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel++; 
    }
  }
}

void QS3PLYR2_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QS3PLYR2 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta12();
  int PHIMAX=gp->GetNPadsPhi12();
  int VMMCMIN=9;
  int VMMCMAX=32;
  int currVMM=VMMTYPEC;
  int curr_channel=VMMCMAX;
  int LYR=2;
  int BOARD=LYR+3;
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=PHIMAX;iphi>=1;iphi--){
      // No VMM transition; all on VMMC
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel--;
    }
  }
}

void QS3PLYR3_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QS3PLYR3 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta34();
  int PHIMAX=gp->GetNPadsPhi34();
  int VMMCMIN=2;
  int VMMCMAX=40;
  int currVMM=VMMTYPEC;
  int LYR=3;
  int BOARD=LYR+3;
  int curr_channel=VMMCMAX;
  for(int ieta=1;ieta<=ETAMAX;ieta++){
    for(int iphi=1;iphi<=PHIMAX;iphi++){
      // No VMM transition; all on VMMC
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel--; 
    }
  }
}

void QS3PLYR4_pads( GeometryProvider* gp, double z_pads){
  std::cout<<"========== Adding QS3PLYR4 Pads =========="<<std::endl;
  int ETAMAX=gp->GetNPadsEta34();
  int PHIMAX=gp->GetNPadsPhi34();
  int VMMCMIN=1;
  int VMMCMAX=39;
  int currVMM=VMMTYPEC;
  int LYR=4;
  int BOARD=LYR+3;
  int curr_channel=VMMCMIN;
  for(int ieta=1;ieta<=ETAMAX;ieta++){//rows
    for(int iphi=PHIMAX;iphi>=1;iphi--){//columns
      // No VMM transition; all on VMMC
      gp->AddComponent(BOARD,currVMM,curr_channel,iphi,ieta,z_pads,LYR,PadComponent);
      gp->padIndices()[{BOARD,currVMM,curr_channel,LYR}]=std::make_pair(ieta,iphi);
      //std::cout<<"board="<<BOARD<<" VMM="<<currVMM<<" channel="<<curr_channel<<" iphi="<<iphi<<" ieta="<<ieta<<" z="<<z_pads<<" layer="<<LYR<<std::endl;
      curr_channel++;
    }
  }
}

void QS3PLYR14_strips( GeometryProvider* gp, int layer, double z_strips){
  std::cout<<"========== Adding QS3PLYR"<<layer<<" Strips =========="<<std::endl;
  int board = layer - 1;
  double x_ch = 0;
  double z_ch = z_strips;
  // Insert strips by increasing strip number (bottom to top)
  // Strip number will increase only when VMM/CH combination is used
  int curr_strip = 1;
  // VMM numbers will decrease from 7 --> 3
  for(int i_vmm = 7; i_vmm >= 3; i_vmm--){
    // For each VMM, channel number will decrease from 63 --> 0
    for(int i_ch = 63; i_ch >= 0; i_ch--){
      // Check if this channel/VMM is needed
      if((i_vmm == 3) && (i_ch <= 12))
	continue;
      //std::cout<<"Adding board = "<<board<<", i_vmm = "<<i_vmm<<", i_ch = "<<i_ch<<", x_ch = "<<x_ch<<", y_ch = "<<curr_strip<<", z_ch = "<<z_ch<<", layer = "<<layer<<std::endl;
      gp->AddComponent(board, i_vmm, i_ch, x_ch, curr_strip, z_ch, layer, StripComponent);
      curr_strip++;
    }
  }
}

void QS3PLYR23_strips( GeometryProvider* gp, int layer, double z_strips){
  std::cout<<"========== Adding QS3PLYR"<<layer<<" Strips =========="<<std::endl;
  int board = layer - 1;
  double x_ch = 0;
  double z_ch = z_strips;
  // Insert strips by increasing strip number (bottom to top)
  // Strip number will increase only when VMM/CH combination is used
  int curr_strip = 1;
  // VMM numbers will increase from 3 --> 7
  for(int i_vmm = 3; i_vmm <= 7; i_vmm++){
    // For each VMM, channel number will increase from 0 --> 63
    for(int i_ch = 0; i_ch <= 63; i_ch++){
      // Check if this channel/VMM is needed
      if((i_vmm == 3) && (i_ch <= 12))
	continue;
      //std::cout<<"Adding board = "<<board<<", i_vmm = "<<i_vmm<<", i_ch = "<<i_ch<<", x_ch = "<<x_ch<<", y_ch = "<<curr_strip<<", z_ch = "<<z_ch<<", layer = "<<layer<<std::endl;
      gp->AddComponent(board, i_vmm, i_ch, x_ch, curr_strip, z_ch, layer, StripComponent);
      curr_strip++;      
    }
  }
}
