#ifndef COMPONENT_DEFS_H
#define COMPONENT_DEFS_H

#define VMMTYPEA 0
#define VMMTYPEB 1
#define VMMTYPEC 2

//S.I 03.09.18

class GeometryProvider;

void QL1PLYR1_pads( GeometryProvider* gp, double z_pads);
void QL1PLYR2_pads( GeometryProvider* gp, double z_pads);
void QL1PLYR3_pads( GeometryProvider* gp, double z_pads);
void QL1PLYR4_pads( GeometryProvider* gp, double z_pads);

void QL1PLYR14_strips( GeometryProvider* gp, int layer, double z_strips);
void QL1PLYR23_strips( GeometryProvider* gp, int layer, double z_strips);

void QS3PLYR1_pads( GeometryProvider* gp, double z_pads);
void QS3PLYR2_pads( GeometryProvider* gp, double z_pads);
void QS3PLYR3_pads( GeometryProvider* gp, double z_pads);
void QS3PLYR4_pads( GeometryProvider* gp, double z_pads);

void QS3PLYR14_strips( GeometryProvider* gp, int layer, double z_strips);
void QS3PLYR23_strips( GeometryProvider* gp, int layer, double z_strips);


#endif
