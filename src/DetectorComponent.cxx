/*
 * DetectorComponent.cpp
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov and Luca M
 */

#include "DetectorComponent.h"


DetectorComponent::DetectorComponent():
board(-1),
vmm(-1),
ch(-1),
layer(-1),
x(0),
y(0),
z(0),
theta(0),
phi(0),
width(0),
length(0),
type(-1){

}

DetectorComponent::~DetectorComponent() {

}

DetectorComponent::DetectorComponent(int in_board,
				     int in_vmm,
				     int in_ch,
				     int in_layer,
				     int in_type,
				     float in_x,
				     float in_y,
				     float in_z,
				     float in_theta,
				     float in_phi,
				     float in_width,
				     float in_length):
board(in_board),
vmm(in_vmm),
ch(in_ch),
layer(in_layer),
type(in_type),
x(in_x),
y(in_y),
z(in_z),
theta(in_theta),
phi(in_phi),
width(in_width),
length(in_length){

}

DetectorComponent::DetectorComponent(const DetectorComponent &comp){
  board = comp.board;
  vmm = comp.vmm;
  ch = comp.ch;
  layer = comp.layer;
  type = comp.type;
  x = comp.x; 
  y = comp.y; 
  z = comp.z; 
  theta = comp.theta; 
  phi = comp.phi; 
  width = comp.width; 
  length = comp.length; 
}
