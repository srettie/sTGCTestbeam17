/*
 * DetectorComponent.h
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov & luca m
 */

#ifndef DETECTORCOMPONENT_H_
#define DETECTORCOMPONENT_H_

#include <iostream>

enum ComponentType {PadComponent, StripComponent, WireComponent};

class DetectorComponent {
 public:
  DetectorComponent();
  DetectorComponent(int in_board,
		    int in_vmm,
		    int in_ch,
		    int in_layer,
		    int in_type,
		    float in_x,
		    float in_y,
		    float in_z,
		    float in_theta,
		    float in_phi,
		    float in_width,
		    float in_length);
  DetectorComponent(const DetectorComponent &comp);
  virtual ~DetectorComponent();

  int GetBoard() const {
    return board;
  }

  bool isPad() const {
    return (type == PadComponent);
  }

  bool isStrip() const {
    return (type == StripComponent);
  }

  bool isWire() const {
    return (type == WireComponent);
  }

  void SetBoard(int board) {
    this->board = board;
  }

  int GetCh() const {
    return ch;
  }

  void SetCh(int ch) {
    this->ch = ch;
  }

  float GetLength() const {
    return length;
  }

  void SetLength(float length) {
    this->length = length;
  }

  float GetPhi() const {
    return phi;
  }

  void SetPhi(float phi) {
    this->phi = phi;
  }

  float GetTheta() const {
    return theta;
  }

  void SetTheta(float theta) {
    this->theta = theta;
  }

  int GetVmm() const {
    return vmm;
  }

  void SetVmm(int vmm) {
    this->vmm = vmm;
  }

  float GetWidth() const {
    return width;
  }

  void SetWidth(float widht) {
    this->width = widht;
  }

  float GetX() const {
    return x;
  }

  void SetX(float x) {
    this->x = x;
  }

  float GetY() const {
    return y;
  }

  void SetY(float y) {
    this->y = y;
  }

  float GetZ() const {
    return z;
  }

  void SetZ(float z) {
    this->z = z;
  }

  int GetLayer() const {
    return layer;
  }

  void SetLayer(int layer) {
    this->layer = layer;
  }
  int GetType() const {
    return type;
  }

  void SetType(int type) {
    this->type = type;
  }

  void Dump() const {
    std::cout << "Component dump - Board: " << board << " vmm: " << vmm << " ch: " << ch;
    std::cout << " x: " << x << "mm y: " << y << "mm z: " << z << " layer: " << layer << std::endl;
  }
 protected:
  int board;
  int vmm;
  int ch;
  int layer;
  int type; // enum for pad/strip/wire
  float x; // position with respect to axis parallel to the direction of the strips in mm
  float y; // position with respect to axis perpendicular to the direction of strips in mm
  float z; // position with respect to axis parallel to beamline in mm
  float theta; // angle with respect to the beamline axis
  float phi; // azimuthal angle
  float width; // dimension in the y direction in mm
  float length; // dimension in the x direction in mm
};

#endif /* DETECTORCOMPONENT_H_ */
