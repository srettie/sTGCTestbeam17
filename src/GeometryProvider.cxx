/*
 * GeometryProvider.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: ekajomov and Luca M
 */

#include "GeometryProvider.h"
#include "TText.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TMath.h"

GeometryProvider::GeometryProvider() {
	// TODO Auto-generated constructor stub

}

GeometryProvider::~GeometryProvider() {
	// TODO Auto-generated destructor stub
}

DetectorComponent GeometryProvider::GetComponent(int feb, int vmm, int channel) {
	DetectorComponent c;
	c = m_readout2geo[GetKey(feb, vmm, channel)];
	return c;
}

std::map<long, DetectorComponent> GeometryProvider::GetGeoMap(){
  return m_readout2geo;
}

void GeometryProvider::SetQuadType(std::string type)
{
  m_quad_type = type;
  if(type=="QL1P"){
    m_nstrips = 408;
    m_npads_phi12 = 6;
    m_npads_phi34 = 7;
    m_npads_eta12 = 17;
    m_npads_eta34 = 16;
  }
  else if(type=="QS3P"){
    m_nstrips = 307;
    m_npads_phi12 = 2;
    m_npads_phi34 = 3;
    m_npads_eta12 = 12;
    m_npads_eta34 = 13;
  }
  else{
    // TODO - ADD ACTUAL ERROR HANDLING HERE...
    std::cout<<"ERROR: Quad type \""<<type<<"\" not found!!"<<std::endl;
  }

}

std::string GeometryProvider::GetQuadType()
{
  return m_quad_type;
}
int GeometryProvider::GetNStrips()
{
  return m_nstrips;
}
int GeometryProvider::GetNPadsPhi12()
{
  return m_npads_phi12;
}
int GeometryProvider::GetNPadsPhi34()
{
  return m_npads_phi34;
}
int GeometryProvider::GetNPadsEta12()
{
  return m_npads_eta12;
}
int GeometryProvider::GetNPadsEta34()
{
  return m_npads_eta34;
}

void GeometryProvider::AddComponent(int feb, int vmm, int channel, float x, float y,
				    float z, int layer, int type) {
	DetectorComponent c;
	c.SetX(x);
	c.SetY(y);
	c.SetZ(z);
	c.SetBoard(feb);
	c.SetVmm(vmm);
	c.SetCh(channel);
	c.SetLayer(layer);
	c.SetType(type);

	m_readout2geo[GetKey(feb,vmm,channel)] = c;
}

long GeometryProvider::GetKey(int feb, int vmm, int channel) {
	if (channel < 0 || feb < 0 || vmm < 0)
		return 0;
	long key = feb * 1e6 + vmm * 1e4 + channel;
	return key;
}

std::array<std::array<double,2>,4> GeometryProvider::padCorners(int ieta,int iphi ,
                                                   int layer,
                                                   int side,
                                                   int sectorNo,
                                                   int secType,
								int moduleNo){
  //A parametric descrption of sTGC Pad geometry
        
  /*Calculate pad corners in the local coordinate system
   * the technique is borrowed from :
   * MuonSpectrometer/MuonDetDescr/MuonReadoutGeometry/src/sTgcReadoutElement.cxx
   * MuonSpectrometer/MuonDetDescr/MuonReadoutGeometry/src/MuonPadDesign.cxx
   * and design parameters are  from : MuonSpectrometer/MuonG4/NSW_Sim/data/stations.v2.02.xml
   */
     
        
  double radialDistance=0;//distance from the beam axis to the module/lyr centre. Can be set to zero for a TB setup ?
   
  //those parameters all should be read from the official xml / MuonG4
  //QL1P Parameters
  double yCutout=0;
  double length=1332;
  double ylFrame=14;
  double ysFrame=11;
  double sPadWidth=476.42;
  double lPadWidth=1126.66;
  int PadPhiShift[]={-2,2,-2,2};
  double Size=length - ylFrame - ysFrame;
  double firstPadH[] =  {54.39,54.39,98,15,98.15};
  double padH[]={81.42,81.42,81.66,81.66};
  double firsPadDivision[]={-10,-10,-12.5,-12.5};
  int nPadPhi[]={6,6,7,7};
  int nPadH[]={17,17,16,16};
  double anglePadPhi=5;
   
   
  double firstRowPos=firstPadH[layer];
  double inputRowPitch=padH[layer];
  double firstPhiPos=firsPadDivision[layer];
  int nPadColumns=nPadPhi[layer];
  double npadh=nPadH[layer];
  double inputPhiPitch=anglePadPhi;
  double padphishift=PadPhiShift[layer];
   

  double minSensitiveY =yCutout ? -(Size-yCutout) : -0.5*Size;

  double maxSensitiveY = yCutout ? yCutout : 0.5*Size;

  double yBot = 0., yTop = 0.;
  if(ieta == 1) { 
    yBot = yCutout ? -(Size - yCutout) : -0.5*Size;
    yTop = yBot + firstRowPos; 
  }
  else if(ieta > 1) { 
    yBot = yCutout ? -(Size - yCutout) + firstRowPos + (ieta-2)*inputRowPitch : -0.5*Size + firstRowPos + (ieta-2)*inputRowPitch;
    yTop = yBot + inputRowPitch;
    if(ieta == npadh)
      yTop = maxSensitiveY;
  }
  else { // Unkwown ieta
    return std::array<std::array<double,2>,4>();
  }
  ////// ASM-2015-12-07 : New Implementation

  //std::cout <<"\tMuonPadDesign::channelPosition iEta " << iEta << " inputRowPitch "  <<  inputRowPitch << " yBot " << yBot << " yTop " << yTop<<std::endl;

  // restrict y to the module sensitive area
  double minY = minSensitiveY;
  double maxY = maxSensitiveY;
  //std::cout <<"\tMuonPadDesign::channelPosition minSensitiveY " << minY << " maxSensitiveY " << maxY<<std::endl;
  if(yBot<minY) yBot = minY;
  if(yTop>maxY) yTop = maxY;

  // here L/R are defined as if you were looking from the IP to the
  // detector (same a clockwise/counterclockwise phi but shorter)
  double phiRight  = firstPhiPos + (iphi-2)*inputPhiPitch;
  double phiLeft   = firstPhiPos + (iphi-1)*inputPhiPitch;
  double xBotRight = -1*(yBot+radialDistance) * TMath::Tan(M_PI*phiRight/180.) + padphishift;
  double xBotLeft  = -1*(yBot+radialDistance) * TMath::Tan(M_PI*phiLeft/180.) + padphishift;
  double xTopRight = -1*(yTop+radialDistance) * TMath::Tan(M_PI*phiRight/180.) + padphishift;
  double xTopLeft  = -1*(yTop+radialDistance) * TMath::Tan(M_PI*phiLeft/180.) + padphishift;
    
  //Adjust outer columns
  if(iphi == 1){
    double yLength = yCutout ? Size - yCutout : Size;
    xBotRight = 0.5*(sPadWidth + (lPadWidth-sPadWidth)*(yBot-minY)/yLength);
    xTopRight = 0.5*(sPadWidth + (lPadWidth-sPadWidth)*(yTop-minY)/yLength);
  }
  if(iphi == nPadColumns){
    double yLength = yCutout ? Size - yCutout : Size;
    xBotLeft = -0.5*(sPadWidth + (lPadWidth-sPadWidth)*(yBot-minY)/yLength);
    xTopLeft = -0.5*(sPadWidth + (lPadWidth-sPadWidth)*(yTop-minY)/yLength);
  }

  //Adjust for cutout region
  if(yCutout && yTop > 0){
    float cutoutXpos = 0.5*lPadWidth;
    if(iphi == 1){
      xTopRight = cutoutXpos;
      if(yBot > 0)
	xBotRight = cutoutXpos;
    }
    else if (iphi == nPadColumns){
      xTopLeft = -1.0*cutoutXpos;
      if(yBot > 0)
	xBotLeft = -1.0*cutoutXpos;
    }
  }
  std::cout<<"bottomleft "<<"        bottomright"<<"      topleft "<<"     topright"<<std::endl;
  std::cout<<"["<<xBotLeft<<" "<<yBot<<"] ["<<xBotRight<<" "<<yBot<<"] ["<<xTopLeft<<" "<<yTop<<"] ["<<xTopRight<<" "<<yTop<<"]"<<std::endl;
    
    
  /*
    corners.push_back(Amg::Vector2D(xBotLeft,yBot));
    corners.push_back(Amg::Vector2D(xBotRight,yBot));
    corners.push_back(Amg::Vector2D(xTopLeft,yTop));
    corners.push_back(Amg::Vector2D(xTopRight,yTop));
    //std::cout << "MuonPadDesign::channelPosition padEta " << pad.first << " padPhi " << pad.second << " x " << xCenter << " y " << yCenter << std::endl;
    */
  return std::array<std::array<double,2>,4>();
        
        
        
        
        
        
        
}

