/*
 * GeometryProvider.h
 *
 *  Created on: Sep 20, 2017
 *      Author: ekajomov
 */

#ifndef GEOMETRYPROVIDER_H_
#define GEOMETRYPROVIDER_H_

#include <map>
#include <unordered_map>
#include <utility>
#include <array>

#include "DetectorComponent.h"

#define VMMTYPEB 1
#define VMMTYPEC 2

using PadChannels = std::array<int,4>;//board VMMTYPE VMMchannel  layer
using EtaPhi = std::pair<int,int>;
using PadMap = std::map<PadChannels,EtaPhi>;

class DetectorComponent;

class GeometryProvider {
 public:
  GeometryProvider();
  virtual ~GeometryProvider();
  DetectorComponent GetComponent(int feb, int vmm, int channel);
  void AddComponent(int feb, int vmm, int channel, float x, float y, float z, int layer, int type);
  std::map<long, DetectorComponent> GetGeoMap();
  void SetQuadType(std::string type);
  std::string GetQuadType();
  int GetNStrips();
  int GetNPadsPhi12();
  int GetNPadsPhi34();
  int GetNPadsEta12();
  int GetNPadsEta34();
  
  PadMap& padIndices() {return m_indices;}
  //side 0=C 1=A / secType 0=S 1=L / layer={1...8}
  std::array<std::array<double,2>,4> padCorners(int ieta,int iphi ,int layer,int side=1, int sectorNo=1,int  secType=1,int moduleNo=1);
    
 protected:

  long GetKey(int feb, int vmm, int channel);

  std::map<long, DetectorComponent> m_readout2geo;
  PadMap m_indices;
  std::string m_quad_type;
  int m_nstrips;
  int m_npads_phi12;
  int m_npads_phi34;
  int m_npads_eta12;
  int m_npads_eta34;
};

#endif /* GEOMETRYPROVIDER_H_ */
