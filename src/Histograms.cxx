#include "RecoManager.h"


void RecoManager::BookHistograms(GeometryProvider* gp){
  ////////////////////
  // Output histograms
  // Naming convention for downstream plotting macro: "[h/h2]_[pads/strips]_<quantity>_<optional cut>_layerX"
  /////////////
  
  // Variables independant of quad type
  int nlayers = 4;
  int nchan = 64;
  int nvmms = 8;
  int nuid = 800; // number of unique identifiers for VMM/CH
  int nbins_hits = 15;
  int nbins_pdo = 1024;
  int nbins_tdo = 256;
  int nbins_tdo_calibrated = 200;
  int nbins_bcid = 4096;
  int nbins_pdo_ratio = 100;
  int counts_per_bcid = 80;// Best guess for now
  int nbins_strips_clusters = 10;
  int nbins_strips_per_cluster = 15;
  int nbins_strips_tracks = 5;
  int nbins_strips_trackSlope = 2000;
  int nbins_strips_trackSlopeRange = 2;
  int nbins_strips_trackIntercept = 5000;
  int nbins_residuals = 250;
  int nbins_bcid_rel = 8;
  double range_residuals = 2;
  double range_tdo_calibrated = 100;
  double strip_pitch = 3.2;

  // Quad-specific variables
  int nbins_strips = gp->GetNStrips();
  int nbins_pads_x12 = gp->GetNPadsPhi12();
  int nbins_pads_y12 = gp->GetNPadsEta12();
  int nbins_pads_x34 = gp->GetNPadsPhi34();
  int nbins_pads_y34 = gp->GetNPadsEta34();

  // Loop over components of geometry provider
  std::map<long, DetectorComponent> geomap = GetGeometryProvider()->GetGeoMap();
  for( const auto& M : geomap){
    auto c=M.second;

    if(!c.isPad() && !c.isStrip())
      continue;

    std::string name = "";
    if(c.isStrip()){
      name = "h_strips_tdo_calibrated_l"+std::to_string(c.GetLayer())+"_y"+std::to_string(int(c.GetY()));
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO (ns);Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));
    }

    if(c.isPad()){
      // Want to plot pdo and tdo for each pad
      name = "h_pads_pdo_l"+std::to_string(c.GetLayer())+"_x"+std::to_string(int(c.GetX()))+"_y"+std::to_string(int(c.GetY()));
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";PDO;Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_tdo_calibrated_l"+std::to_string(c.GetLayer())+"_x"+std::to_string(int(c.GetX()))+"_y"+std::to_string(int(c.GetY()));
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO (ns);Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));
    }
  }
  
  h1.insert(std::make_pair("h_pads_nFiringLayers", new TH1F("h_pads_nFiringLayers", "h_pads_nFiringLayers;Number of fired pad layers;Events", nlayers+1, 0, nlayers+1)));
  h1.insert(std::make_pair("h_strips_nFiringLayers", new TH1F("h_strips_nFiringLayers", "h_strips_nFiringLayers;Number of fired strip layers;Events", nlayers+1, 0, nlayers+1)));
  
  std::string name = "";
  for(int i = 1; i < 5; ++i){
    std::string si = std::to_string(i);
    std::string l = "_layer" + si;

    // Pads
    name = "h_pads_nHits"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Number of pad hits in layer "+si+";Events").c_str(), nbins_hits, 0, nbins_hits)));

    name = "h_pads_pdo"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

    name = "h_pads_tdo"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";TDO in pad layer "+si+";Events").c_str(), nbins_tdo, 0, nbins_tdo)));

    name = "h_pads_bcid_rel"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";bcid_rel in pad layer "+si+";Events").c_str(), nbins_bcid_rel, 0, nbins_bcid_rel)));

    name = "h_pads_bcid_rel_in_time_peak"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";bcid_rel in-time peak used in pad layer "+si+";Events").c_str(), nbins_bcid_rel, 0, nbins_bcid_rel)));

    name = "h_pads_tdo_calibrated"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO in pad layer "+si+" (ns);Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));
    
    name = "h2_pads_hitsProfile"+l;
    if(i == 1 || i == 2)
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
					      nbins_pads_x12, 1, nbins_pads_x12+1, nbins_pads_y12, 1, nbins_pads_y12+1)));
    else if (i == 3 || i == 4)
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
					      nbins_pads_x34, 1, nbins_pads_x34+1, nbins_pads_y34, 1, nbins_pads_y34+1)));

    name = "h2_pads_hitsProfileVMMChannel"+l;
    h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";VMM channel in layer "+si+";VMM number in layer "+si+";").c_str(), nchan, 0, nchan, nvmms, 0, nvmms)));

    name = "h2_pads_chargeVsKey"+l;
    h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Unique pad channel key in layer "+si+";PDO").c_str(), nuid, 0, nuid, nbins_pdo, 0, nbins_pdo)));

    name = "h2_pads_pdo_vs_tdo_calibrated"+l;
    h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Calibrated TDO in pad layer "+si+";ADC pad charge in layer "+si+";").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated, nbins_pdo, 0, nbins_pdo)));
    
    // Strips
    name = "h_strips_nHits"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Number of strip hits in layer "+si+";Events").c_str(), nbins_hits, 0, nbins_hits)));

    name = "h_strips_pdo"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC strip charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

    name = "h_strips_tdo"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";TDO in strip layer "+si+";Events").c_str(), nbins_tdo, 0, nbins_tdo)));

    name = "h_strips_tdo_calibrated"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO in strip layer "+si+" (ns);Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));

    name = "h_strips_bcid_rel"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";bcid_rel in strip layer "+si+";Events").c_str(), nbins_bcid_rel, 0, nbins_bcid_rel)));

    name = "h_strips_bcid_rel_in_time_peak"+l;
    h1.insert(std::make_pair(name,  new TH1F(name.c_str(), (name+";bcid_rel in-time peak used in strip layer "+si+";Events").c_str(), nbins_bcid_rel, 0, nbins_bcid_rel)));

    name = "h_strips_hitsProfile"+l;
    h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Strip y position in layer "+si+";Counts").c_str(), nbins_strips, 1, nbins_strips+1)));

    name = "h2_strips_chargeVsKey"+l;
    h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Unique strip channel key in layer "+si+";PDO").c_str(), nuid, 0, nuid, nbins_pdo, 0, nbins_pdo)));

    // Book layer-specific histograms for full-blown analysis
    if(!doFast){

      // Pads      
      name = "h_pads_pdo_multcut"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_pdo_mult2_maxHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_pdo_mult2_minHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_pdo_maxHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_tdo_calibrated_maxHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO in pad layer "+si+";Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));

      name = "h_pads_pdo_subLead"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_tdo_calibrated_subLead"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO in pad layer "+si+";Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));

      name = "h_pads_pdo_ratio_subLeadOverMaxHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";#frac{Subleading hit PDO}{Leading hit PDO};Events").c_str(), nbins_pdo_ratio, 0, 1)));

      name = "h_pads_pdo_subSubLead"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC pad charge in layer "+si+";Events").c_str(), nbins_pdo, 0, nbins_pdo)));

      name = "h_pads_tdo_calibrated_subSubLead"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Calibrated TDO in pad layer "+si+";Events").c_str(), nbins_tdo_calibrated, -range_tdo_calibrated, range_tdo_calibrated)));

      name = "h_pads_pdo_ratio_subSubLeadOverMaxHit"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";#frac{SubSubleading hit PDO}{Leading hit PDO};Events").c_str(), nbins_pdo_ratio, 0, 1)));

      name = "h2_pads_hitsProfile_mult2_maxHit"+l;
      if(i == 1 || i == 2)
	h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
						nbins_pads_x12, 1, nbins_pads_x12+1, nbins_pads_y12, 1, nbins_pads_y12+1)));
      else if (i == 3 || i == 4)
	h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
						nbins_pads_x34, 1, nbins_pads_x34+1, nbins_pads_y34, 1, nbins_pads_y34+1)));

      name = "h2_pads_hitsProfile_mult2_minHit"+l;
      if(i == 1 || i == 2)
	h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
						nbins_pads_x12, 1, nbins_pads_x12+1, nbins_pads_y12, 1, nbins_pads_y12+1)));
      else if (i == 3 || i == 4)
	h2.insert(std::make_pair(name, new TH2F(name.c_str(), ("Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(),
						nbins_pads_x34, 1, nbins_pads_x34+1, nbins_pads_y34, 1, nbins_pads_y34+1)));

      name = "h2_pads_hitsProfileVMMChannel_multcut"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";VMM channel in layer "+si+";VMM number in layer "+si+";").c_str(), nchan, 0, nchan, nvmms, 0, nvmms)));

      name = "h2_pads_hitsProfileVMMChannel_maxHit"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";VMM channel in layer "+si+";VMM number in layer "+si+";").c_str(), nchan, 0, nchan, nvmms, 0, nvmms)));

      name = "h2_pads_nHits_vs_pdo_maxHit"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";ADC pad max charge in layer "+si+";Number of pad hits in layer "+si+";").c_str(), nbins_pdo, 0, nbins_pdo, nbins_hits, 0, nbins_hits)));

      // Strips clusters
      name = "h_strips_nClusters"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Number of clusters in strip layer "+si+";Events").c_str(), nbins_strips_clusters, 0, nbins_strips_clusters)));

      name = "h_strips_stripsPerCluster"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Strips per cluster in layer "+si+";Counts").c_str(), nbins_strips_per_cluster, 0, nbins_strips_per_cluster)));

      name = "h_strips_chargePerCluster"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC charge per cluster in layer "+si+";Counts").c_str(), nbins_pdo*nbins_strips_clusters, 0, nbins_pdo*nbins_strips_clusters)));

      name = "h_strips_chargePerCluster_1strip"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC charge per cluster (1 strip) in layer "+si+";Counts").c_str(), nbins_pdo*nbins_strips_clusters, 0, nbins_pdo*nbins_strips_clusters)));

      name = "h_strips_chargePerCluster_2strip"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC charge per cluster (2 strips) in layer "+si+";Counts").c_str(), nbins_pdo*nbins_strips_clusters, 0, nbins_pdo*nbins_strips_clusters)));

      name = "h_strips_chargePerCluster_multstrip"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";ADC charge per cluster (>2 strips) in layer "+si+";Counts").c_str(), nbins_pdo*nbins_strips_clusters, 0, nbins_pdo*nbins_strips_clusters)));

      name = "h_strips_clustersProfile"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Cluster y position in layer "+si+";Counts").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

      name = "h_strips_clustersProfile_mult3"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";3-strip cluster y position in layer "+si+";Counts").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

      name = "h_strips_clustersProfile_mult4"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";4-strip cluster y position in layer "+si+";Counts").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));
      
      name = "h_strips_clustersProfile_mult5"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";5-strip cluster y position in layer "+si+";Counts").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

      name = "h_strips_clusterDistance"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Distance between clusters in layer "+si+";Counts").c_str(), nbins_strips, 1, nbins_strips+1)));

      name = "h2_strips_stripsPerCluster_vs_nClusters"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Number of clusters in strip layer "+si+";Strips per cluster in layer"+si).c_str(), nbins_strips_clusters, 0, nbins_strips_clusters, nbins_strips_per_cluster, 0, nbins_strips_per_cluster)));

      name = "h2_strips_stripsPerCluster_vs_nClusters_weighted"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Number of clusters in strip layer "+si+";Strips per cluster in layer"+si).c_str(), nbins_strips_clusters, 0, nbins_strips_clusters, nbins_strips_per_cluster, 0, nbins_strips_per_cluster)));

      // Strips resolution
      name = "h_strips_globalResidual"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Global y residual in layer "+si+" [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

      name = "h_strips_biasedResidual"+l;
      h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Biased y residual in layer "+si+" [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

      name = "h2_strips_localResidual"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Track y position in layer "+si+" [strip units];Global y residual in layer "+si+" [strip units]").c_str(), nbins_strips_clusters*nbins_strips, 0, nbins_strips, nbins_residuals, -range_residuals, range_residuals)));

      // Trigger
      name = "h2_pads_trigDiffVsVMM"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";VMM number in layer "+si+";Hit BCID - Trigger BCID").c_str(), nvmms, 0, nvmms, 2*nbins_bcid, -nbins_bcid, nbins_bcid)));
      
      name = "h2_pads_trigDiffVsKey"+l;
      h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";VMM number in layer "+si+";Hit BCID - Trigger BCID").c_str(), nuid, 0, nuid, 2*nbins_bcid, -nbins_bcid, nbins_bcid)));
      
    }
  }

  // Return if only running fast analysis
  if(doFast)
    return;
  
  // Initialize other histograms for full-blown analysis
  name = "h_strips_nTracks";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Number of tracks;Events").c_str(), nbins_strips_tracks, 0, nbins_strips_tracks)));

  name = "h_strips_nClustersPerTrack";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Number of clusters per track;Count").c_str(), nbins_strips_clusters, 0, nbins_strips_clusters)));

  name = "h_strips_trackSlope";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Track slope;Count").c_str(), nbins_strips_trackSlope, -nbins_strips_trackSlopeRange, nbins_strips_trackSlopeRange)));

  name = "h_strips_trackIntercept";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";Track intercept;Count").c_str(), nbins_strips_trackIntercept, 1, nbins_strips+1)));

  name = "h2_strips_clustersCorrelation_1vs2";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 2;Cluster y position in layer 1").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));
  
  name = "h2_strips_clustersCorrelation_1vs3";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 3;Cluster y position in layer 1").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

  name = "h2_strips_clustersCorrelation_1vs4";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 4;Cluster y position in layer 1").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

  name = "h2_strips_clustersCorrelation_2vs3";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 3;Cluster y position in layer 2").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));
  
  name = "h2_strips_clustersCorrelation_2vs4";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 4;Cluster y position in layer 2").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

  name = "h2_strips_clustersCorrelation_3vs4";
  h2.insert(std::make_pair(name, new TH2F(name.c_str(), (name+";Cluster y position in layer 4;Cluster y position in layer 3").c_str(), nbins_strips_clusters*nbins_strips, 1, nbins_strips+1, nbins_strips_clusters*nbins_strips, 1, nbins_strips+1)));

  name = "h_strips_pairwiseResidual_layer12";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{1} - y_{2})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

  name = "h_strips_pairwiseResidual_layer13";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{1} - y_{3})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

  name = "h_strips_pairwiseResidual_layer14";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{1} - y_{4})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

  name = "h_strips_pairwiseResidual_layer23";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{2} - y_{3})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

  name = "h_strips_pairwiseResidual_layer24";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{2} - y_{4})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

  name = "h_strips_pairwiseResidual_layer34";
  h1.insert(std::make_pair(name, new TH1F(name.c_str(), (name+";(y_{3} - y_{4})/#sqrt{2} [strip units];Count").c_str(), nbins_residuals, -range_residuals, range_residuals)));

}

void RecoManager::FillHistograms(){
  bool debug = false;
  if(debug)
    std::cout<<"Filling histograms..."<<std::endl;
  int nFiredLayers_strips = 0;
  int nFiredLayers_pads = 0;
  if(hits_processed_strips_l1.size()>0) nFiredLayers_strips++;
  if(hits_processed_strips_l2.size()>0) nFiredLayers_strips++;
  if(hits_processed_strips_l3.size()>0) nFiredLayers_strips++;
  if(hits_processed_strips_l4.size()>0) nFiredLayers_strips++;  
  if(hits_processed_pads_l1.size()>0) nFiredLayers_pads++;
  if(hits_processed_pads_l2.size()>0) nFiredLayers_pads++;
  if(hits_processed_pads_l3.size()>0) nFiredLayers_pads++;
  if(hits_processed_pads_l4.size()>0) nFiredLayers_pads++;

  int strip_hits_l1_size = hits_processed_strips_l1.size();
  int strip_hits_l2_size = hits_processed_strips_l2.size();
  int strip_hits_l3_size = hits_processed_strips_l3.size();
  int strip_hits_l4_size = hits_processed_strips_l4.size();
  int pad_hits_l1_size = hits_processed_pads_l1.size();
  int pad_hits_l2_size = hits_processed_pads_l2.size();
  int pad_hits_l3_size = hits_processed_pads_l3.size();
  int pad_hits_l4_size = hits_processed_pads_l4.size();

  // Use this to only have one for loop
  std::vector<int> v = {
    strip_hits_l1_size, strip_hits_l2_size, strip_hits_l3_size, strip_hits_l4_size,
    pad_hits_l1_size, pad_hits_l2_size, pad_hits_l3_size, pad_hits_l4_size,
  };
  
  int nhits = *std::max_element(v.begin(), v.end());

  // Cut for plots with less than n hits
  int nmultcut = 3;

  // Get geometry map
  std::map<long, DetectorComponent> geomap = GetGeometryProvider()->GetGeoMap();
      
  // Diagnostic histograms
  if(debug)
    std::cout<<"Diagnostics..."<<std::endl;

  h1["h_strips_nFiringLayers"]->Fill(nFiredLayers_strips);
  h1["h_pads_nFiringLayers"]->Fill(nFiredLayers_pads);
  
  h1["h_strips_nHits_layer1"]->Fill(strip_hits_l1_size);
  h1["h_strips_nHits_layer2"]->Fill(strip_hits_l2_size);
  h1["h_strips_nHits_layer3"]->Fill(strip_hits_l3_size);
  h1["h_strips_nHits_layer4"]->Fill(strip_hits_l4_size);
  h1["h_pads_nHits_layer1"]->Fill(pad_hits_l1_size);
  h1["h_pads_nHits_layer2"]->Fill(pad_hits_l2_size);
  h1["h_pads_nHits_layer3"]->Fill(pad_hits_l3_size);
  h1["h_pads_nHits_layer4"]->Fill(pad_hits_l4_size);

  if(debug)
    std::cout<<"Loop over hits..."<<std::endl;

  for(int i_hit = 0; i_hit < nhits; i_hit++){
    // Make sure to only fill things that exist...
    bool valid_s1 = (i_hit < strip_hits_l1_size);
    bool valid_s2 = (i_hit < strip_hits_l2_size);
    bool valid_s3 = (i_hit < strip_hits_l3_size);
    bool valid_s4 = (i_hit < strip_hits_l4_size);

    bool valid_p1 = (i_hit < pad_hits_l1_size);
    bool valid_p2 = (i_hit < pad_hits_l2_size);
    bool valid_p3 = (i_hit < pad_hits_l3_size);
    bool valid_p4 = (i_hit < pad_hits_l4_size);

    if(valid_s1){
      h1["h_strips_hitsProfile_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetComponent().GetY());
      h1["h_strips_pdo_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetAdc());
      h1["h_strips_tdo_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetTdo());
      h1["h_strips_tdo_calibrated_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetCalibratedTDO());
      h1["h_strips_tdo_calibrated_l1_y"+std::to_string(int(hits_processed_strips_l1[i_hit].GetComponent().GetY()))]->Fill(hits_processed_strips_l1[i_hit].GetCalibratedTDO());
      h1["h_strips_bcid_rel_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetBcidRel());
      h1["h_strips_bcid_rel_in_time_peak_layer1"]->Fill(in_time_peak[hits_processed_strips_l1[i_hit].GetComponent().GetBoard()][hits_processed_strips_l1[i_hit].GetComponent().GetVmm()][hits_processed_strips_l1[i_hit].GetComponent().GetCh()]);
      h2["h2_strips_chargeVsKey_layer1"]->Fill(hits_processed_strips_l1[i_hit].GetUniqueKey(), hits_processed_strips_l1[i_hit].GetAdc());
    }
    if(valid_s2){
      h1["h_strips_hitsProfile_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetComponent().GetY());
      h1["h_strips_pdo_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetAdc());
      h1["h_strips_tdo_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetTdo());
      h1["h_strips_tdo_calibrated_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetCalibratedTDO());
      h1["h_strips_tdo_calibrated_l2_y"+std::to_string(int(hits_processed_strips_l2[i_hit].GetComponent().GetY()))]->Fill(hits_processed_strips_l2[i_hit].GetCalibratedTDO());
      h1["h_strips_bcid_rel_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetBcidRel());
      h1["h_strips_bcid_rel_in_time_peak_layer2"]->Fill(in_time_peak[hits_processed_strips_l2[i_hit].GetComponent().GetBoard()][hits_processed_strips_l2[i_hit].GetComponent().GetVmm()][hits_processed_strips_l2[i_hit].GetComponent().GetCh()]);
      h2["h2_strips_chargeVsKey_layer2"]->Fill(hits_processed_strips_l2[i_hit].GetUniqueKey(), hits_processed_strips_l2[i_hit].GetAdc());
    }
    if(valid_s3){
      h1["h_strips_hitsProfile_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetComponent().GetY());
      h1["h_strips_pdo_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetAdc());
      h1["h_strips_tdo_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetTdo());
      h1["h_strips_tdo_calibrated_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetCalibratedTDO());
      h1["h_strips_tdo_calibrated_l3_y"+std::to_string(int(hits_processed_strips_l3[i_hit].GetComponent().GetY()))]->Fill(hits_processed_strips_l3[i_hit].GetCalibratedTDO());
      h1["h_strips_bcid_rel_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetBcidRel());
      h1["h_strips_bcid_rel_in_time_peak_layer3"]->Fill(in_time_peak[hits_processed_strips_l3[i_hit].GetComponent().GetBoard()][hits_processed_strips_l3[i_hit].GetComponent().GetVmm()][hits_processed_strips_l3[i_hit].GetComponent().GetCh()]);
      h2["h2_strips_chargeVsKey_layer3"]->Fill(hits_processed_strips_l3[i_hit].GetUniqueKey(), hits_processed_strips_l3[i_hit].GetAdc());
    }
    if(valid_s4){
      h1["h_strips_hitsProfile_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetComponent().GetY());
      h1["h_strips_pdo_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetAdc());
      h1["h_strips_tdo_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetTdo());
      h1["h_strips_tdo_calibrated_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetCalibratedTDO());
      h1["h_strips_tdo_calibrated_l4_y"+std::to_string(int(hits_processed_strips_l4[i_hit].GetComponent().GetY()))]->Fill(hits_processed_strips_l4[i_hit].GetCalibratedTDO());
      h1["h_strips_bcid_rel_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetBcidRel());
      h1["h_strips_bcid_rel_in_time_peak_layer4"]->Fill(in_time_peak[hits_processed_strips_l4[i_hit].GetComponent().GetBoard()][hits_processed_strips_l4[i_hit].GetComponent().GetVmm()][hits_processed_strips_l4[i_hit].GetComponent().GetCh()]);
      h2["h2_strips_chargeVsKey_layer4"]->Fill(hits_processed_strips_l4[i_hit].GetUniqueKey(), hits_processed_strips_l4[i_hit].GetAdc());
    }

    if(valid_p1){
      h1["h_pads_pdo_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetAdc());
      h1["h_pads_tdo_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetTdo());
      h1["h_pads_bcid_rel_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetBcidRel());
      h1["h_pads_bcid_rel_in_time_peak_layer1"]->Fill(in_time_peak[hits_processed_pads_l1[i_hit].GetComponent().GetBoard()][hits_processed_pads_l1[i_hit].GetComponent().GetVmm()][hits_processed_pads_l1[i_hit].GetComponent().GetCh()]);
      h1["h_pads_tdo_calibrated_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetCalibratedTDO());
      h2["h2_pads_hitsProfile_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetComponent().GetX(), hits_processed_pads_l1[i_hit].GetComponent().GetY());
      h2["h2_pads_hitsProfileVMMChannel_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetComponent().GetCh(), hits_processed_pads_l1[i_hit].GetComponent().GetVmm());
      h2["h2_pads_chargeVsKey_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetUniqueKey(), hits_processed_pads_l1[i_hit].GetAdc());
      h2["h2_pads_pdo_vs_tdo_calibrated_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetCalibratedTDO(), hits_processed_pads_l1[i_hit].GetAdc());
      
      h1["h_pads_pdo_l1_x"+std::to_string(int(hits_processed_pads_l1[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l1[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l1[i_hit].GetAdc());
      h1["h_pads_tdo_calibrated_l1_x"+std::to_string(int(hits_processed_pads_l1[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l1[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l1[i_hit].GetCalibratedTDO());
    }
    if(valid_p2){
      h1["h_pads_pdo_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetAdc());
      h1["h_pads_tdo_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetTdo());
      h1["h_pads_bcid_rel_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetBcidRel());
      h1["h_pads_bcid_rel_in_time_peak_layer2"]->Fill(in_time_peak[hits_processed_pads_l2[i_hit].GetComponent().GetBoard()][hits_processed_pads_l2[i_hit].GetComponent().GetVmm()][hits_processed_pads_l2[i_hit].GetComponent().GetCh()]);
      h1["h_pads_tdo_calibrated_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetCalibratedTDO());
      h2["h2_pads_hitsProfile_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetComponent().GetX(),hits_processed_pads_l2[i_hit].GetComponent().GetY());
      h2["h2_pads_hitsProfileVMMChannel_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetComponent().GetCh(),hits_processed_pads_l2[i_hit].GetComponent().GetVmm());
      h2["h2_pads_chargeVsKey_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetUniqueKey(), hits_processed_pads_l2[i_hit].GetAdc());
      h2["h2_pads_pdo_vs_tdo_calibrated_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetCalibratedTDO(), hits_processed_pads_l2[i_hit].GetAdc());
      
      h1["h_pads_pdo_l2_x"+std::to_string(int(hits_processed_pads_l2[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l2[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l2[i_hit].GetAdc());
      h1["h_pads_tdo_calibrated_l2_x"+std::to_string(int(hits_processed_pads_l2[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l2[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l2[i_hit].GetCalibratedTDO());
    }
    if(valid_p3){
      h1["h_pads_pdo_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetAdc());
      h1["h_pads_tdo_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetTdo());
      h1["h_pads_bcid_rel_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetBcidRel());
      h1["h_pads_bcid_rel_in_time_peak_layer3"]->Fill(in_time_peak[hits_processed_pads_l3[i_hit].GetComponent().GetBoard()][hits_processed_pads_l3[i_hit].GetComponent().GetVmm()][hits_processed_pads_l3[i_hit].GetComponent().GetCh()]);
      h1["h_pads_tdo_calibrated_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetCalibratedTDO());
      h2["h2_pads_hitsProfile_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetComponent().GetX(),hits_processed_pads_l3[i_hit].GetComponent().GetY());
      h2["h2_pads_hitsProfileVMMChannel_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetComponent().GetCh(),hits_processed_pads_l3[i_hit].GetComponent().GetVmm());
      h2["h2_pads_chargeVsKey_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetUniqueKey(), hits_processed_pads_l3[i_hit].GetAdc());
      h2["h2_pads_pdo_vs_tdo_calibrated_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetCalibratedTDO(), hits_processed_pads_l3[i_hit].GetAdc());
    
      h1["h_pads_pdo_l3_x"+std::to_string(int(hits_processed_pads_l3[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l3[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l3[i_hit].GetAdc());
      h1["h_pads_tdo_calibrated_l3_x"+std::to_string(int(hits_processed_pads_l3[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l3[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l3[i_hit].GetCalibratedTDO());
    }
    if(valid_p4){
      h1["h_pads_pdo_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetAdc());
      h1["h_pads_tdo_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetTdo());
      h1["h_pads_bcid_rel_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetBcidRel());
      h1["h_pads_bcid_rel_in_time_peak_layer4"]->Fill(in_time_peak[hits_processed_pads_l4[i_hit].GetComponent().GetBoard()][hits_processed_pads_l4[i_hit].GetComponent().GetVmm()][hits_processed_pads_l4[i_hit].GetComponent().GetCh()]);
      h1["h_pads_tdo_calibrated_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetCalibratedTDO());
      h2["h2_pads_hitsProfile_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetComponent().GetX(),hits_processed_pads_l4[i_hit].GetComponent().GetY());
      h2["h2_pads_hitsProfileVMMChannel_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetComponent().GetCh(),hits_processed_pads_l4[i_hit].GetComponent().GetVmm());
      h2["h2_pads_chargeVsKey_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetUniqueKey(), hits_processed_pads_l4[i_hit].GetAdc());
      h2["h2_pads_pdo_vs_tdo_calibrated_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetCalibratedTDO(), hits_processed_pads_l4[i_hit].GetAdc());
    
      h1["h_pads_pdo_l4_x"+std::to_string(int(hits_processed_pads_l4[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l4[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l4[i_hit].GetAdc());
      h1["h_pads_tdo_calibrated_l4_x"+std::to_string(int(hits_processed_pads_l4[i_hit].GetComponent().GetX()))+"_y"+std::to_string(int(hits_processed_pads_l4[i_hit].GetComponent().GetY()))]->Fill(hits_processed_pads_l4[i_hit].GetCalibratedTDO());
    }
    if(!doFast){
      if(valid_p1 && (pad_hits_l1_size < nmultcut)){
	h1["h_pads_pdo_multcut_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetAdc());
	h2["h2_pads_hitsProfileVMMChannel_multcut_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetComponent().GetCh(),hits_processed_pads_l1[i_hit].GetComponent().GetVmm());
      }
      if(valid_p2 && (pad_hits_l2_size < nmultcut)){
	h1["h_pads_pdo_multcut_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetAdc());
   	h2["h2_pads_hitsProfileVMMChannel_multcut_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetComponent().GetCh(),hits_processed_pads_l2[i_hit].GetComponent().GetVmm());
      }
      if(valid_p3 && (pad_hits_l3_size < nmultcut)){
	h1["h_pads_pdo_multcut_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetAdc());
   	h2["h2_pads_hitsProfileVMMChannel_multcut_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetComponent().GetCh(),hits_processed_pads_l3[i_hit].GetComponent().GetVmm());
      }
      if(valid_p4 && (pad_hits_l4_size < nmultcut)){
	h1["h_pads_pdo_multcut_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetAdc());
  	h2["h2_pads_hitsProfileVMMChannel_multcut_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetComponent().GetCh(),hits_processed_pads_l4[i_hit].GetComponent().GetVmm());
      }

      if(valid_p1){
	h2["h2_pads_trigDiffVsVMM_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetComponent().GetVmm(), hits_processed_pads_l1[i_hit].GetTriggerBcidDiff());
	h2["h2_pads_trigDiffVsKey_layer1"]->Fill(hits_processed_pads_l1[i_hit].GetUniqueKey(), hits_processed_pads_l1[i_hit].GetTriggerBcidDiff());
      }
      if(valid_p2){
	h2["h2_pads_trigDiffVsVMM_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetComponent().GetVmm(), hits_processed_pads_l2[i_hit].GetTriggerBcidDiff());
	h2["h2_pads_trigDiffVsKey_layer2"]->Fill(hits_processed_pads_l2[i_hit].GetUniqueKey(), hits_processed_pads_l2[i_hit].GetTriggerBcidDiff());
      }
      if(valid_p3){
	h2["h2_pads_trigDiffVsVMM_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetComponent().GetVmm(), hits_processed_pads_l3[i_hit].GetTriggerBcidDiff());
	h2["h2_pads_trigDiffVsKey_layer3"]->Fill(hits_processed_pads_l3[i_hit].GetUniqueKey(), hits_processed_pads_l3[i_hit].GetTriggerBcidDiff());
      }
      if(valid_p4){
	h2["h2_pads_trigDiffVsVMM_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetComponent().GetVmm(), hits_processed_pads_l4[i_hit].GetTriggerBcidDiff());
	h2["h2_pads_trigDiffVsKey_layer4"]->Fill(hits_processed_pads_l4[i_hit].GetUniqueKey(), hits_processed_pads_l4[i_hit].GetTriggerBcidDiff());
      }      
    }    
  }

  if(doFast)
    return;

  if(debug)
    std::cout<<"Full-blown analysis..."<<std::endl;

  // Fill other histograms for full-blown analysis
  Hit pads_maxHit_l1 = hm.GetPadMaxHit(1);
  Hit pads_maxHit_l2 = hm.GetPadMaxHit(2);
  Hit pads_maxHit_l3 = hm.GetPadMaxHit(3);
  Hit pads_maxHit_l4 = hm.GetPadMaxHit(4);

  Hit pads_subLead_l1 = hm.GetPadSubLeadHit(1);
  Hit pads_subLead_l2 = hm.GetPadSubLeadHit(2);
  Hit pads_subLead_l3 = hm.GetPadSubLeadHit(3);
  Hit pads_subLead_l4 = hm.GetPadSubLeadHit(4);

  Hit pads_subSubLead_l1 = hm.GetPadSubSubLeadHit(1);
  Hit pads_subSubLead_l2 = hm.GetPadSubSubLeadHit(2);
  Hit pads_subSubLead_l3 = hm.GetPadSubSubLeadHit(3);
  Hit pads_subSubLead_l4 = hm.GetPadSubSubLeadHit(4);
  
  Hit pads_minHit_l1 = hm.GetPadMinHit(1);
  Hit pads_minHit_l2 = hm.GetPadMinHit(2);
  Hit pads_minHit_l3 = hm.GetPadMinHit(3);
  Hit pads_minHit_l4 = hm.GetPadMinHit(4);

  if(debug)
    std::cout<<"Getting clusters..."<<std::endl;
  
  // Clusters
  clusters_processed_strips_l1 = stgc_cb->BuildClustersForLayer(hits_processed_strips_l1);
  clusters_processed_strips_l2 = stgc_cb->BuildClustersForLayer(hits_processed_strips_l2);
  clusters_processed_strips_l3 = stgc_cb->BuildClustersForLayer(hits_processed_strips_l3);
  clusters_processed_strips_l4 = stgc_cb->BuildClustersForLayer(hits_processed_strips_l4);

  int strip_clusters_l1_size = clusters_processed_strips_l1.size();
  int strip_clusters_l2_size = clusters_processed_strips_l2.size();
  int strip_clusters_l3_size = clusters_processed_strips_l3.size();
  int strip_clusters_l4_size = clusters_processed_strips_l4.size();

  // Use this to only have one for loop
  std::vector<int> v_clusters = {
    strip_clusters_l1_size, strip_clusters_l2_size, strip_clusters_l3_size, strip_clusters_l4_size,
  };
  
  int nclusters = *std::max_element(v_clusters.begin(), v_clusters.end());

  if(debug){
    std::cout<<"Loop over strip "<<nclusters<<" clusters..."<<std::endl;
    std::cout<<"Layer 1 has "<<strip_clusters_l1_size<<" clusters..."<<std::endl;
    std::cout<<"Layer 2 has "<<strip_clusters_l2_size<<" clusters..."<<std::endl;
    std::cout<<"Layer 3 has "<<strip_clusters_l3_size<<" clusters..."<<std::endl;
    std::cout<<"Layer 4 has "<<strip_clusters_l4_size<<" clusters..."<<std::endl;
  }
  // Strips clusters
  h1["h_strips_nClusters_layer1"]->Fill(strip_clusters_l1_size);
  h1["h_strips_nClusters_layer2"]->Fill(strip_clusters_l2_size);
  h1["h_strips_nClusters_layer3"]->Fill(strip_clusters_l3_size);
  h1["h_strips_nClusters_layer4"]->Fill(strip_clusters_l4_size);

  if(strip_clusters_l1_size == 1 && strip_clusters_l2_size == 1)
    h2["h2_strips_clustersCorrelation_1vs2"]->Fill(clusters_processed_strips_l2[0].GetY(), clusters_processed_strips_l1[0].GetY());
  if(strip_clusters_l1_size == 1 && strip_clusters_l3_size == 1)
    h2["h2_strips_clustersCorrelation_1vs3"]->Fill(clusters_processed_strips_l3[0].GetY(), clusters_processed_strips_l1[0].GetY());
  if(strip_clusters_l1_size == 1 && strip_clusters_l4_size == 1)
    h2["h2_strips_clustersCorrelation_1vs4"]->Fill(clusters_processed_strips_l4[0].GetY(), clusters_processed_strips_l1[0].GetY());
  if(strip_clusters_l2_size == 1 && strip_clusters_l3_size == 1)
    h2["h2_strips_clustersCorrelation_2vs3"]->Fill(clusters_processed_strips_l3[0].GetY(), clusters_processed_strips_l2[0].GetY());
  if(strip_clusters_l2_size == 1 && strip_clusters_l4_size == 1)
    h2["h2_strips_clustersCorrelation_2vs4"]->Fill(clusters_processed_strips_l4[0].GetY(), clusters_processed_strips_l2[0].GetY());
  if(strip_clusters_l3_size == 1 && strip_clusters_l4_size == 1)
    h2["h2_strips_clustersCorrelation_3vs4"]->Fill(clusters_processed_strips_l4[0].GetY(), clusters_processed_strips_l3[0].GetY());
  
  
  // Loop over strip clusters
  for(unsigned int i_cluster = 0; i_cluster < nclusters; i_cluster++){
    // Make sure to only fill things that exist...
    bool valid_s1 = (i_cluster < strip_clusters_l1_size);
    bool valid_s2 = (i_cluster < strip_clusters_l2_size);
    bool valid_s3 = (i_cluster < strip_clusters_l3_size);
    bool valid_s4 = (i_cluster < strip_clusters_l4_size);

    if(debug)
      std::cout<<"Trying clusters in layer 1"<<std::endl;
    if(valid_s1){
      h1["h_strips_stripsPerCluster_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetNHits());
      h1["h_strips_chargePerCluster_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetTotalAdc());
      h1["h_strips_clustersProfile_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetY());
      h2["h2_strips_stripsPerCluster_vs_nClusters_layer1"]->Fill(strip_clusters_l1_size, clusters_processed_strips_l1[i_cluster].GetNHits());
      h2["h2_strips_stripsPerCluster_vs_nClusters_weighted_layer1"]->Fill(strip_clusters_l1_size, clusters_processed_strips_l1[i_cluster].GetNHits(), 1.0/strip_clusters_l1_size);
      if(clusters_processed_strips_l1[i_cluster].GetNHits()==3){
	h1["h_strips_clustersProfile_mult3_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetY());
      }
      if(clusters_processed_strips_l1[i_cluster].GetNHits()==4){
	h1["h_strips_clustersProfile_mult4_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetY());
      }
      if(clusters_processed_strips_l1[i_cluster].GetNHits()==5){
	h1["h_strips_clustersProfile_mult5_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetY());
      }
      if(clusters_processed_strips_l1[i_cluster].GetNHits()==1){
	h1["h_strips_chargePerCluster_1strip_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l1[i_cluster].GetNHits()==2){
	h1["h_strips_chargePerCluster_2strip_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l1[i_cluster].GetNHits()>2){
	h1["h_strips_chargePerCluster_multstrip_layer1"]->Fill(clusters_processed_strips_l1[i_cluster].GetTotalAdc());
      } 
    }

    if(debug)
      std::cout<<"Trying clusters in layer 2"<<std::endl;
    if(valid_s2){
      h1["h_strips_stripsPerCluster_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetNHits());
      h1["h_strips_chargePerCluster_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetTotalAdc());
      h1["h_strips_clustersProfile_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetY());
      h2["h2_strips_stripsPerCluster_vs_nClusters_layer2"]->Fill(strip_clusters_l2_size, clusters_processed_strips_l2[i_cluster].GetNHits());
      h2["h2_strips_stripsPerCluster_vs_nClusters_weighted_layer2"]->Fill(strip_clusters_l2_size, clusters_processed_strips_l2[i_cluster].GetNHits(), 1.0/strip_clusters_l2_size);
      if(clusters_processed_strips_l2[i_cluster].GetNHits()==3){
	h1["h_strips_clustersProfile_mult3_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetY());
      }
      if(clusters_processed_strips_l2[i_cluster].GetNHits()==4){
	h1["h_strips_clustersProfile_mult4_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetY());
      }
      if(clusters_processed_strips_l2[i_cluster].GetNHits()==5){
	h1["h_strips_clustersProfile_mult5_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetY());
      }
      if(clusters_processed_strips_l2[i_cluster].GetNHits()==1){
	h1["h_strips_chargePerCluster_1strip_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l2[i_cluster].GetNHits()==2){
	h1["h_strips_chargePerCluster_2strip_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l2[i_cluster].GetNHits()>2){
	h1["h_strips_chargePerCluster_multstrip_layer2"]->Fill(clusters_processed_strips_l2[i_cluster].GetTotalAdc());
      } 
    }
    if(debug)
      std::cout<<"Trying clusters in layer 3"<<std::endl;
    if(valid_s3){
      h1["h_strips_stripsPerCluster_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetNHits());
      h1["h_strips_chargePerCluster_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetTotalAdc());
      h1["h_strips_clustersProfile_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetY());
      h2["h2_strips_stripsPerCluster_vs_nClusters_layer3"]->Fill(strip_clusters_l3_size, clusters_processed_strips_l3[i_cluster].GetNHits());
      h2["h2_strips_stripsPerCluster_vs_nClusters_weighted_layer3"]->Fill(strip_clusters_l3_size, clusters_processed_strips_l3[i_cluster].GetNHits(), 1.0/strip_clusters_l3_size);
      if(clusters_processed_strips_l3[i_cluster].GetNHits()==3){
	h1["h_strips_clustersProfile_mult3_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetY());
      }
      if(clusters_processed_strips_l3[i_cluster].GetNHits()==4){
	h1["h_strips_clustersProfile_mult4_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetY());
      }
      if(clusters_processed_strips_l3[i_cluster].GetNHits()==5){
	h1["h_strips_clustersProfile_mult5_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetY());
      }
      if(clusters_processed_strips_l3[i_cluster].GetNHits()==1){
	h1["h_strips_chargePerCluster_1strip_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l3[i_cluster].GetNHits()==2){
	h1["h_strips_chargePerCluster_2strip_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l3[i_cluster].GetNHits()>2){
	h1["h_strips_chargePerCluster_multstrip_layer3"]->Fill(clusters_processed_strips_l3[i_cluster].GetTotalAdc());
      }
    }
    if(debug)
      std::cout<<"Trying clusters in layer 4"<<std::endl;
    if(valid_s4){
      h1["h_strips_stripsPerCluster_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetNHits());
      h1["h_strips_chargePerCluster_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetTotalAdc());
      h1["h_strips_clustersProfile_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetY());
      h2["h2_strips_stripsPerCluster_vs_nClusters_layer4"]->Fill(strip_clusters_l4_size, clusters_processed_strips_l4[i_cluster].GetNHits());
      h2["h2_strips_stripsPerCluster_vs_nClusters_weighted_layer4"]->Fill(strip_clusters_l4_size, clusters_processed_strips_l4[i_cluster].GetNHits(), 1.0/strip_clusters_l4_size);
      if(clusters_processed_strips_l4[i_cluster].GetNHits()==3){
	h1["h_strips_clustersProfile_mult3_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetY());
      }
      if(clusters_processed_strips_l4[i_cluster].GetNHits()==4){
	h1["h_strips_clustersProfile_mult4_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetY());
      }
      if(clusters_processed_strips_l4[i_cluster].GetNHits()==5){
	h1["h_strips_clustersProfile_mult5_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetY());
      }
      if(clusters_processed_strips_l4[i_cluster].GetNHits()==1){
	h1["h_strips_chargePerCluster_1strip_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l4[i_cluster].GetNHits()==2){
	h1["h_strips_chargePerCluster_2strip_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetTotalAdc());
      }
      if(clusters_processed_strips_l4[i_cluster].GetNHits()>2){
	h1["h_strips_chargePerCluster_multstrip_layer4"]->Fill(clusters_processed_strips_l4[i_cluster].GetTotalAdc());
      } 
    }    
  }

  // Loop over clusters and fill distance between each of them

  if(strip_clusters_l1_size > 1){
    for(unsigned int i_cluster = 0; i_cluster < strip_clusters_l1_size; i_cluster++){
      for(unsigned int j_cluster = i_cluster+1; j_cluster < strip_clusters_l1_size; j_cluster++){
	h1["h_strips_clusterDistance_layer1"]->Fill(abs(clusters_processed_strips_l1[i_cluster].GetY() - clusters_processed_strips_l1[j_cluster].GetY()));
      }
    }
  }
  if(strip_clusters_l2_size > 1){
    for(unsigned int i_cluster = 0; i_cluster < strip_clusters_l2_size; i_cluster++){
      for(unsigned int j_cluster = i_cluster+1; j_cluster < strip_clusters_l2_size; j_cluster++){
	h1["h_strips_clusterDistance_layer2"]->Fill(abs(clusters_processed_strips_l2[i_cluster].GetY() - clusters_processed_strips_l2[j_cluster].GetY()));
      }
    }
  }
  if(strip_clusters_l3_size > 1){
    for(unsigned int i_cluster = 0; i_cluster < strip_clusters_l3_size; i_cluster++){
      for(unsigned int j_cluster = i_cluster+1; j_cluster < strip_clusters_l3_size; j_cluster++){
	h1["h_strips_clusterDistance_layer3"]->Fill(abs(clusters_processed_strips_l3[i_cluster].GetY() - clusters_processed_strips_l3[j_cluster].GetY()));
      }
    }
  }
  if(strip_clusters_l4_size > 1){
    for(unsigned int i_cluster = 0; i_cluster < strip_clusters_l4_size; i_cluster++){
      for(unsigned int j_cluster = i_cluster+1; j_cluster < strip_clusters_l4_size; j_cluster++){
	h1["h_strips_clusterDistance_layer4"]->Fill(abs(clusters_processed_strips_l4[i_cluster].GetY() - clusters_processed_strips_l4[j_cluster].GetY()));
      }
    }
  }

  // Tracks
  std::vector<Cluster> event_clusters;
  event_clusters.insert(event_clusters.end(), clusters_processed_strips_l1.begin(), clusters_processed_strips_l1.end());
  event_clusters.insert(event_clusters.end(), clusters_processed_strips_l2.begin(), clusters_processed_strips_l2.end());
  event_clusters.insert(event_clusters.end(), clusters_processed_strips_l3.begin(), clusters_processed_strips_l3.end());
  event_clusters.insert(event_clusters.end(), clusters_processed_strips_l4.begin(), clusters_processed_strips_l4.end());

  if(debug)
    std::cout<<"Event has "<<event_clusters.size()<<" clusters total; L1/L2/L3/L4 "
	     <<clusters_processed_strips_l1.size()<<"/"
	     <<clusters_processed_strips_l2.size()<<"/"
	     <<clusters_processed_strips_l3.size()<<"/"
	     <<clusters_processed_strips_l4.size()<<"."<<std::endl;
  
  std::vector<TrackCandidate> trackCandidates = tcs->SelectTrackCandidates(event_clusters, debug);

  if(debug)
    std::cout<<"Event has "<<trackCandidates.size()<<" track candidates."<<std::endl;
  
  tracks_processed = tbuilder->FitTracks(trackCandidates, debug);

  int ntracks = tracks_processed.size();
  if(debug)
    std::cout<<"Event has "<<ntracks<<" tracks."<<std::endl;

  h1["h_strips_nTracks"]->Fill(ntracks);

  

  // Loop over tracks and fill in diagnostic histograms
  for(unsigned int i_track = 0; i_track < ntracks; i_track++){

    // Residuals
    float GlobalResidualsY_l1 = tracks_processed[i_track].GetGlobalResidualsY()[0];
    float GlobalResidualsY_l2 = tracks_processed[i_track].GetGlobalResidualsY()[1];
    float GlobalResidualsY_l3 = tracks_processed[i_track].GetGlobalResidualsY()[2];
    float GlobalResidualsY_l4 = tracks_processed[i_track].GetGlobalResidualsY()[3];
    
    float BiasedResidualsY_l1 = tracks_processed[i_track].GetBiasedResidualsY()[0];
    float BiasedResidualsY_l2 = tracks_processed[i_track].GetBiasedResidualsY()[1];
    float BiasedResidualsY_l3 = tracks_processed[i_track].GetBiasedResidualsY()[2];
    float BiasedResidualsY_l4 = tracks_processed[i_track].GetBiasedResidualsY()[3];
    
    float trk_y = tracks_processed[i_track].GetB();

    int ncl = tracks_processed[i_track].GetNClusters();
    
    h1["h_strips_nClustersPerTrack"]->Fill(ncl);
    h1["h_strips_trackSlope"]->Fill(tracks_processed[i_track].GetA());
    h1["h_strips_trackIntercept"]->Fill(tracks_processed[i_track].GetB());

    h1["h_strips_pairwiseResidual_layer12"]->Fill(tracks_processed[i_track].getSelfResidual12());
    h1["h_strips_pairwiseResidual_layer13"]->Fill(tracks_processed[i_track].getSelfResidual13());
    h1["h_strips_pairwiseResidual_layer14"]->Fill(tracks_processed[i_track].getSelfResidual14());
    h1["h_strips_pairwiseResidual_layer23"]->Fill(tracks_processed[i_track].getSelfResidual23());
    h1["h_strips_pairwiseResidual_layer24"]->Fill(tracks_processed[i_track].getSelfResidual24());
    h1["h_strips_pairwiseResidual_layer34"]->Fill(tracks_processed[i_track].getSelfResidual34());

    // Only consider tracks that have clusters in all 4 layers for residuals
    if(debug)
      std::cout<<"Track has "<<ncl<<" clusters."<<std::endl;
    if(ncl == 4){
      h1["h_strips_globalResidual_layer1"]->Fill(GlobalResidualsY_l1);
      h1["h_strips_globalResidual_layer2"]->Fill(GlobalResidualsY_l2);
      h1["h_strips_globalResidual_layer3"]->Fill(GlobalResidualsY_l3);
      h1["h_strips_globalResidual_layer4"]->Fill(GlobalResidualsY_l4);
	
      h1["h_strips_biasedResidual_layer1"]->Fill(BiasedResidualsY_l1);
      h1["h_strips_biasedResidual_layer2"]->Fill(BiasedResidualsY_l2);
      h1["h_strips_biasedResidual_layer3"]->Fill(BiasedResidualsY_l3);
      h1["h_strips_biasedResidual_layer4"]->Fill(BiasedResidualsY_l4);

      h2["h2_strips_localResidual_layer1"]->Fill(trk_y, GlobalResidualsY_l1);
      h2["h2_strips_localResidual_layer2"]->Fill(trk_y, GlobalResidualsY_l2);
      h2["h2_strips_localResidual_layer3"]->Fill(trk_y, GlobalResidualsY_l3);
      h2["h2_strips_localResidual_layer4"]->Fill(trk_y, GlobalResidualsY_l4);
    }
  }
  
  if(debug)
    std::cout<<"Check leading pad..."<<std::endl;

  // Pads
  if(pads_maxHit_l1.GetAdc() > -1){
    h1["h_pads_pdo_maxHit_layer1"]->Fill(pads_maxHit_l1.GetAdc());
    h1["h_pads_tdo_calibrated_maxHit_layer1"]->Fill(pads_maxHit_l1.GetCalibratedTDO());
    h2["h2_pads_hitsProfileVMMChannel_maxHit_layer1"]->Fill(pads_maxHit_l1.GetComponent().GetCh(), pads_maxHit_l1.GetComponent().GetVmm());
    h2["h2_pads_nHits_vs_pdo_maxHit_layer1"]->Fill(pads_maxHit_l1.GetAdc(), pad_hits_l1_size);
  }
  if(pads_maxHit_l2.GetAdc() > -1){
    h1["h_pads_pdo_maxHit_layer2"]->Fill(pads_maxHit_l2.GetAdc());
    h1["h_pads_tdo_calibrated_maxHit_layer2"]->Fill(pads_maxHit_l2.GetCalibratedTDO());
    h2["h2_pads_hitsProfileVMMChannel_maxHit_layer2"]->Fill(pads_maxHit_l2.GetComponent().GetCh(), pads_maxHit_l2.GetComponent().GetVmm());
    h2["h2_pads_nHits_vs_pdo_maxHit_layer2"]->Fill(pads_maxHit_l2.GetAdc(), pad_hits_l2_size);
  }
  if(pads_maxHit_l3.GetAdc() > -1){
    h1["h_pads_pdo_maxHit_layer3"]->Fill(pads_maxHit_l3.GetAdc());
    h1["h_pads_tdo_calibrated_maxHit_layer3"]->Fill(pads_maxHit_l3.GetCalibratedTDO());
    h2["h2_pads_hitsProfileVMMChannel_maxHit_layer3"]->Fill(pads_maxHit_l3.GetComponent().GetCh(), pads_maxHit_l3.GetComponent().GetVmm());
    h2["h2_pads_nHits_vs_pdo_maxHit_layer3"]->Fill(pads_maxHit_l3.GetAdc(), pad_hits_l3_size);
  }
  if(pads_maxHit_l4.GetAdc() > -1){
    h1["h_pads_pdo_maxHit_layer4"]->Fill(pads_maxHit_l4.GetAdc());
    h1["h_pads_tdo_calibrated_maxHit_layer4"]->Fill(pads_maxHit_l4.GetCalibratedTDO());
    h2["h2_pads_hitsProfileVMMChannel_maxHit_layer4"]->Fill(pads_maxHit_l4.GetComponent().GetCh(), pads_maxHit_l4.GetComponent().GetVmm());
    h2["h2_pads_nHits_vs_pdo_maxHit_layer4"]->Fill(pads_maxHit_l4.GetAdc(), pad_hits_l4_size);
  }

  if(debug)
    std::cout<<"Check subleading pad..."<<std::endl;

  if(pads_subLead_l1.GetAdc() > -1){
    h1["h_pads_pdo_subLead_layer1"]->Fill(pads_subLead_l1.GetAdc());
    h1["h_pads_tdo_calibrated_subLead_layer1"]->Fill(pads_subLead_l1.GetCalibratedTDO());
  }
  if(pads_subLead_l2.GetAdc() > -1){
    h1["h_pads_pdo_subLead_layer2"]->Fill(pads_subLead_l2.GetAdc());
    h1["h_pads_tdo_calibrated_subLead_layer2"]->Fill(pads_subLead_l2.GetCalibratedTDO());
  }
  if(pads_subLead_l3.GetAdc() > -1){
    h1["h_pads_pdo_subLead_layer3"]->Fill(pads_subLead_l3.GetAdc());
    h1["h_pads_tdo_calibrated_subLead_layer3"]->Fill(pads_subLead_l3.GetCalibratedTDO());
  }
  if(pads_subLead_l4.GetAdc() > -1){
    h1["h_pads_pdo_subLead_layer4"]->Fill(pads_subLead_l4.GetAdc());
    h1["h_pads_tdo_calibrated_subLead_layer4"]->Fill(pads_subLead_l4.GetCalibratedTDO());
  }

  if(debug)
    std::cout<<"Check subsubleading pad..."<<std::endl;

  if(pads_subSubLead_l1.GetAdc() > -1){
    h1["h_pads_pdo_subSubLead_layer1"]->Fill(pads_subSubLead_l1.GetAdc());
    h1["h_pads_tdo_calibrated_subSubLead_layer1"]->Fill(pads_subSubLead_l1.GetCalibratedTDO());
  }
  if(pads_subSubLead_l2.GetAdc() > -1){
    h1["h_pads_pdo_subSubLead_layer2"]->Fill(pads_subSubLead_l2.GetAdc());
    h1["h_pads_tdo_calibrated_subSubLead_layer2"]->Fill(pads_subSubLead_l2.GetCalibratedTDO());
  }
  if(pads_subSubLead_l3.GetAdc() > -1){
    h1["h_pads_pdo_subSubLead_layer3"]->Fill(pads_subSubLead_l3.GetAdc());
    h1["h_pads_tdo_calibrated_subSubLead_layer3"]->Fill(pads_subSubLead_l3.GetCalibratedTDO());
  }
  if(pads_subSubLead_l4.GetAdc() > -1){
    h1["h_pads_pdo_subSubLead_layer4"]->Fill(pads_subSubLead_l4.GetAdc());
    h1["h_pads_tdo_calibrated_subSubLead_layer4"]->Fill(pads_subSubLead_l4.GetCalibratedTDO());
  }

  if(debug)
    std::cout<<"Check sublead ratio..."<<std::endl;

  if((pads_maxHit_l1.GetAdc() > -1) && (pads_subLead_l1.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subLeadOverMaxHit_layer1"]->Fill(double(pads_subLead_l1.GetAdc()) / double(pads_maxHit_l1.GetAdc()));
  }
  if((pads_maxHit_l2.GetAdc() > -1) && (pads_subLead_l2.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subLeadOverMaxHit_layer2"]->Fill(double(pads_subLead_l2.GetAdc()) / double(pads_maxHit_l2.GetAdc()));
  }
  if((pads_maxHit_l3.GetAdc() > -1) && (pads_subLead_l3.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subLeadOverMaxHit_layer3"]->Fill(double(pads_subLead_l3.GetAdc()) / double(pads_maxHit_l3.GetAdc()));
  }
  if((pads_maxHit_l4.GetAdc() > -1) && (pads_subLead_l4.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subLeadOverMaxHit_layer4"]->Fill(double(pads_subLead_l4.GetAdc()) / double(pads_maxHit_l4.GetAdc()));
  }

  if(debug)
    std::cout<<"Check subsublead ratio..."<<std::endl;

  if((pads_maxHit_l1.GetAdc() > -1) && (pads_subSubLead_l1.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subSubLeadOverMaxHit_layer1"]->Fill(double(pads_subSubLead_l1.GetAdc()) / double(pads_maxHit_l1.GetAdc()));
  }
  if((pads_maxHit_l2.GetAdc() > -1) && (pads_subSubLead_l2.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subSubLeadOverMaxHit_layer2"]->Fill(double(pads_subSubLead_l2.GetAdc()) / double(pads_maxHit_l2.GetAdc()));
  }
  if((pads_maxHit_l3.GetAdc() > -1) && (pads_subSubLead_l3.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subSubLeadOverMaxHit_layer3"]->Fill(double(pads_subSubLead_l3.GetAdc()) / double(pads_maxHit_l3.GetAdc()));
  }
  if((pads_maxHit_l4.GetAdc() > -1) && (pads_subSubLead_l4.GetAdc() > -1)){
    h1["h_pads_pdo_ratio_subSubLeadOverMaxHit_layer4"]->Fill(double(pads_subSubLead_l4.GetAdc()) / double(pads_maxHit_l4.GetAdc()));
  }

  if(debug)
    std::cout<<"Check multiplicity 2..."<<std::endl;

  if(pad_hits_l1_size == 2){
    h1["h_pads_pdo_mult2_maxHit_layer1"]->Fill(pads_maxHit_l1.GetAdc());
    h1["h_pads_pdo_mult2_minHit_layer1"]->Fill(pads_minHit_l1.GetAdc());
    h2["h2_pads_hitsProfile_mult2_maxHit_layer1"]->Fill(pads_maxHit_l1.GetComponent().GetX(),pads_maxHit_l1.GetComponent().GetY());
    h2["h2_pads_hitsProfile_mult2_minHit_layer1"]->Fill(pads_minHit_l1.GetComponent().GetX(),pads_minHit_l1.GetComponent().GetY());
  }
  
  if(pad_hits_l2_size == 2){
    h1["h_pads_pdo_mult2_maxHit_layer2"]->Fill(pads_maxHit_l2.GetAdc());
    h1["h_pads_pdo_mult2_minHit_layer2"]->Fill(pads_minHit_l2.GetAdc());  
    h2["h2_pads_hitsProfile_mult2_maxHit_layer2"]->Fill(pads_maxHit_l2.GetComponent().GetX(),pads_maxHit_l2.GetComponent().GetY());
    h2["h2_pads_hitsProfile_mult2_minHit_layer2"]->Fill(pads_minHit_l2.GetComponent().GetX(),pads_minHit_l2.GetComponent().GetY());
  }
  
  if(pad_hits_l3_size == 2){
    h1["h_pads_pdo_mult2_maxHit_layer3"]->Fill(pads_maxHit_l3.GetAdc());
    h1["h_pads_pdo_mult2_minHit_layer3"]->Fill(pads_minHit_l3.GetAdc());
    h2["h2_pads_hitsProfile_mult2_maxHit_layer3"]->Fill(pads_maxHit_l3.GetComponent().GetX(),pads_maxHit_l3.GetComponent().GetY());
    h2["h2_pads_hitsProfile_mult2_minHit_layer3"]->Fill(pads_minHit_l3.GetComponent().GetX(),pads_minHit_l3.GetComponent().GetY());
  }  
  
  if(pad_hits_l4_size == 2){
    h1["h_pads_pdo_mult2_maxHit_layer4"]->Fill(pads_maxHit_l4.GetAdc());
    h1["h_pads_pdo_mult2_minHit_layer4"]->Fill(pads_minHit_l4.GetAdc());
    h2["h2_pads_hitsProfile_mult2_maxHit_layer4"]->Fill(pads_maxHit_l4.GetComponent().GetX(),pads_maxHit_l4.GetComponent().GetY());
    h2["h2_pads_hitsProfile_mult2_minHit_layer4"]->Fill(pads_minHit_l4.GetComponent().GetX(),pads_minHit_l4.GetComponent().GetY());
  }
  if(debug)
    std::cout<<"Done..."<<std::endl;

}

void RecoManager::WriteHistograms(){
  for(auto it = h1.begin(); it != h1.end(); ++it)
    //if(it->second->GetEntries() > 0)
      it->second->Write(it->first.c_str());
  for(auto it = h2.begin(); it != h2.end(); ++it)
    //if(it->second->GetEntries() > 0)
      it->second->Write(it->first.c_str());
}
