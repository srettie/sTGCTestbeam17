/*
 * Hit.cpp
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov
 */

#include "Hit.h"

Hit::Hit():
adc(-1){

}

Hit::Hit(DetectorComponent in_component, int in_adc, int in_tdo):
  component(in_component),
  adc(in_adc),
  tdo(in_tdo){
  }

Hit::Hit(DetectorComponent in_component, int in_adc, int in_tdo, int in_bcid, int in_bcid_trig, int in_tdo_trig):
  component(in_component),
  adc(in_adc),
  tdo(in_tdo),
  bcid(in_bcid),
  bcid_trig(in_bcid_trig),
  tdo_trig(in_tdo_trig){
  }
Hit::Hit(DetectorComponent in_component, int in_adc, int in_tdo, int in_bcid, int in_bcid_rel):
  component(in_component),
  adc(in_adc),
  tdo(in_tdo),
  bcid(in_bcid),
  bcid_rel(in_bcid_rel){
  }

Hit::Hit(const Hit &hit)
{
	component = hit.component;
	adc = hit.adc;
	tdo = hit.tdo;
	tdo_trig = hit.tdo_trig;
	tdo_calibrated = hit.tdo_calibrated;
	bcid = hit.bcid;
	bcid_trig = hit.bcid_trig;
	bcid_rel = hit.bcid_rel;
}

Hit::~Hit() {
	// TODO Auto-generated destructor stub
}
