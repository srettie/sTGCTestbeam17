/*
 * Hit.h
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov and Luca M
 */

#include "DetectorComponent.h"
#include <iostream>

#ifndef HIT_H_
#define HIT_H_

class Hit {
public:
	Hit();
	Hit(class DetectorComponent in_component, int in_adc, int in_tdo);
	Hit(class DetectorComponent in_component, int in_adc, int in_tdo, int in_bcid, int in_bcid_trig, int in_tdo_trig);
	Hit(class DetectorComponent in_component, int in_adc, int in_tdo, int in_bcid, int in_bcid_rel);
	Hit(const Hit &hit);

	virtual ~Hit();

	int GetAdc() const {
		return adc;
	}

	void SetAdc(int adc) {
		this->adc = adc;
	}
	
	double GetCalibratedTDO() const {
		return tdo_calibrated;
	}

	void SetCalibratedTDO(double tdo_calib) {
		this->tdo_calibrated = tdo_calib;
	}

	int GetBcidRel() const {
	  return bcid_rel;
	}

	int GetBcid() const {
	  return bcid;
	}

	void SetBcid(int bcid) {
	  this->bcid = bcid;
	}

	int GetBcidTrig() const {
	  return bcid_trig;
	}

	void SetBcidTrig(int bcid_trig) {
	  this->bcid_trig = bcid_trig;
	}

	int GetTdoTrig() const {
	  return tdo_trig;
	}

	void SetTdoTrig(int tdo_trig) {
	  this->tdo_trig = tdo_trig;
	}

	const class DetectorComponent& GetComponent() const {
		return component;
	}

	void SetComponent(const class DetectorComponent& component) {
		this->component = component;
	}

	int GetLayer() const {
		return this->component.GetLayer();
	}

	void Dump() const{
	  //		std::cout << "Hit with ADC val: " << adc << " on component:" << std::endl;
	  std::cout<<"NEW HIT:"<<std::endl;
	  component.Dump();
	  std::cout<<"ADC: "<<adc<<" TDO: "<<tdo<<" BCID: "<<bcid<<" Trigger BCID: "<< bcid_trig<<std::endl;
	  std::cout<<"BCID - Trigger BCID: "<<bcid-bcid_trig<<std::endl;
	}
	
	int GetTdo() const {
		return tdo;
	}

	void SetTdo(int tdo) {
		this->tdo = tdo;
	}

	bool isStripHit() const {
	  return component.isStrip();
	}

	bool isPadHit() const {
	  return component.isPad();
	}

	int GetUniqueKey() const {
	  return 100*component.GetVmm() + component.GetCh();
	}

	int GetTriggerBcidDiff() const {
	  int diff = bcid - bcid_trig;
	  if(diff < 0)
	    diff += 4096;
	  return diff;
	}

	int GetCorrectedTDO() const {
	  // 80 is "best guess" for now...
	  // 80 is counts per 25ns
	  //////////return GetTriggerBcidDiff()*80 + (tdo - tdo_trig);
	  return 80*(bcid_rel-3) - tdo;
	}

protected:
	DetectorComponent component;
	int adc;
	int tdo;
	int tdo_trig;
	double tdo_calibrated;
	int bcid;
	int bcid_trig;
	int bcid_rel;
};

#endif /* HIT_H_ */
