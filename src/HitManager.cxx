/*
 * HitManager.cpp
 *
 *  Created on: Sep 25, 2017
 *      Author: ekajomov and Luca M
 */

#include "HitManager.h"

HitManager::HitManager() {
  // TODO Auto-generated constructor stub

}

HitManager::~HitManager() {
  // TODO Auto-generated destructor stub
}

std::vector<Hit> HitManager::GetHitsInStripLayer(int layer) {
  if (layer2hits_strips.find(layer) == layer2hits_strips.end())
    {
      std::vector<Hit> hits;
      layer2hits_strips[layer] = hits;
    }
  return layer2hits_strips[layer];
}

std::vector<Hit> HitManager::GetHitsInPadLayer(int layer) {
  if (layer2hits_pads.find(layer) == layer2hits_pads.end())
    {
      std::vector<Hit> hits;
      layer2hits_pads[layer] = hits;
    }
  return layer2hits_pads[layer];
}


int HitManager::GetStripLayerCharge(int layer) {
  if (layer2hits_strips.find(layer) == layer2hits_strips.end())
    return 0;
  // Integrate charge of all strips for this layer
  int charge = 0;
  for(unsigned int i_hit = 0; i_hit < layer2hits_strips[layer].size(); i_hit++)
    charge += layer2hits_strips[layer][i_hit].GetAdc();
  
  return charge;
}

int HitManager::GetPadLayerCharge(int layer) {
  if (layer2hits_pads.find(layer) == layer2hits_pads.end())
    return 0;
  // Integrate charge of all pads for this layer
  int charge = 0;
  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    charge += layer2hits_pads[layer][i_hit].GetAdc();

  return charge;
}

int HitManager::GetPadMaxHitCharge(int layer) {
  if (layer2hits_pads.find(layer) == layer2hits_pads.end())
    return 0;
  int maxCharge = 0;
  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      if(maxCharge<layer2hits_pads[layer][i_hit].GetAdc()) maxCharge = layer2hits_pads[layer][i_hit].GetAdc();
    }

  return maxCharge;
}

int HitManager::GetPadMinHitCharge(int layer) {
  if (layer2hits_pads.find(layer) == layer2hits_pads.end())
    return 0;
  int minCharge = 9999;
  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      if(minCharge>layer2hits_pads[layer][i_hit].GetAdc()) minCharge = layer2hits_pads[layer][i_hit].GetAdc();
    }

  return minCharge;
}

Hit HitManager::GetPadMaxHit(int layer) {

  Hit hit;

  if (layer2hits_pads.find(layer) == layer2hits_pads.end()){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  if(layer2hits_pads[layer].size() ==0){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }

  int maxCharge = 0;
  int maxCharge_i = -1;

  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      if(maxCharge < layer2hits_pads[layer][i_hit].GetAdc()){
	maxCharge = layer2hits_pads[layer][i_hit].GetAdc();
	maxCharge_i = i_hit;
      }
    }

  hit = layer2hits_pads[layer][maxCharge_i];

  return hit;
}

Hit HitManager::GetPadSubLeadHit(int layer){

  Hit maxHit = GetPadMaxHit(layer);
  Hit hit;

  if (layer2hits_pads.find(layer) == layer2hits_pads.end()){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  if(layer2hits_pads[layer].size() < 2){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  
  int subLeadCharge = 0;
  int subLeadCharge_i = -1;
  
  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      // Skip max hit
      if(maxHit.GetUniqueKey() == layer2hits_pads[layer][i_hit].GetUniqueKey())
	continue;
      if(subLeadCharge < layer2hits_pads[layer][i_hit].GetAdc()){
	subLeadCharge = layer2hits_pads[layer][i_hit].GetAdc();
	subLeadCharge_i = i_hit;
      }
    }
  
  hit = layer2hits_pads[layer][subLeadCharge_i];
  
  return hit;
  
}

Hit HitManager::GetPadSubSubLeadHit(int layer){

  Hit maxHit = GetPadMaxHit(layer);
  Hit subLeadHit = GetPadSubLeadHit(layer);
  
  Hit hit;

  if (layer2hits_pads.find(layer) == layer2hits_pads.end()){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  if(layer2hits_pads[layer].size() < 3){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  
  int subSubLeadCharge = 0;
  int subSubLeadCharge_i = -1;
  
  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      // Skip max hit
      if(maxHit.GetUniqueKey() == layer2hits_pads[layer][i_hit].GetUniqueKey())
	continue;
      // Skip sublead hit
      if(subLeadHit.GetUniqueKey() == layer2hits_pads[layer][i_hit].GetUniqueKey())
	continue;
      if(subSubLeadCharge < layer2hits_pads[layer][i_hit].GetAdc()){
	subSubLeadCharge = layer2hits_pads[layer][i_hit].GetAdc();
	subSubLeadCharge_i = i_hit;
      }
    }
  
  hit = layer2hits_pads[layer][subSubLeadCharge_i];
  
  return hit;
  
}


Hit HitManager::GetPadMinHit(int layer) {

  Hit hit;

  if (layer2hits_pads.find(layer) == layer2hits_pads.end()){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }
  if(layer2hits_pads[layer].size() ==0){
    //std::cout<< "empty hit " << hit.GetComponent().GetCh() << std::endl;
    return hit;
  }

  int maxCharge = 9999;
  int maxCharge_i = -1;

  for(unsigned int i_hit = 0; i_hit < layer2hits_pads[layer].size(); i_hit++)
    {
      if(maxCharge > layer2hits_pads[layer][i_hit].GetAdc()){
	maxCharge = layer2hits_pads[layer][i_hit].GetAdc();
	maxCharge_i = i_hit;
      }
    }

  hit = layer2hits_pads[layer][maxCharge_i];

  return hit;
}

void HitManager::AddHit(Hit hit) {
  int layer = hit.GetLayer();

  if(hit.isStripHit()){
    if (layer2hits_strips.find(layer) == layer2hits_strips.end())
      {
	//std::cout<<"In AddHit...inside if statement"<<std::endl;
	std::vector<Hit> hits;
	hits.push_back(hit);
	//std::cout<<"Pushed back hit..."<<std::endl;
	layer2hits_strips[hit.GetLayer()] = hits;
	return;
      }
    std::vector<Hit>& hits = layer2hits_strips[hit.GetLayer()];
    hits.push_back(hit);
  }
  if(hit.isPadHit()){
    if (layer2hits_pads.find(layer) == layer2hits_pads.end())
      {
	//std::cout<<"In AddHit...inside if statement"<<std::endl;
	std::vector<Hit> hits;
	hits.push_back(hit);
	//std::cout<<"Pushed back hit..."<<std::endl;
	layer2hits_pads[hit.GetLayer()] = hits;
	return;
      }
    std::vector<Hit>& hits = layer2hits_pads[hit.GetLayer()];
    hits.push_back(hit);	  
  }
  return;
}

void HitManager::Clear() {
  layer2hits_strips.clear();
  layer2hits_pads.clear();
}

void HitManager::SetTree(TTree* in_tree)
{
  tree = in_tree;
}
void HitManager::PreEventProcessing()
{

}
void HitManager::PostEventProcessing()
{

}

bool HitManager::Init()
{
  return true;
}

bool HitManager::Finalize()
{
  return true;
}
