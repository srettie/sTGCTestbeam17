/*
 * HitManager.h
 *
 *  Created on: Sep 25, 2017
 *      Author: ekajomov
 */

#ifndef HITMANAGER_H_
#define HITMANAGER_H_

#include <vector>
#include <map>
#include "Hit.h"
#include "TTree.h"
#include "IRecoTool.h"


class HitManager : public IRecoTool {
public:
	HitManager();
	virtual ~HitManager();

	std::vector<Hit> GetHitsInStripLayer(int layer);
	std::vector<Hit> GetHitsInPadLayer(int layer);
	int GetStripLayerCharge(int layer);
	int GetPadLayerCharge(int layer);
	int GetPadMaxHitCharge(int layer);
	int GetPadMinHitCharge(int layer);
	Hit GetPadMaxHit(int layer);
	Hit GetPadSubLeadHit(int layer);
	Hit GetPadSubSubLeadHit(int layer);
	Hit GetPadMinHit(int layer);
	void AddHit(Hit);
	void Clear();

	void SetTree(TTree*);
	void PreEventProcessing();
	void PostEventProcessing();
	bool Init();
	bool Finalize();

protected:
	std::map<int, std::vector<Hit>> layer2hits_strips;
	std::map<int, std::vector<Hit>> layer2hits_pads;
	TTree* tree;

};

#endif /* HITMANAGER_H_ */
