/*
 * IRecoTool.h
 *
 *  Created on: Sep 27, 2017
 *      Author: ekajomov
 */

#ifndef IRECOTOOL_H_
#define IRECOTOOL_H_

class IRecoTool
{
public:
	IRecoTool(){}
	virtual ~IRecoTool(){}
	virtual void PreEventProcessing() =0;
	virtual void PostEventProcessing() =0;
	virtual bool Init() =0;
	virtual bool Finalize() =0;
};



#endif /* IRECOTOOL_H_ */
