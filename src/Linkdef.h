#include "AGDDTokenizer.h"
#include "AsciiGeometryProviderBuilder.h"
#include "ClusterBuilder.h"
#include "Cluster.h"
#include "ComponentDefs.h"
#include "DetectorComponent.h"
#include "ExpressionEvaluator.h"
#include "GeometryProvider.h"
#include "Hit.h"
#include "HitManager.h"
#include "IRecoTool.h"
#include "MyMainFrame.h"
#include "QuadrupletGeometryProviderBuilder.h"
#include "Readout.h"
#include "RecoManager.h"
#include "sTGCHandler.h"
#include "sTGCParameters.h"
#include "sTGC_readoutHandler.h"
#include "sTGCReadoutParameters.h"
#include "TestBeamConfigHandler.h"
#include "TestBeam_Tracker.h"
#include "TrackBuilder.h"
#include "TrackCandidate.h"
#include "TrackCandidateSelector.h"
#include "Track.h"
#include "TriggerSkimmer.h"
#include "varHandler.h"
#include "XercesParser.h"
#include "XMLHandler.h"
#include "XMLHandlerStore.h"


#include <vector>

#ifdef __ROOTCLING__

#pragma link C++ class AGDDTokenizer+;
#pragma link C++ class AsciiGeometryProviderBuilder+;
#pragma link C++ class ClusterBuilder+;
#pragma link C++ class Cluster+;
#pragma link C++ class ComponentDefs+;
#pragma link C++ class DetectorComponent+;
#pragma link C++ class ExpressionEvaluator+;
#pragma link C++ class GeometryProvider+;
#pragma link C++ class Hit+;
#pragma link C++ class HitManager+;
#pragma link C++ class IRecoTool+;
#pragma link C++ class MyMainFrame+;
#pragma link C++ class QuadrupletGeometryProviderBuilder+;
#pragma link C++ class Readout+;
#pragma link C++ class RecoManager+;
#pragma link C++ class sTGCHandler+;
#pragma link C++ class sTGCParameters+;
#pragma link C++ class sTGC_readoutHandler+;
#pragma link C++ class sTGCReadoutParameters+;
#pragma link C++ class TestBeam_Tracker+;
#pragma link C++ class TestBeamConfigHandler+;
#pragma link C++ class TrackBuilder+;
#pragma link C++ class TrackCandidate+;
#pragma link C++ class TrackCandidateSelector+;
#pragma link C++ class Track+;
#pragma link C++ class TriggerSkimmer+;
#pragma link C++ class varHandler+;
#pragma link C++ class XercesParser+;
#pragma link C++ class XMLHandler+;
#pragma link C++ class XMLHandlerStore+;

#pragma link C++ class std::vector<vector <float>>+;
#pragma link C++ class std::vector<vector <int>>+;
#pragma link C++ class std::vector<Hit>+;
#pragma link C++ class std::vector<Cluster>+;
#pragma link C++ class std::vector<Track>+;
#pragma link C++ class std::vector<TrackCandidate>+;
#pragma link C++ class std::vector<std::vector <ClusterCandidate>>+;
#endif
