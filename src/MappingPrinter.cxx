//============================================================================
// Name        : MappingPrinter.cxx
// Author      : srettie
//============================================================================

//#include "AsciiGeometryProviderBuilder.h"
#include "QuadrupletGeometryProviderBuilder.h"
#include "GeometryProvider.h"
#include "TrackCandidateSelector.h"
#include "TrackBuilder.h"
#include "RecoManager.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TText.h"
#include "TPaveText.h"

#include "TestBeamConfigHandler.h"
#include "XercesParser.h"
#include "XMLHandlerStore.h"

using namespace std;

int MappingPrinter() {

  // Setup XML parser
  XMLHandlerStore *hs=XMLHandlerStore::GetHandlerStore();
  auto tbch= std::make_unique<TestBeamConfigHandler>("TestBeamTrigConfig");
  XercesParser xp;
  xp.ParseFileAndNavigate("xml/testBeamConfig18.xml");
  
  // Setup geometry
  QuadrupletGeometryProviderBuilder qgpb;
  qgpb.SetStripZs(tbch->parDouble("STRIPZL1"),tbch->parDouble("STRIPZL2"),tbch->parDouble("STRIPZL3"),tbch->parDouble("STRIPZL4"));
  qgpb.SetPadZs(tbch->parDouble("PADZL1"),tbch->parDouble("PADZL2"),tbch->parDouble("PADZL3"),tbch->parDouble("PADZL4"));
  qgpb.SetQuadType(tbch->parString("QUADTYPE"));
  GeometryProvider* gp = qgpb.Build();
  
  // Prepare canvas
  TCanvas *can = new TCanvas();
  can->cd();
  can->SetGrid();
  gStyle->SetOptStat(0);
  
  TPaveText *pt = new TPaveText();

  std::map<long, DetectorComponent> geomap = gp->GetGeoMap();
  
  int nbins_pads_x12 = gp->GetNPadsPhi12();
  int nbins_pads_y12 = gp->GetNPadsEta12();
  int nbins_pads_x34 = gp->GetNPadsPhi34();
  int nbins_pads_y34 = gp->GetNPadsEta34();

  for(int i = 1; i < 5; ++i){
    std::string si = std::to_string(i);
    std::string l = "_layer" + si;
    std::string name = gp->GetQuadType()+l;
    int nbinsx;
    int nbinsy;
    if((i==1)||(i==2)){
      nbinsx = nbins_pads_x12;
      nbinsy = nbins_pads_y12;
    }
    else if((i==3)||(i==4)){
      nbinsx = nbins_pads_x34;
      nbinsy = nbins_pads_y34;
    }
    // Book and draw histogram
    TH2F* h2_mapping = new TH2F(name.c_str(), (name+";Layer "+si+" as seen when looking into the beam;Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(), nbinsx, 1, nbinsx+1, nbinsy, 1, nbinsy+1);
    h2_mapping->GetXaxis()->SetNdivisions(nbinsx);
    h2_mapping->GetYaxis()->SetNdivisions(nbinsy);
    h2_mapping->Draw();    
    // Loop over pads and print useful information
    for(std::map<long, DetectorComponent>::iterator it=geomap.begin(); it!=geomap.end(); ++it){
      long key = it->first;
      DetectorComponent c = it->second;
      if(i != c.GetLayer() || !c.isPad())
	continue;
      std::string mapping = "VMM: "+std::to_string(c.GetVmm())+" Ch: "+std::to_string(c.GetCh());
      pt = new TPaveText(c.GetX(), c.GetY(), c.GetX()+1, c.GetY()+1, "NB");
      pt->SetFillColorAlpha(kWhite, 0);
      pt->SetMargin();
      pt->AddText(mapping.c_str());
      pt->Draw();
    }
    if(i<4)
      can->SaveAs(("plots/PadMapping_"+gp->GetQuadType()+".pdf(").c_str());
    else
      can->SaveAs(("plots/PadMapping_"+gp->GetQuadType()+".pdf)").c_str());
  }
  return 0;
}
