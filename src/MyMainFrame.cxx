/*
 * Gui_example.cpp
 *
 *  Created on: Oct 19, 2017
 *      Author: srettie & lucamoleri
 */
#include "MyMainFrame.h"
#include <TApplication.h>
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGTab.h>
# include <TTree.h>
#include <TRootEmbeddedCanvas.h>
#include "TrackCandidateSelector.h"

#include "Hit.h"
#include "HitManager.h"
#include "RecoManager.h"
#include "DetectorComponent.h"
#include "QuadrupletGeometryProviderBuilder.h"
#include "TGFrame.h"

#include "TestBeamConfigHandler.h"
#include "XercesParser.h"
#include "XMLHandlerStore.h"

MyMainFrame::MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h,const char* fname_in) : TGMainFrame(p,w,h){

  // Setup XML parser
  XMLHandlerStore *hs=XMLHandlerStore::GetHandlerStore();
  auto tbch= std::make_unique<TestBeamConfigHandler>("TestBeamTrigConfig");
  XercesParser xp;
  xp.ParseFileAndNavigate("xml/testBeamConfig18.xml");

  // create title frame

  TGHorizontalFrame *titleFrame = new TGHorizontalFrame(this,200,40);
  titleText = new TGLabel(titleFrame, "Click \"Next Event\" Button To Start");
  titleFrame->AddFrame(titleText, new TGLayoutHints(kLHintsCenterX, 5,5,10,0));
  AddFrame(titleFrame, new TGLayoutHints(kLHintsCenterX, 2,2,2,0));

  // create nextEvt and prevEvt and exit buttons frame

  TGCompositeFrame *fcomposite1 = new TGCompositeFrame(this, 60, 20, kHorizontalFrame);
  TGTextButton *bPrev = new TGTextButton(fcomposite1, "&previous event");
  TGTextButton *bNext = new TGTextButton(fcomposite1, "&next event");
  //TGTextButton *bPrint = new TGTextButton(fcomposite1, "&print event");
  bNext->Connect("Clicked()","MyMainFrame",this,"ProcessEvent(=1)");
  bPrev->Connect("Clicked()","MyMainFrame",this,"ProcessEvent(=0)");
  //bPrint->Connect("Clicked()","MyMainFrame",this,"PrintEvent()");
  fcomposite1->AddFrame(bPrev, new TGLayoutHints(kLHintsNoHints,5,5,3,4));
  fcomposite1->AddFrame(bNext, new TGLayoutHints(kLHintsNoHints,5,5,3,4));
  TGTextButton *bExit = new TGTextButton(fcomposite1, "&Exit", "gApplication->Terminate(0)");
  fcomposite1->AddFrame(bExit, new TGLayoutHints(kLHintsNoHints,5,5,3,4));

  AddFrame(fcomposite1, new TGLayoutHints(kLHintsNoHints,5,5,3,4));

  //--------- create the Tab widget
  TGTab *fTab = new TGTab(this, 1600, 1600);
  TGLayoutHints *fL3 = new TGLayoutHints(kLHintsTop, 5, 5, 5, 5);

  // create hits tab

  TGCompositeFrame *tf = fTab->AddTab("Hits");

  TGCompositeFrame *fcomposite2 = new TGCompositeFrame(tf, 6000, 6000, kHorizontalFrame);
  TGLayoutHints *fL1 = new TGLayoutHints( kLHintsCenterY, 5, 5, 5, 5);

  AddFrame(fTab, new TGLayoutHints(kLHintsCenterY, 5, 5, 5, 5));

  fEc1 = new TRootEmbeddedCanvas("ec1", fcomposite2, 600, 600);
  fEc1->GetCanvas()->Divide(1, 4);
  fcomposite2->AddFrame(fEc1, fL1);
  fEc2 = new TRootEmbeddedCanvas("ec2", fcomposite2, 600, 600);
  fEc2->GetCanvas()->Divide(1, 4);
  fcomposite2->AddFrame(fEc2, fL1);
  tf->AddFrame(fcomposite2, fL1);

  // create tracks tab

  tf = fTab->AddTab("Tracks");
  TGCompositeFrame *fcomposite3 = new TGCompositeFrame(tf, 6000, 6000, kHorizontalFrame);

  AddFrame(fTab, new TGLayoutHints( kLHintsTop, 5, 5, 5, 5));

  fEc3 = new TRootEmbeddedCanvas("ec3", fcomposite3, 600, 600);
  fcomposite3->AddFrame(fEc3, fL1);
  fEc3->GetCanvas()->Divide(1, 4);
  fEc4 = new TRootEmbeddedCanvas("ec4", fcomposite3, 600, 600);
  fcomposite3->AddFrame(fEc4, fL1);
  //fcomposite3->Resize();
  tf->AddFrame(fcomposite3, fL1);

  // Read files
  evtCtr = -1;

  // Setup geometry
  QuadrupletGeometryProviderBuilder qgpb;
  qgpb.SetStripZs(tbch->parDouble("STRIPZL1"),tbch->parDouble("STRIPZL2"),tbch->parDouble("STRIPZL3"),tbch->parDouble("STRIPZL4"));
  qgpb.SetPadZs(tbch->parDouble("PADZL1"),tbch->parDouble("PADZL2"),tbch->parDouble("PADZL3"),tbch->parDouble("PADZL4"));
  qgpb.SetQuadType(tbch->parString("QUADTYPE"));
  gp = qgpb.Build();

  TTree *tOut = new TTree("TB_analysis", "TB_analysis");

  stgc_cb = new ClusterBuilder;

  stgc_cb->SetHitNeighborWindow(1);
  stgc_cb->SetNeighborDistance(3.2);
  stgc_cb->SetMinHits(2);
  stgc_cb->SetMaxHoles(0);
  //stgc_cb->SetDoSWaveCorrection(false);
  stgc_cb->SetDoGaussianClustering(tbch->par("DOGAUSCLUSTERING"));

  stgc_cb->SetTree(tOut);
  stgc_cb->Init();

  tb = new TrackBuilder;

  float minIntercept = -500;
  float maxIntercept = 500;
  int interceptBins = 500;
  float minSlope = -10;
  float maxSlope = 10;
  int slopeBins = 100;
  float deltaY = 10;

  tcs = new TrackCandidateSelector;
  tcs->SetHoughTransformParameters(minIntercept,maxIntercept, interceptBins, minSlope, maxSlope, slopeBins);
  tcs->SetDeltaY(deltaY);
  tcs->Init();

  // Setup reco manager
  reco.SetGeometryProvider(gp);
  reco.SetTrackBuilder(tb);
  reco.SetTrackCandidateSelector(tcs);
  reco.SetEventsFname(fname_in);
  reco.SetTrigSignal(tbch->par("TRGELINK"), tbch->par("TRGVMM"), tbch->par("TRGCHAN"));
  reco.SetMaxEvents(tbch->par("MAXEVENTS"));
  reco.SetBCIDWindow(tbch->par("BCIDWINDOW"));
  reco.SetDoFast(tbch->par("DOFAST"));

  if(!reco.InitEventDisplay()){
    std::cout<<"ERROR: Cannot initialize the RecoManager for event display."<<std::endl;
    gApplication->Terminate(0);
  }
  /*
  if(!reco.GetOffsets()){
    std::cout << "ERROR: Cannot find bcid offsets." << std::endl;
    gApplication->Terminate(0);
  }
  */
  // Set a name to the main frame
  SetWindowName("sTGC Test Beam Event Display");

  // Map all subwindows of main frame
  MapSubwindows();

  std::cout << "subwindows mapped" << std::endl;
  // Initialize the layout algorithm
  Resize(GetDefaultSize());

  // Map main frame
  MapWindow();
  std::cout << "mainframe mapped" << std::endl;
}

void MyMainFrame::ProcessEvent(int NextFlag = 1) {

  // Move to next event
  if(NextFlag == 1) evtCtr++;
  else if(!(evtCtr == 0))  evtCtr--;

  // Load event from readout
  if(!reco.ProcessEvent(evtCtr)){
    std::cout<<"Last event reached - exiting."<<std::endl;
    gApplication->Terminate(0);
  }
  // Fill hits from event
  if (!reco.FillHits()){
    std::cout << "ERROR: problems while filling hits." << std::endl;
    gApplication->Terminate(0);
  }

  // Update title
  int hit_event = evtCtr;//reco.GetCurrentEventNumber();
  std::string t = "Event Number: " + std::to_string(hit_event);
  titleText->SetText(t.c_str());

  TCanvas *fCanvas1 = fEc1->GetCanvas();
  TCanvas *fCanvas2 = fEc2->GetCanvas();
  TCanvas *fCanvas3 = fEc3->GetCanvas();
  TCanvas *fCanvas4 = fEc4->GetCanvas();

  std::cout<<"Entry: "<<evtCtr<<std::endl;

  // Book histograms for event display  
  int nstrips = gp->GetNStrips();
  int nbins_pads_x12 = gp->GetNPadsPhi12();
  int nbins_pads_y12 = gp->GetNPadsEta12();
  int nbins_pads_x34 = gp->GetNPadsPhi34();
  int nbins_pads_y34 = gp->GetNPadsEta34();

  // Use hit manager to get event hits
  hm = reco.GetHitManager();

  std::string name = "";

  std::vector<Cluster> event_clusters;

  for(int i = 1; i < 5; ++i){
    std::string si = std::to_string(i);
    std::string l = "_layer" + si;
    int nbinsx_pads;
    int nbinsy_pads;
    if((i==1)||(i==2)){
      nbinsx_pads = nbins_pads_x12;
      nbinsy_pads = nbins_pads_y12;
    }
    else if((i==3)||(i==4)){
      nbinsx_pads = nbins_pads_x34;
      nbinsy_pads = nbins_pads_y34;
    }
    
    // Strips
    name = "h_strip_hits"+l+"_event"+std::to_string(hit_event);
    TH1F* h_strip_hits = new TH1F(name.c_str(), (name+";Strip y position in layer "+si+";ADC counts;").c_str(), nstrips, 1, nstrips+1);

    // Clusters
    name = "h_strip_clusters"+l+"_event"+std::to_string(hit_event);
    TH1F* h_strip_clusters = new TH1F(name.c_str(), (name+";Cluster y position in layer "+si+";ADC counts;").c_str(), 10*nstrips, 1, nstrips+1);

    // Pads
    name = "h_pad_hits"+l+"_event"+std::to_string(hit_event);
    TH2F* h2_pad_hits =  new TH2F(name.c_str(), (name+";Pad x position in layer "+si+";Pad y position in layer "+si+";").c_str(), nbinsx_pads, 1, nbinsx_pads+1, nbinsy_pads, 1, nbinsy_pads+1);

    // Hits
    std::vector<Hit> strip_hits = hm.GetHitsInStripLayer(i);
    std::vector<Cluster> strip_clusters = stgc_cb->BuildClustersForLayer(strip_hits);
    std::vector<Hit> pad_hits = hm.GetHitsInPadLayer(i);
    event_clusters.insert(event_clusters.end(), strip_clusters.begin(), strip_clusters.end());

    // Fill histograms
    for(unsigned int i_hit = 0; i_hit < strip_hits.size(); i_hit++)
      h_strip_hits->Fill(strip_hits[i_hit].GetComponent().GetY(), strip_hits[i_hit].GetAdc());
    for(unsigned int i_hit = 0; i_hit < pad_hits.size(); i_hit++)
      h2_pad_hits->Fill(pad_hits[i_hit].GetComponent().GetX(), pad_hits[i_hit].GetComponent().GetY(), pad_hits[i_hit].GetAdc());
    for(unsigned int i_cl = 0; i_cl < strip_clusters.size(); i_cl++)
      h_strip_clusters->Fill(strip_clusters[i_cl].GetYaligned(), strip_clusters[i_cl].GetTotalAdc());

    // Draw distributions for each layer
    fCanvas1->cd(i);
    h_strip_hits->Draw("histo");
    fCanvas2->cd(i);
    h2_pad_hits->Draw("colz");
    fCanvas3->cd(i);
    h_strip_clusters->Draw("histo");

  }
  
  std::vector<TrackCandidate> trackCandidates = tcs->SelectTrackCandidates(event_clusters);
  std::vector<Track> tracks = tb->FitTracks(trackCandidates);
  tcs->FillAccumulator(event_clusters);
  
  //Draw Hough transform
  fCanvas4->cd();
  tcs->GetAccumulator()->Draw("colz");

  fCanvas1->Update();
  fCanvas2->Update();
  fCanvas3->Update();
  fCanvas4->Update();

}

MyMainFrame::~MyMainFrame() {
  // Clean up used widgets: frames, buttons, layout hints
}
