#ifndef MYMAINFRAME_H
#define MYMAINFRAME_H

/*
 * EventDisplay.h
 *
 *  Created on: Oct 4, 2017
 *      Author: srettie
 */
#include <TGFrame.h>
#include "TRootEmbeddedCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include <TGLabel.h>
#include "Readout.h"
#include "GeometryProvider.h"
#include "HitManager.h"
#include "RecoManager.h"
#include "ClusterBuilder.h"
class MyMainFrame : public TGMainFrame{
protected:

 private:
  TRootEmbeddedCanvas *fEc1;
  TRootEmbeddedCanvas *fEc2;
  TRootEmbeddedCanvas *fEc3;
  TRootEmbeddedCanvas *fEc4;
  TGLabel             *titleText;
  Int_t               evtCtr;
  Int_t               maxEntries;
  TTree              *t_in;
  Readout             readout;
  TFile	             *f_in;
  GeometryProvider    *gp;
  ClusterBuilder   	  *stgc_cb;
  TrackBuilder 		  *tb;
  TrackCandidateSelector *tcs;
  HitManager          hm;
  TTree 			*tOut;
  RecoManager         reco;
 public:
  MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h,const char* fname_in);
   ~MyMainFrame();
  void ProcessEvent(int NextFlag);
  ClassDef(MyMainFrame, 0)
};

#endif
