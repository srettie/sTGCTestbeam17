/*
 * QuadrupletGeometryProviderBuilder.cpp
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov
 */

#include "QuadrupletGeometryProviderBuilder.h"
#include "ComponentDefs.h"

QuadrupletGeometryProviderBuilder::QuadrupletGeometryProviderBuilder():
  layer1_z_strips(-1), layer2_z_strips(-1), layer3_z_strips(-1), layer4_z_strips(-1),
  layer1_z_pads(-1), layer2_z_pads(-1), layer3_z_pads(-1), layer4_z_pads(-1), quad_type("")
{
  // TODO Auto-generated constructor stub

}

QuadrupletGeometryProviderBuilder::~QuadrupletGeometryProviderBuilder() {
  // TODO Auto-generated destructor stub
}

void QuadrupletGeometryProviderBuilder::SetStripZs(float l1, float l2, float l3, float l4)
{
  layer1_z_strips = l1;
  layer2_z_strips = l2;
  layer3_z_strips = l3;
  layer4_z_strips = l4;
}

void QuadrupletGeometryProviderBuilder::SetPadZs(float l1, float l2, float l3, float l4)
{
  layer1_z_pads = l1;
  layer2_z_pads = l2;
  layer3_z_pads = l3;
  layer4_z_pads = l4;
}

void QuadrupletGeometryProviderBuilder::SetQuadType(std::string type)
{
  quad_type = type;
}

std::string QuadrupletGeometryProviderBuilder::GetQuadType()
{
  return quad_type;
}

GeometryProvider* QuadrupletGeometryProviderBuilder::Build(){
  GeometryProvider* gp = new GeometryProvider();
  gp->SetQuadType(quad_type);
  // layer		board pads	board strips
  // 1			0			4
  // 2			1			5
  // 3			2			6
  // 4			3			7
  //
  // layers mounted back to back: PSSPPSSP, with 1/2 pitch (1.6mm) shift from even and odd layers

  double strip_pitch = 3.2;
  double x_ch = 0;
  double y_ch = 0;
  double z_ch = 0;
  
  if(quad_type == "QS2"){
    // Add strips to geometry provider
    for(int i_layer = 1; i_layer <= 4; i_layer++)
      {
	double shift = (i_layer % 2)? 0 : 1.6;
	int board = i_layer + 3;
	// Changed pad layer 2 elink to strips layer 3!!!
	if (i_layer == 3)
	  board = 1;
	if (i_layer == 1)
	  z_ch = layer1_z_strips;
	if (i_layer == 2)
	  z_ch = layer2_z_strips;
	if (i_layer == 3)
	  z_ch = layer3_z_strips;
	if (i_layer == 4)
	  z_ch = layer4_z_strips;
	for (int i_vmm = 7; i_vmm > 3; i_vmm--){
	    for (int i_ch = 0; i_ch < 64; i_ch++){
		y_ch = ((97 + (7 - i_vmm) * 64) - i_ch - 1) * strip_pitch; // y starts from 0 at the center of the first strip
		// Flip wrt center of detector for L2/L3
		if(i_layer == 2 || i_layer == 3)
		  y_ch = (364*3.2) - y_ch;
		y_ch += shift;
		x_ch = 0;
		gp->AddComponent(board, i_vmm, i_ch, x_ch, y_ch, z_ch, i_layer, StripComponent);
	      }
	  }
      }

    // Add pads to geometry provider
    //using the mapping in https://docs.google.com/spreadsheets/d/1Af1FKVlMSCWc5CWSoA0Wma-cthdPx-dF0eZz9emX1uU/edit#gid=1132762987

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // USING A RIGHT-HANDED COORDINATE SYSTEM WHERE THE BEAM GOES INTO THE POSITIVE Z DIRECTION
    // THE ORIGIN OF THE COORDINATE SYSTEM IS IN THE BOTTOM RIGHT CORNER OF THE QUAD, AS SEEN BY THE BEAM
    /////////////////////////////////////////////////////////////////////////////////////////////////////
	    
    //board=layer-1
    //              board=0, i_vmm, i_ch, x_ch, y_ch, z_ch, i_layer=1
    gp->AddComponent(0, 1, 0, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 1, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 2, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 3, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 4, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 5, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 6, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 7, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 8, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 9, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 10, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 11, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 12, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 13, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 14, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 15, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 16, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 17, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 18, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 19, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 20, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 21, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 22, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 23, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 24, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 25, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 26, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 27, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 28, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 29, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 30, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 31, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 32, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 33, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 34, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 35, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 36, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 37, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 38, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 39, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 40, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 41, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 42, 1, 1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 43, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 44, 2, 1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 45, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 46, 1, 2, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 47, 2, 2, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 48, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 49, 1, 3, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 50, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 51, 2, 3, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 52, 1, 4, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 53, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 54, 2, 4, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 55, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 56, 1, 5, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 57, 2, 5, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 58, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 59, 1, 6, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 60, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 61, 2, 6, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 62, 1, 7, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 1, 63, -1, -1, layer2_z_pads, 1, PadComponent);

    gp->AddComponent(0, 2, 0, 2, 7, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 1, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 2, 1, 8, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 3, 2, 8, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 4, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 5, 1, 9, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 6, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 7, 2, 9, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 8, 1, 10, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 9, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 10, 2, 10, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 11, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 12, 1, 11, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 13, 2, 11, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 14, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 15, 1, 12, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 16, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 17, 2, 12, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 18, 1, 13, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 19, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 20, 2, 13, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 21, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 22, 1, 14, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 23, 2, 14, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 24, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 25, 1, 15, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 26, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 27, 2, 15, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 28, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 29, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 30, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 31, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 32, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 33, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 34, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 35, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 36, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 37, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 38, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 39, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 40, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 41, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 42, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 43, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 44, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 45, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 46, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 47, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 48, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 49, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 50, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 51, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 52, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 53, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 54, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 55, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 56, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 57, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 58, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 59, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 60, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 61, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 62, -1, -1, layer2_z_pads, 1, PadComponent);
    gp->AddComponent(0, 2, 63, -1, -1, layer2_z_pads, 1, PadComponent);

    gp->AddComponent(1, 1, 0, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 1, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 2, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 3, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 4, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 5, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 6, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 7, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 8, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 9, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 10, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 11, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 12, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 13, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 14, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 15, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 16, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 17, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 18, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 19, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 20, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 21, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 22, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 23, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 24, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 25, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 26, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 27, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 28, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 29, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 30, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 31, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 32, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 33, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 34, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 35, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 36, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 37, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 38, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 39, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 40, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 41, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 42, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 43, 2, 15, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 44, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 45, 2, 14, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 46, 1, 13, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 47, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 48, 2, 12, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 49, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 50, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 51, 2, 11, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 52, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 53, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 54, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 55, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 56, 1, 10, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 57, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 58, 2, 9, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 59, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 60, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 61, 2, 8, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 62, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 1, 63, 1, 8, layer2_z_pads, 2, PadComponent);

    gp->AddComponent(1, 2, 0, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 1, 2, 7, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 2, 1, 6, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 3, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 4, 2, 6, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 5, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 6, 1, 5, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 7, 2, 5, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 8, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 9, 1, 4, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 10, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 11, 2, 4, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 12, 1, 3, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 13, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 14, 2, 3, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 15, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 16, 1, 2, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 17, 2, 2, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 18, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 19, 1, 1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 20, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 21, 2, 1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 22, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 23, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 24, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 25, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 26, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 27, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 28, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 29, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 30, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 31, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 32, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 33, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 34, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 35, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 36, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 37, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 38, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 39, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 40, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 41, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 42, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 43, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 44, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 45, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 46, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 47, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 48, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 49, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 50, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 51, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 52, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 53, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 54, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 55, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 56, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 57, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 58, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 59, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 60, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 61, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 62, -1, -1, layer2_z_pads, 2, PadComponent);
    gp->AddComponent(1, 2, 63, -1, -1, layer2_z_pads, 2, PadComponent);

    gp->AddComponent(2, 1, 0, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 1, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 2, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 3, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 4, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 5, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 6, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 7, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 8, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 9, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 10, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 11, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 12, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 13, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 14, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 15, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 16, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 17, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 18, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 19, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 20, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 21, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 22, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 23, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 24, 3, 15, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 25, 2, 15, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 26, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 27, 1, 15, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 28, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 29, 3, 14, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 30, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 31, 2, 14, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 32, 1, 14, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 33, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 34, 3, 13, layer2_z_pads, 3, PadComponent);// WAS 1, 12
    gp->AddComponent(2, 1, 35, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 36, 2, 13, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 37, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 38, 1, 13, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 39, 3, 12, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 40, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 41, 2, 12, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 42, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 43, 1, 12, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 44, 3, 11, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 45, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 46, 2, 11, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 47, 1, 11, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 48, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 49, 3, 10, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 50, 2, 10, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 51, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 52, 1, 10, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 53, 3, 9, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 54, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 55, 2, 9, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 56, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 57, 1, 9, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 58, 3, 8, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 59, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 60, 2, 8, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 61, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 1, 62, 1, 8, layer2_z_pads, 3, PadComponent);// WAS -1, -1
    gp->AddComponent(2, 1, 63, 3, 7, layer2_z_pads, 3, PadComponent);

    gp->AddComponent(2, 2, 0, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 1, 2, 7, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 2, 1, 7, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 3, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 4, 3, 6, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 5, 2, 6, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 6, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 7, 1, 6, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 8, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 9, 3, 5, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 10, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 11, 2, 5, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 12, 1, 5, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 13, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 14, 3, 4, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 15, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 16, 2, 4, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 17, 1, 4, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 18, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 19, 3, 3, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 20, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 21, 2, 3, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 22, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 23, 1, 3, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 24, 3, 2, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 25, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 26, 2, 2, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 27, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 28, 1, 2, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 29, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 30, 3, 1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 31, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 32, 2, 1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 33, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 34, 1, 1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 35, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 36, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 37, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 38, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 39, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 40, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 41, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 42, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 43, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 44, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 45, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 46, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 47, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 48, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 49, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 50, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 51, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 52, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 53, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 54, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 55, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 56, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 57, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 58, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 59, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 60, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 61, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 62, -1, -1, layer2_z_pads, 3, PadComponent);
    gp->AddComponent(2, 2, 63, -1, -1, layer2_z_pads, 3, PadComponent);

    gp->AddComponent(3, 1, 0, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 1, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 2, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 3, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 4, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 5, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 6, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 7, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 8, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 9, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 10, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 11, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 12, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 13, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 14, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 15, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 16, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 17, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 18, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 19, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 20, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 21, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 22, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 23, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 24, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 25, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 26, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 27, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 28, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 29, 3, 1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 30, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 31, 2, 1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 32, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 33, 1, 1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 34, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 35, 3, 2, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 36, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 37, 2, 2, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 38, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 39, 1, 2, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 40, 3, 3, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 41, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 42, 2, 3, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 43, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 44, 1, 3, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 45, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 46, 3, 4, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 47, 2, 4, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 48, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 49, 1, 4, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 50, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 51, 3, 5, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 52, 2, 5, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 53, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 54, 1, 5, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 55, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 56, 3, 6, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 57, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 58, 2, 6, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 59, 1, 6, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 60, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 61, 3, 7, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 62, 2, 7, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 1, 63, -1, -1, layer2_z_pads, 4, PadComponent);

    gp->AddComponent(3, 2, 0, 1, 7, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 1, 3, 8, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 2, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 3, 2, 8, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 4, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 5, 1, 8, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 6, 3, 9, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 7, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 8, 2, 9, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 9, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 10, 1, 9, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 11, 3, 10, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 12, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 13, 2, 10, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 14, 1, 10, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 15, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 16, 3, 11, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 17, 2, 11, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 18, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 19, 1, 11, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 20, 3, 12, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 21, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 22, 2, 12, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 23, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 24, 1, 12, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 25, 3, 13, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 26, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 27, 2, 13, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 28, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 29, 1, 13, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 30, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 31, 3, 14, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 32, 2, 14, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 33, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 34, 1, 14, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 35, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 36, 3, 15, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 37, 2, 15, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 38, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 39, 1, 15, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 40, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 41, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 42, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 43, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 44, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 45, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 46, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 47, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 48, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 49, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 50, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 51, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 52, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 53, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 54, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 55, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 56, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 57, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 58, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 59, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 60, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 61, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 62, -1, -1, layer2_z_pads, 4, PadComponent);
    gp->AddComponent(3, 2, 63, -1, -1, layer2_z_pads, 4, PadComponent);    
  }
  
  else if(quad_type=="QL1P"){
    QL1PLYR1_pads(gp, layer1_z_pads);
    QL1PLYR2_pads(gp, layer2_z_pads);
    QL1PLYR3_pads(gp, layer3_z_pads);
    QL1PLYR4_pads(gp, layer4_z_pads);
    QL1PLYR14_strips(gp, 1, layer1_z_strips);
    QL1PLYR23_strips(gp, 2, layer2_z_strips);
    QL1PLYR23_strips(gp, 3, layer3_z_strips);
    QL1PLYR14_strips(gp, 4, layer4_z_strips);
  }
  else if(quad_type=="QS3P"){
    QS3PLYR1_pads(gp, layer1_z_pads);
    QS3PLYR2_pads(gp, layer2_z_pads);
    QS3PLYR3_pads(gp, layer3_z_pads);
    QS3PLYR4_pads(gp, layer4_z_pads);    
    QS3PLYR14_strips(gp, 1, layer1_z_strips);
    QS3PLYR23_strips(gp, 2, layer2_z_strips);
    QS3PLYR23_strips(gp, 3, layer3_z_strips);
    QS3PLYR14_strips(gp, 4, layer4_z_strips);
  }
  else{
    // TODO - ADD ACTUAL ERROR HANDLING HERE...
    std::cout<<"ERROR: Quad type \""<<quad_type<<"\" not found!!"<<std::endl;
  }

	
  return gp;
}
