/*
 * QuadrupletGeometryProviderBuilder.h
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov
 */

#ifndef QUADRUPLETGEOMETRYPROVIDERBUILDER_H_
#define QUADRUPLETGEOMETRYPROVIDERBUILDER_H_
#include "GeometryProvider.h"



class QuadrupletGeometryProviderBuilder {
public:
	QuadrupletGeometryProviderBuilder();
	virtual ~QuadrupletGeometryProviderBuilder();
	void SetStripZs(float l1, float l2, float l3, float l4);
	void SetPadZs(float l1, float l2, float l3, float l4);
	void SetQuadType(std::string);
	std::string GetQuadType();
	GeometryProvider* Build();
protected:
	float layer1_z_strips;
	float layer2_z_strips;
	float layer3_z_strips;
	float layer4_z_strips;

	float layer1_z_pads;
	float layer2_z_pads;
	float layer3_z_pads;
	float layer4_z_pads;
	std::string quad_type;	
};

#endif /* QUADRUPLETGEOMETRYPROVIDERBUILDER_H_ */
