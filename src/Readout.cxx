
#ifndef Readout_cxx
#define Readout_cxx
#include "Readout.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
Readout::Readout(TTree *tree) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  /*if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/outDecoded.root");
    if (!f || !f->IsOpen()) {
    f = new TFile("data/outDecoded.root");
    }
    f->GetObject("decoded_event_tree",tree);

    }*/
  Init(tree);
}

Readout::~Readout()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t Readout::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t Readout::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void Readout::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  m_tdo = 0;
  m_pdo = 0;
  m_chan = 0;
  m_vmm_id = 0;
  m_elink_id = 0;
  m_bcid_rel = 0;
  m_bcid = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("m_tdo", &m_tdo, &b_m_tdo);
  fChain->SetBranchAddress("m_pdo", &m_pdo, &b_m_pdo);
  fChain->SetBranchAddress("m_chan", &m_chan, &b_m_chan);
  fChain->SetBranchAddress("m_vmm_id", &m_vmm_id, &b_m_vmm_id);
  fChain->SetBranchAddress("m_elink_id", &m_elink_id, &b_m_elink_id);
  fChain->SetBranchAddress("m_bcid_rel", &m_bcid_rel, &b_m_bcid_rel);
  fChain->SetBranchAddress("m_bcid", &m_bcid, &b_m_bcid);
  //fChain->SetBranchAddress("m_evid", &m_evid, &b_m_evid);
  fChain->SetBranchAddress("m_nhits", &m_nhits, &b_m_nhits);
  Notify();
}

Bool_t Readout::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void Readout::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t Readout::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.

  // To remove compilation warning
  int a = entry;
  return 1;
}





void Readout::Loop()
{
//   In a ROOT session, you can do:
//      root> .L Readout.C
//      root> Readout t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
   }
}
#endif
