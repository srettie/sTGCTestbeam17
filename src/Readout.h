//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct  3 22:54:43 2017 by ROOT version 6.11/01
// from TTree decoded_event_tree/decoded_event_tree
// found on file: data/outDecoded.root
//////////////////////////////////////////////////////////

#ifndef Readout_h
#define Readout_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class Readout {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  std::vector<unsigned int> *m_tdo;
  std::vector<unsigned int> *m_pdo;
  std::vector<unsigned int> *m_chan;
  std::vector<unsigned int> *m_vmm_id;
  std::vector<unsigned int> *m_elink_id;
  std::vector<unsigned int> *m_bcid_rel;
  std::vector<unsigned int> *m_bcid;
  //Int_t           m_evid;
  Int_t           m_nhits;

  // List of branches
  TBranch        *b_m_tdo;   //!
  TBranch        *b_m_pdo;   //!
  TBranch        *b_m_chan;   //!
  TBranch        *b_m_vmm_id;   //!
  TBranch        *b_m_elink_id;   //!
  TBranch        *b_m_bcid_rel;   //!
  TBranch        *b_m_bcid;   //!
  //TBranch        *b_m_evid;   //!
  TBranch        *b_m_nhits;   //!

  Readout(TTree *tree=0);
  virtual ~Readout();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

//#endif

//#ifdef Readout_cxx

#endif // #ifdef Readout_cxx
