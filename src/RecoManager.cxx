/*
 * RecoManager.cpp
 *
 *  Created on: Sep 25, 2017
 *      Author: ekajomov
 */

#include "RecoManager.h"
#include "ClusterBuilder.h"
#include "TrackCandidateSelector.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include "TFile.h"
#include "TKey.h"

#include <memory>


#define DBG_S std::cout << "S123: "<< __LINE__ << __FILE__<< std::endl;



RecoManager::RecoManager(): gp(0), tReadout(0), f_events(), f_out(0), stgc_cb(0), tcs(0), tOut(0), maxEvents(-1), doFast(false)
{

}

RecoManager::~RecoManager() {
  // TODO Auto-generated destructor stub
}

void RecoManager::SetGeometryProvider(GeometryProvider* in_gp) {
  this->gp = in_gp;
}

GeometryProvider* RecoManager::GetGeometryProvider() {
  return this->gp;
}

ClusterBuilder* RecoManager::GetClusterBuilder() {
  return this->stgc_cb;
}
HitManager RecoManager::GetHitManager(){
  return this->hm;
}


void RecoManager::SetEventsFname(std::string in_events_fname) {
  std::cout << "INFO: Setting events file name: " << in_events_fname << std::endl;
  this->events_fname = in_events_fname;
}

void RecoManager::SetOutputFname(std::string in_output_fname) {
  std::cout << "INFO: Setting output file name: " << in_output_fname << std::endl;
  this->output_fname = in_output_fname;
}

void RecoManager::SetOutputLogFname(std::string in_output_log_fname) {
  std::cout << "INFO: Setting output file name: " << in_output_log_fname << std::endl;
  this->output_log_fname = in_output_log_fname;
}

void RecoManager::SetOutputProcessedFname(std::string in_output_processed_fname) {
  std::cout << "INFO: Setting output file name: " << in_output_processed_fname << std::endl;
  this->output_processed_fname = in_output_processed_fname;
}

void RecoManager::SetSTgcClusterBuilder( ClusterBuilder* cb )
{
  std::cout << "INFO: Setting STGC Cluster Builder: " <<  std::endl;
  this->stgc_cb = cb;
  reco_tools.push_back(cb);
}

void RecoManager::SetTrackBuilder(TrackBuilder* in_tbuilder)
{
  std::cout << "INFO: Setting STGC Track Builder: " <<  std::endl;
  this->tbuilder = in_tbuilder;
  reco_tools.push_back(in_tbuilder);
}

void RecoManager::SetTrigSignal(int in_trig_elink, int in_trig_vmm, int in_trig_chan)
{
  this->m_trig_elink = in_trig_elink;
  this->m_trig_vmm = in_trig_vmm;
  this->m_trig_chan = in_trig_chan;
}

void RecoManager::SetBCIDWindow(int in_bcid_window)
{
  this->m_bcid_window = in_bcid_window;
}

bool RecoManager::InitEventDisplay(){
  std::cout << "INFO: Initializing event display reconstruction manager" << std::endl;
  if (events_fname == "" )
    {
      std::cout << "ERROR: Events filename is empty. Exit" << std::endl;
      return false;
    }
  std::cout << "INFO: Opening events file: " << events_fname << std::endl;
  f_events = TFile::Open(events_fname.c_str());
  if (f_events == 0)
    {
      std::cout << "ERROR: Cannot open the events file. Exit" << std::endl;
      return false;
    }
  ///////////// this is the part where we setup the tree to be readout /////
  f_events->GetObject("decoded_event_tree", tReadout);
  if (tReadout == 0)
    {
      std::cout << "ERROR: Cannot open extract the readout tree. Exit" << std::endl;
      return false;
    }
  std::cout << "INFO: Setting up the readout reader" << std::endl;
  readout.Init(tReadout);

  /////////f_out = TFile::Open(output_fname.c_str(), "RECREATE");
  /////////tOut = new TTree("TB_analysis", "TB_analysis");

  /////////f_out_log= std::ofstream();
  /////////f_out_log.open(output_log_fname.c_str());
  /////////f_out_log<<"Event \t Board \t VMM \t Channel \t BCID \t Trigger BCID \t PDO \t TDO"<<std::endl;
  //f_out_log << "event_number" << "\t" << "number_of_tracks" << "\t" << "track_intercept" << "\t" << "track_slope" << "\n";
  
  //hm.SetTree(tOut);
  reco_tools.push_back(&hm);

  std::cout << "INFO: Initialization of event display successful" << std::endl;
  f_events->cd();
  return true;

}

bool RecoManager::ProcessEvent(int evtCtr){
  long nentries = tReadout->GetEntriesFast();
  long maxEntries = (maxEvents == -1 || (nentries < maxEvents) ) ? nentries : maxEvents;
  if(evtCtr < maxEntries){
    readout.GetEntry(evtCtr);
    return true;
  }
  else
    return false;
}

bool RecoManager::Init() {
  std::cout << "INFO: Initializing reconstruction manager" << std::endl;
  if (events_fname == "" )
    {
      std::cout << "ERROR: Events filename is empty. Exit" << std::endl;
      return false;
    }
  std::cout << "INFO: Opening events file: " << events_fname << std::endl;
  f_events = TFile::Open(events_fname.c_str());
  if (f_events == 0)
    {
      std::cout << "ERROR: Cannot open the events file. Exit" << std::endl;
      return false;
    }
  ///////////// this is the part where we setup the tree to be readout /////
  f_events->GetObject("decoded_event_tree", tReadout);
  if (tReadout == 0)
    {
      std::cout << "ERROR: Cannot open extract the readout tree. Exit" << std::endl;
      return false;
    }
  std::cout << "INFO: Setting up the readout reader" << std::endl;
  readout.Init(tReadout);

  f_out = TFile::Open(output_fname.c_str(), "RECREATE"); // analysis output tree
  //f_out_processed = TFile::Open(output_processed_fname.c_str(), "RECREATE");  // processed data output tree
  tOut = new TTree("TB_analysis", "TB_analysis");

  // tOutProcess = new TTree("TB_processed", "TB_processed");
  // tOutProcess->Branch("hits_strips_l1", &hits_processed_strips_l1);
  // tOutProcess->Branch("hits_strips_l2", &hits_processed_strips_l2);
  // tOutProcess->Branch("hits_strips_l3", &hits_processed_strips_l3);
  // tOutProcess->Branch("hits_strips_l4", &hits_processed_strips_l4);
  // tOutProcess->Branch("hits_pads_l1", &hits_processed_pads_l1);
  // tOutProcess->Branch("hits_pads_l2", &hits_processed_pads_l2);
  // tOutProcess->Branch("hits_pads_l3", &hits_processed_pads_l3);
  // tOutProcess->Branch("hits_pads_l4", &hits_processed_pads_l4);
  // tOutProcess->Branch("clusters_strips_l1", &clusters_processed_strips_l1);
  // tOutProcess->Branch("clusters_strips_l2", &clusters_processed_strips_l2);
  // tOutProcess->Branch("clusters_strips_l3", &clusters_processed_strips_l3);
  // tOutProcess->Branch("clusters_strips_l4", &clusters_processed_strips_l4);
  // tOutProcess->Branch("tracks", &tracks_processed);


  //f_out_log= std::ofstream();
  //f_out_log.open(output_log_fname.c_str());
  //f_out_log<<"Event \t Board \t VMM \t Channel \t BCID \t Trigger BCID \t PDO \t TDO"<<std::endl;
  //f_out_log << "event_number" << "\t" << "number_of_tracks" << "\t" << "track_intercept" << "\t" << "track_slope" << "\n";
  
  hm.SetTree(tOut);
  reco_tools.push_back(&hm);

  if (stgc_cb == 0)
    {
      std::cout << "ERROR: ClusterBuilder is NULL. Exit" << std::endl;
      return false;
    }
  stgc_cb->SetTree(tOut);
  stgc_cb->Init();

  if (tcs == 0)
    {
      std::cout << "ERROR: TrackCandidateSelector is NULL. Exit" << std::endl;
      return false;
    }
  tcs->SetTree(tOut);
  tcs->Init();

  if (tbuilder == 0)
    {
      std::cout << "ERROR: TrackBuilder. Exit" << std::endl;
      return false;
    }
  tbuilder->SetTree(tOut);
  tbuilder->Init();

  
  // Booking output histograms
  BookHistograms(GetGeometryProvider());

  std::cout << "INFO: Initialization successful" << std::endl;
  f_events->cd();
  return true;

}

void RecoManager::Loop(){//std::vector<std::vector<float>> in_parameters) {

  std::cout << "INFO: Processing events" << std::endl;
  long nentries = tReadout->GetEntriesFast();
  std::cout << "INFO: " << nentries << " events exist in the input file" << std::endl;

  long maxEntries = (maxEvents == -1 || (nentries < maxEvents) ) ? nentries : maxEvents;
  long nbytes = 0, nb = 0;

  /* NO NEED TO GET OFFSETS WITH 2.3a BOARDS
  if(!GetOffsets())
    {
      std::cout << "ERROR: problems while finding bcid offsets - Exit" << std::endl;
      return;
    }
  */
  
  if(!GetTDOCalibrations())
    {
      std::cout << "ERROR: problems while calibrating TDO - Exit" << std::endl;
      return;
    }
  
  
  
  for (long jentry=0; jentry<maxEntries;jentry++) {
    nb = readout.GetEntry(jentry);   nbytes += nb;

    PreEventProcessing(); // this notifies the tools so they prepare themselves to recieve a new event

    int event = jentry;//readout.m_evid;
    if(event%1000==0)
      std::cout << "Event: " << event << std::endl;

    if (!FillHits())
      {
	std::cout << "ERROR: problems while filling hits - Skipping event" << std::endl;
	continue;
	//return;
      }

    // Get Hits
    hits_processed_strips_l1 = hm.GetHitsInStripLayer(1);
    hits_processed_strips_l2 = hm.GetHitsInStripLayer(2);
    hits_processed_strips_l3 = hm.GetHitsInStripLayer(3);
    hits_processed_strips_l4 = hm.GetHitsInStripLayer(4);

    hits_processed_pads_l1 = hm.GetHitsInPadLayer(1);
    hits_processed_pads_l2 = hm.GetHitsInPadLayer(2);
    hits_processed_pads_l3 = hm.GetHitsInPadLayer(3);
    hits_processed_pads_l4 = hm.GetHitsInPadLayer(4);

    if(event < 5){
      std::cout<<"Layer 1/2/3/4 has "
	       <<hits_processed_strips_l1.size()<<"/"
	       <<hits_processed_strips_l2.size()<<"/"
	       <<hits_processed_strips_l3.size()<<"/"
	       <<hits_processed_strips_l4.size()<<" strip hits."<<std::endl;
      std::cout<<"Layer 1/2/3/4 has "
	       <<hits_processed_pads_l1.size()<<"/"
	       <<hits_processed_pads_l2.size()<<"/"
	       <<hits_processed_pads_l3.size()<<"/"
	       <<hits_processed_pads_l4.size()<<" pad hits."<<std::endl;
    }

    // Fill histograms
    FillHistograms();

    // This notifies the tools so they prepare themselves to say bye to the event
    PostEventProcessing();
  }
}

void RecoManager::Finalize() {
  std::cout << "Finishing reconstruction" << std::endl;
  f_out->cd();
  tOut->Write();
  //tOutProcess->Write();
  WriteHistograms();

  f_out->Write();
  f_out->Close();
}

void RecoManager::SetTrackCandidateSelector(TrackCandidateSelector* in_tcs) {
  tcs = in_tcs;
  reco_tools.push_back(tcs);
}

bool RecoManager::GetOffsets(){
  // Fill the BCID offsets for each board/VMM configuration
  
  // Template histogram for bcid offsets
  TH1F *tmp_hist = new TH1F();

  // Get Keys from root file
  TObject *obj;
  TKey *key;
  TIter next( f_events->GetListOfKeys() );

  // Clone temporary hist for later
  while ((key = (TKey *) next())) {
    obj = f_events->Get(key->GetName()); // copy object to memory  
    // Check that this is a TH1F
    if(obj->IsA() == TH1F::Class()){
      std::string name = key->GetName();
      // We only need to clone the first histogram
      if(name.find("diff_bcid") != std::string::npos){
	tmp_hist = (TH1F*)obj->Clone();
	tmp_hist->Reset();
	break;
      }
    }
  }
  
  ///////////////////////////////
  // Now fill in all the offsets
  ///////////////////////////////
  
  // Loop over elinks (i.e. boards)
  for(int i_elink = 0; i_elink < 8; i_elink++){
    // Loop over VMMs 0 <-> A, 1 <-> B, 2 <-> C
    for(int i_vmm = 0; i_vmm < 8; i_vmm++){
      // Get Keys from root file
      TIter next( f_events->GetListOfKeys() );
      // Loop over all keys and fill temporary histogram for this board/VMM
      while ((key = (TKey *) next())) {
	    obj = f_events->Get(key->GetName()); // copy object to memory
        // Check that this is a TH1F
        if(obj->IsA() != TH1F::Class())
        continue;
        std::string name = key->GetName();
        // Only consider "diff_bcid" histograms
        if(name.find("diff_bcid") == std::string::npos)
        continue;
        std::string s_elink = name.substr(name.find("elink_")+6,1);
        std::string s_vmm = name.substr(name.find("vmm_")+4,1);
        std::string s_ch = name.substr(name.find("chan_")+5,2);
        // Don't add trigger signal to find offsets...
        if(!s_elink.compare(std::to_string(GetTrigSignalBoard())) &&
        !s_vmm.compare(std::to_string(GetTrigSignalVMM())) &&
        !s_ch.compare(std::to_string(GetTrigSignalChan())) )
        continue;

        // Add all offsets for this board/VMM
        if( !s_elink.compare(std::to_string(i_elink)) && !s_vmm.compare(std::to_string(i_vmm)) ){
	  //std::cout<<"Adding Board: "<<i_elink<<", VMM: "<<i_vmm<<", Key:"<<key->GetName()<<std::endl;
	  TH1F *h = (TH1F*)obj;
	  //std::cout<<"Maximum is "<<h->GetXaxis()->GetBinCenter(h->GetMaximumBin())<<", weighted by number of entries "<<h->GetEntries()<<std::endl;
	  tmp_hist->Add((TH1F*)obj);
        }
      }
      //DBG_S
      // Finally get the proper offset for this board/VMM
      int offset = (int) tmp_hist->GetXaxis()->GetBinCenter(tmp_hist->GetMaximumBin());
      //DBG_S;std::cout<<" OFFSET "<<offset<<std::endl;
      if(offset > -1){
          offsets_bcid[i_elink][i_vmm] = offset;
      }
      else{
          offsets_bcid[i_elink][i_vmm] = -1;
      }
      //DBG_S
      // Only print out if we have an offset...
      if(offsets_bcid[i_elink][i_vmm] > -1){
          std::cout<<"Board: "<<i_elink<<", VMM: "<<i_vmm<<", Offset: "<<offsets_bcid[i_elink][i_vmm]<<std::endl;
      }
      //DBG_S
    
      // Only save if calling from reco manager; not from event display
      if(f_out){
        std::string tmp_name = "elink_" + std::to_string(i_elink) + "_vmm_" + std::to_string(i_vmm) + "_offset_" + std::to_string(offsets_bcid[i_elink][i_vmm]);
        tmp_hist->SetName(tmp_name.c_str());
        tmp_hist->SetTitle(tmp_name.c_str());
        f_out->cd();
        tmp_hist->Write();
      }

      // Reset histogram for nex board/VMM pair
      tmp_hist->Reset();

      // Change back to tree after writing output
      f_events->cd();

    }
  }
  return true;
}

bool RecoManager::GetTDOCalibrations()
{
  // Fill the TDO calibrations for each board/VMM/Channel configuration
  
  // Get Keys from root file
  TObject *obj;
  TKey *key;

  
  ////////////////////////////////
  // Now get the TDO calibrations
  ////////////////////////////////

  // Default is 3...
  //SetInTimePeak(3);
  //std::map<int, TH1F*> h_in_time_peaks;
  
  // Get Keys from root file
  TIter next( f_events->GetListOfKeys() );
  // Loop over all keys and fill calibration data
  while ((key = (TKey *) next())) {
    obj = f_events->Get(key->GetName()); // copy object to memory
    // Check that this is a TH1F
    if(obj->IsA() != TH1F::Class())
      continue;
    std::string name = key->GetName();

    // Use the diff_bcid histograms to find in-time peak
    if(name.find("diff_bcid") != std::string::npos){
      std::string s_elink = name.substr(name.find("elink_")+6,2);
      int elink = std::stoi(s_elink) / 4;
      std::string s_vmm = name.substr(name.find("vmm_")+4,1);
      int vmm = capture2VMM[std::stoi(s_vmm)];
      std::string s_ch = name.substr(name.find("chan_")+5,2);
      int ch = std::stoi(s_ch);

      TH1F *h = (TH1F*)obj;
      in_time_peak[elink][vmm][ch] = h->GetBinCenter(h->GetMaximumBin());

      //std::cout<<"Elink: "<<elink<<", VMM: "<<vmm<<", Ch: "<<ch<<" --> "<<in_time_peak[elink][vmm][ch]<<std::endl;
      //if(h_in_time_peaks.find(vmm) == h_in_time_peaks.end())
      //	h_in_time_peaks[vmm] = (TH1F*)obj->Clone();
      //else
      //	h_in_time_peaks[vmm]->Add((TH1F*)obj);
    }
    
    // Only consider "tdo" histograms
    if(name.find("_tdo") == std::string::npos)
      continue;

    //std::cout<<name<<std::endl;
    std::string s_elink = name.substr(name.find("elink_")+6,2);
    int elink = std::stoi(s_elink) / 4;
    std::string s_vmm = name.substr(name.find("vmm_")+4,1);
    int vmm = capture2VMM[std::stoi(s_vmm)];
    std::string s_ch = name.substr(name.find("chan_")+5,2);
    int ch = std::stoi(s_ch);

    // Get the calibration parameters from the histgram for this elink/vmm/channel
    TH1F *h = (TH1F*)obj;

    double noise_level = 0;
    double mean = h->GetMean();
    int range = h->GetBinLowEdge(h->FindLastBinAbove(noise_level)+1) - h->GetBinLowEdge(h->FindFirstBinAbove(noise_level));

    TDO_calib_mean[elink][vmm][ch] = mean;
    TDO_calib_range[elink][vmm][ch] = range;

    // Only print out calibrated channels
    //std::cout<<"Board: "<<elink<<", VMM: "<<vmm<<", Channel: "<<ch<<", TDO mean: "<<TDO_calib_mean[elink][vmm][ch]<<", TDO range: "<<TDO_calib_range[elink][vmm][ch]<<std::endl;
  }

  // Temporary hack to get in-time peak --> TODO - automate this
  // for(auto const& it : h_in_time_peaks){
  //   in_time_peak[it.first] =  it.second->GetBinCenter(it.second->GetMaximumBin());
  //   std::cout<<"VMM: "<<it.first<<", Peak: "<<in_time_peak[it.first]<<std::endl;
  // }
  
  // if(name=="elink_26_vmm_0_chan_17_diff_bcid"){
  //   TH1F *h = (TH1F*)obj;
  //   SetInTimePeak(h->GetBinCenter(h->GetMaximumBin()));
  // }
  
  return true;
}

bool RecoManager::FillHits()
{
  hm.Clear();
  int nHits = readout.m_vmm_id->size();

  if(nHits != readout.m_nhits){
    std::cout<<"ISSUE: NUMBER OF HITS IN VMM IDS NOT EQUAL TO NHITS!!!"<<std::endl;
  }
  
  /********** No digitized trigger signal anymore...
  bool foundTrigger = false;
  int nTriggers = 0;
  int trig_bcid = -1;
  int trig_tdo = -1;
  int max_trig_pdo = -1;

  
  // Check if we have a valid trigger signal
  for (int i_hit = 0; i_hit < nHits; i_hit++){    
    int hit_board = (*readout.m_elink_id)[i_hit];
    int hit_vmm = (*readout.m_vmm_id)[i_hit];
    int hit_channel = (*readout.m_chan)[i_hit];
    int hit_bcid = (*readout.m_bcid)[i_hit];
    int hit_event = readout.m_evid;
    int hit_pdo = (*readout.m_pdo)[i_hit];
    int hit_tdo = (*readout.m_tdo)[i_hit];

    if(hit_board == GetTrigSignalBoard() &&
       hit_vmm == GetTrigSignalVMM() &&
       hit_channel == GetTrigSignalChan()){
      foundTrigger = true;
      nTriggers++;
      if(hit_pdo > max_trig_pdo){
        trig_bcid = hit_bcid;
        max_trig_pdo = hit_pdo;
        trig_tdo = hit_tdo;
      }
    }
  }
  
  if(!foundTrigger){
    std::cout<<"ISSUE: NO TRIGGER SIGNAL FOUND!!!"<<std::endl;
  }
  if(nTriggers > 1){
    std::cout<<"ISSUE: "<<nTriggers<<" TRIGGER SIGNALS FOR SINGLE EVENT!!!"<<std::endl;
  }
  **********/
  for (int i_hit = 0; i_hit < nHits; i_hit++){
    int hit_board = (*readout.m_elink_id)[i_hit] / 4;
    int hit_vmm = capture2VMM[(*readout.m_vmm_id)[i_hit]];
    int hit_channel = (*readout.m_chan)[i_hit];
    int hit_bcid = (*readout.m_bcid)[i_hit];
    int hit_bcid_rel = (*readout.m_bcid_rel)[i_hit];
    //int hit_event = readout.m_evid;
    int hit_pdo = (*readout.m_pdo)[i_hit];
    int hit_tdo = (*readout.m_tdo)[i_hit];

    //f_out_logo<<hit_event<<"\t"<<hit_board<<"\t"<<hit_vmm<<"\t"<<hit_channel<<"\t"<<hit_bcid<<"\t"<<trig_bcid<<"\t"<<hit_pdo<<"\t"<<hit_tdo<<std::endl;

    /*
    std::cout << "Hit - board: " <<  hit_board << std::endl;
    std::cout << " vmm: " << hit_vmm << std::endl;
    std::cout << " channel: " << hit_channel << std::endl;
    std::cout << " bcid: " << hit_bcid << std::endl;
    std::cout << " bcid_rel: " << hit_bcid_rel << std::endl;
    //std::cout << " event: " << hit_event << std::endl;
    std::cout << " pdo: " << hit_pdo << std::endl;
    std::cout << " tdo: " << hit_tdo << std::endl;
    */
    
    DetectorComponent component = gp->GetComponent(hit_board, hit_vmm, hit_channel);
    int layer = component.GetLayer();

    if(!component.isPad() && !component.isStrip()){
      //std::cout<<"Skipping wire hit for now..."<<std::endl;
      continue;
    }

    if((hit_board==6 || hit_board==7) && layer!=1){
      std::cout<<"ISSUE WITH LAYER 1"<<std::endl;
      std::cout<<"hit_board: "<<hit_board<<std::endl;
      std::cout<<"layer: "<<layer<<std::endl;
      component.Dump();
    }

    /*
    if(hit_board==0 && layer!=1){
      std::cout<<"ISSUE WITH LAYER 1"<<std::endl;
      std::cout<<"hit_board: "<<hit_board<<std::endl;
      std::cout<<"layer: "<<layer<<std::endl;
      component.Dump();
    }
    if(hit_board==1 && layer!=2){
      std::cout<<"ISSUE WITH LAYER 2"<<std::endl;
      std::cout<<"hit_board: "<<hit_board<<std::endl;
      std::cout<<"layer: "<<layer<<std::endl;
      component.Dump();
    }
    if(hit_board==2 && layer!=3){
      std::cout<<"ISSUE WITH LAYER 3"<<std::endl;
      std::cout<<"hit_board: "<<hit_board<<std::endl;
      std::cout<<"layer: "<<layer<<std::endl;
      component.Dump();
    }
    if(hit_board==3 && layer!=4){
      std::cout<<"ISSUE WITH LAYER 4"<<std::endl;
      std::cout<<"hit_board: "<<hit_board<<std::endl;
      std::cout<<"layer: "<<layer<<std::endl;
      component.Dump();
    }

    */
    if (hit_board == -1 || hit_vmm == -1 || hit_channel == -1 || hit_pdo == -1){
      std::cout<<"ISSUE: HIT NOT FILLED?"<<std::endl;
      continue;
    }

    /**********
    int diff_bcid = hit_bcid - trig_bcid;

    // Make sure that the difference is the actual time difference, i.e. that it is positive
    if(diff_bcid < 0)
      diff_bcid += 4096;

    // Window of allowed bcid values from the maximum value; check that the bcid of this hit is in the correct window close to the trigger bcid
    if( abs( diff_bcid - offsets_bcid[hit_board][hit_vmm] )  > m_bcid_window)
      continue;

    // Dont include the scintillator trigger hit as an actual hit
    if(hit_board == GetTrigSignalBoard() && hit_vmm == GetTrigSignalVMM() && hit_channel == GetTrigSignalChan())
      continue;


    **********/

    // With new 2.3a boards, the "zero" for the trigger is at 3...
    if( abs(hit_bcid_rel - in_time_peak[hit_board][hit_vmm][hit_channel]) > m_bcid_window){
      //std::cout<<"HIT OUT OF "<<m_bcid_window<<" BCID WINDOW!!"<<std::endl;
      continue;      
    }
    
    //Hit h(component, hit_pdo, hit_tdo, hit_bcid, trig_bcid, trig_tdo);
    Hit h(component, hit_pdo, hit_tdo, hit_bcid, hit_bcid_rel);

    // Set calibrated TDO value
    double tdo_calib = 25*((hit_bcid_rel - in_time_peak[hit_board][hit_vmm][hit_channel]) - ((hit_tdo - TDO_calib_mean[hit_board][hit_vmm][hit_channel])/(TDO_calib_range[hit_board][hit_vmm][hit_channel])));
    h.SetCalibratedTDO(tdo_calib);

    /*
    std::cout<<"\nHit:"<<std::endl;
    std::cout<<"hit_bcid_rel: "<<hit_bcid_rel<<std::endl;
    std::cout<<"hit_tdo: "<<hit_tdo<<std::endl;
    std::cout<<"TDO_calib_mean[hit_board][hit_vmm][hit_channel]: "<<TDO_calib_mean[hit_board][hit_vmm][hit_channel]<<std::endl;
    std::cout<<"TDO_calib_range[hit_board][hit_vmm][hit_channel]: "<<TDO_calib_range[hit_board][hit_vmm][hit_channel]<<std::endl;
    std::cout<<"Calibrated TDO: "<<h.GetCalibratedTDO()<<std::endl;
    */
    /////// TRY ///////
    //if(h.isStripHit() && h.GetAdc()<200)
    //continue;
    /////// TRY ///////

    
    // Cleanup hits using bcid information to make sure we only select relevant hits
    //if(h.isGood())
    hm.AddHit(h);
      
    // std::cout<<"HM1 has "<<hm.GetHitsInStripLayer(1).size()<<" strip hits."<<std::endl;
    // std::cout<<"HM2 has "<<hm.GetHitsInStripLayer(2).size()<<" strip hits."<<std::endl;
    // std::cout<<"HM3 has "<<hm.GetHitsInStripLayer(3).size()<<" strip hits."<<std::endl;
    // std::cout<<"HM4 has "<<hm.GetHitsInStripLayer(4).size()<<" strip hits."<<std::endl;

    // std::cout<<"HM1 has "<<hm.GetHitsInPadLayer(1).size()<<" pad hits."<<std::endl;
    // std::cout<<"HM2 has "<<hm.GetHitsInPadLayer(2).size()<<" pad hits."<<std::endl;
    // std::cout<<"HM3 has "<<hm.GetHitsInPadLayer(3).size()<<" pad hits."<<std::endl;
    // std::cout<<"HM4 has "<<hm.GetHitsInPadLayer(4).size()<<" pad hits."<<std::endl;


  }

  // if(hm.GetHitsInStripLayer(1).size() < 1){
  //   std::cout<<"Issue?"<<std::endl;
  // }
  
  return true;
}

void RecoManager::PreEventProcessing() {

  //// emptying branches
  hits_processed_strips_l1.clear();
  hits_processed_strips_l2.clear();
  hits_processed_strips_l3.clear();
  hits_processed_strips_l4.clear();
  hits_processed_pads_l1.clear();
  hits_processed_pads_l2.clear();
  hits_processed_pads_l3.clear();
  hits_processed_pads_l4.clear();
  clusters_processed_strips_l1.clear();
  clusters_processed_strips_l2.clear();
  clusters_processed_strips_l3.clear();
  clusters_processed_strips_l4.clear();
  tracks_processed.clear();

  //////////////////////

  f_out->cd();
  for (int i_tool = 0; i_tool < (int)reco_tools.size(); ++i_tool)
    {
      reco_tools[i_tool]->PreEventProcessing();
    }
}

void RecoManager::PostEventProcessing() {
  f_out->cd();
  for (int i_tool = 0; i_tool < (int)reco_tools.size(); ++i_tool)
    {
      reco_tools[i_tool]->PostEventProcessing();
    }
  tOut->Fill();
  //tOutProcess->Fill();   ////// fill processed tree
  f_events->cd();
}

