/*
 * RecoManager.h
 *
 *  Created on: Sep 25, 2017
 *      Author: ekajomov and Luca M
 */

#ifndef RECOMANAGER_H_
#define RECOMANAGER_H_

#include "GeometryProvider.h"
#include "Readout.h"
#include "HitManager.h"
#include "ClusterBuilder.h"
#include "IRecoTool.h"

#include <string>
#include <fstream>
#include <map>

#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"

#include "TrackCandidateSelector.h"
#include "TrackBuilder.h"


class RecoManager {
 public:
  RecoManager();
  virtual ~RecoManager();
  void SetGeometryProvider(GeometryProvider* in_gp);
  GeometryProvider* GetGeometryProvider();
  ClusterBuilder* GetClusterBuilder();
  HitManager GetHitManager();
  void SetEventsFname(std::string events_fname);
  void SetOutputFname(std::string output_fname);
  void SetOutputProcessedFname(std::string output_processed_fname);
  void SetOutputLogFname(std::string output_log_fname);
  void SetSTgcClusterBuilder( ClusterBuilder* cb );
  void SetTrackCandidateSelector(TrackCandidateSelector* tcs);
  void SetTrackBuilder(TrackBuilder* tbuilder);
  void SetTrigSignal(int in_trig_elink, int in_trig_vmm, int in_trig_chan);
  void SetBCIDWindow(int in_bcid_window);
	
  bool InitEventDisplay();
  bool ProcessEvent(int evtCtr);
  bool Init();
  void Loop();//std::vector<std::vector<float>> in_parameters);
  void Finalize();
  bool FillHits();
  bool GetOffsets();
  bool GetTDOCalibrations();
  int GetTrigSignalBoard() const {
    return m_trig_elink;
  }
	
  int GetTrigSignalVMM() const {
    return m_trig_vmm;
  }

  int GetTrigSignalChan() const {
    return m_trig_chan;
  }
	
  /* int GetCurrentEventNumber() const { */
  /*   return readout.m_evid; */
  /* } */
	
  long GetMaxEvents() const {
    return maxEvents;
  }
	
  void SetMaxEvents(long maxEvents) {
    this->maxEvents = maxEvents;
  }
	
  bool GetDoFast() const {
    return doFast;
  }
	
  int GetBCIDWindow() const {
    return m_bcid_window;
  }

  void SetDoFast(bool doFast) {
    this->doFast = doFast;
  }
  void BookHistograms(GeometryProvider* gp);
  void FillHistograms();
  void WriteHistograms();

 protected:
  void PreEventProcessing();
  void PostEventProcessing();

  long maxEvents;
  bool doFast;

  GeometryProvider* gp;
  ClusterBuilder* stgc_cb;
  TrackCandidateSelector* tcs;
  TrackBuilder* tbuilder;

  std::vector<IRecoTool*> reco_tools;

  std::string events_fname;
  std::string output_fname;
  std::string output_log_fname;
  std::string output_processed_fname;

  HitManager hm;

  TTree*  tReadout;   //!pointer to the analyzed TTree or TChain
  Readout readout;
  TFile* f_events;
  TFile* f_out;
  //TFile* f_out_processed;
  //std::ofstream f_out_log;
  TTree* tOut;
  //TTree* tOutProcess;
  // BCID offsets for each elink (i.e. board) and VMM
  std::map<int, std::map<int, int>> offsets_bcid;

  // TDO calibrations for each elink/VMM/Channel
  std::map<int, std::map<int, std::map<int, double>>> TDO_calib_mean;
  std::map<int, std::map<int, std::map<int, double>>> TDO_calib_range;

  // TDO in-time peak (different between e.g. 25ns and 50ns peaking time)
  std::map<int, std::map<int, std::map<int, int>>> in_time_peak;

  // Map converting capture to VMM
  std::map<int, int> capture2VMM = {
    {2, 0},    
    {3, 1},
    {0, 2},
    {1, 3},
    {5, 4},
    {4, 5},
    {6, 6},
    {7, 7}
  };

  // Scintillator Trigger Signal
  int m_trig_elink;
  int m_trig_vmm;
  int m_trig_chan;
    
  //S.I add this as a member so that it can be picked up from xml easily
  int m_bcid_window;

  //////////// processed tree branches //////////////
  std::vector<Hit> hits_processed_strips_l1;
  std::vector<Hit> hits_processed_strips_l2;
  std::vector<Hit> hits_processed_strips_l3;
  std::vector<Hit> hits_processed_strips_l4;
  std::vector<Hit> hits_processed_pads_l1;
  std::vector<Hit> hits_processed_pads_l2;
  std::vector<Hit> hits_processed_pads_l3;
  std::vector<Hit> hits_processed_pads_l4;
  std::vector<Cluster> clusters_processed_strips_l1;
  std::vector<Cluster> clusters_processed_strips_l2;
  std::vector<Cluster> clusters_processed_strips_l3;
  std::vector<Cluster> clusters_processed_strips_l4;
  std::vector<Track> tracks_processed;

  // Maps for output histograms
  std::map<std::string, TH1F*> h1;
  std::map<std::string, TH2F*> h2;
	
};

#endif /* RECOMANAGER_H_ */
