#include "TestBeamConfigHandler.h"
#include <iostream>
TestBeamConfigHandler::TestBeamConfigHandler(std::string s):XMLHandler(s){
    std::cout<<"*** TestBeam Main Parameters ***"<<std::endl;
    
}


void  TestBeamConfigHandler::ElementHandle(){
    
    bool ret=true;
    m_attributes["DOFAST"]=getAttributeAsInt("DOFAST",ret);
    m_attributes["DOGAUSCLUSTERING"]=getAttributeAsInt("DOGAUSCLUSTERING",ret);
    m_attributes["MAXEVENTS"]=getAttributeAsInt("MAXEVENTS",ret);
    m_attributes["NVMM"]=getAttributeAsInt("NVMM",ret);
    m_attributes["NELINKS"]=getAttributeAsInt("NELINKS",ret);
    m_attributes["TRGELINK"]=getAttributeAsInt("TRGELINK",ret);
    m_attributes["TRGVMM"]=getAttributeAsInt("TRGVMM",ret);
    m_attributes["TRGCHAN"]=getAttributeAsInt("TRGCHAN",ret);
    m_attributes["MAXTRGPDO"]=getAttributeAsInt("MAXTRGPDO",ret);
    m_attributes["NCHANSVMM"]=getAttributeAsInt("NCHANSVMM",ret);
    m_attributes["BCIDWINDOW"]=getAttributeAsInt("BCIDWINDOW",ret);
    m_attributes_string["QUADTYPE"]=getAttributeAsString("QUADTYPE",ret);
    m_attributes_double["PADZL1"]=getAttributeAsDouble("PADZL1",ret);
    m_attributes_double["PADZL2"]=getAttributeAsDouble("PADZL2",ret);
    m_attributes_double["PADZL3"]=getAttributeAsDouble("PADZL3",ret);
    m_attributes_double["PADZL4"]=getAttributeAsDouble("PADZL4",ret);
    m_attributes_double["STRIPZL1"]=getAttributeAsDouble("STRIPZL1",ret);
    m_attributes_double["STRIPZL2"]=getAttributeAsDouble("STRIPZL2",ret);
    m_attributes_double["STRIPZL3"]=getAttributeAsDouble("STRIPZL3",ret);
    m_attributes_double["STRIPZL4"]=getAttributeAsDouble("STRIPZL4",ret);
    
    for(const auto& p : m_attributes )
      std::cout<<"Found attribute "<<p.first<<": "<<p.second<<std::endl;        
    for(const auto& p : m_attributes_string )
      std::cout<<"Found attribute "<<p.first<<": "<<p.second<<std::endl;
    for(const auto& p : m_attributes_double )
      std::cout<<"Found attribute "<<p.first<<": "<<p.second<<std::endl;
        
    
    
}

