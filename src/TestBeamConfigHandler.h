#ifndef TESTBEAMCONFIGHANDLER_H
#define TESTBEAMCONFIGHANDLER_H
#include "XMLHandler.h"
#include <unordered_map>
class TestBeamConfigHandler:public XMLHandler {
 public:
	TestBeamConfigHandler(std::string);
	void ElementHandle();
    const int& par(std::string key){ return m_attributes[key];}
    const std::string& parString(std::string key){ return m_attributes_string[key];}
    const double& parDouble(std::string key){ return m_attributes_double[key];}
    
private:
    std::unordered_map<std::string,int> m_attributes;
    std::unordered_map<std::string,std::string> m_attributes_string;
    std::unordered_map<std::string,double> m_attributes_double;
};

#endif
