//============================================================================
// Name        : TestBeam_Tracker.cpp
// Author      : Enrique K and Luca M
// Version     : 0.1 Luca 170914 17:10
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "AsciiGeometryProviderBuilder.h"
#include "QuadrupletGeometryProviderBuilder.h"
#include "GeometryProvider.h"
#include "TrackCandidateSelector.h"
#include "TrackBuilder.h"
#include "RecoManager.h"

#include "TestBeam_Tracker.h"

using namespace std;

int TestBeam_Tracker(TestBeamConfigHandler* tbch, std::string filename) {

  // Load configuration
  cout << "---- Load configuration" << endl;

  //std::string geometry_fname = "geometry.txt";
  //AsciiGeometryProviderBuilder agpb;
  QuadrupletGeometryProviderBuilder qgpb;
  qgpb.SetStripZs(tbch->parDouble("STRIPZL1"),tbch->parDouble("STRIPZL2"),tbch->parDouble("STRIPZL3"),tbch->parDouble("STRIPZL4"));
  qgpb.SetPadZs(tbch->parDouble("PADZL1"),tbch->parDouble("PADZL2"),tbch->parDouble("PADZL3"),tbch->parDouble("PADZL4"));
  qgpb.SetQuadType(tbch->parString("QUADTYPE"));
  GeometryProvider* gp = qgpb.Build();

  ClusterBuilder stgc_cb;
  stgc_cb.SetHitNeighborWindow(1);
  stgc_cb.SetNeighborDistance(3.2);
  stgc_cb.SetMinHits(2);
  stgc_cb.SetMaxHoles(0);
  //stgc_cb.SetDoSWaveCorrection(false);
  stgc_cb.SetDoGaussianClustering(tbch->par("DOGAUSCLUSTERING"));
  stgc_cb.SetGeometryProvider(gp);

  TrackCandidateSelector tcs;
  float minIntercept = -500;
  float maxIntercept = 500;
  int interceptBins = 500; // CAREFUL: too fine binning slows down a lot the analysis
  float minSlope = -10;
  float maxSlope = 10;
  int slopeBins = 100; // CAREFUL: too fine binning slows down a lot the analysis
  float deltaY = 10;
  tcs.SetHoughTransformParameters(minIntercept,maxIntercept, interceptBins,
				  minSlope, maxSlope, slopeBins);
  tcs.SetDeltaY(deltaY);

  TrackBuilder tb;
  
  std::string events_fname = filename;
  std::string rootfile=events_fname;
  //extract the name only part with no extension and path
  events_fname.erase(events_fname.find_last_of("."), std::string::npos);
  events_fname.erase(0,events_fname.find_last_of("/")+1);
  
  std::string output_fname ="results/out_"+events_fname+".root";
  std::string output_log_fname ="results/"+events_fname+"_log.txt";
  std::string output_processed_fname = "results/"+events_fname+"_processed.root";

  cout << "---- Setting up the reconstruction" << endl;
  RecoManager reco;
  reco.SetGeometryProvider(gp);
  reco.SetTrackBuilder(&tb);
  reco.SetSTgcClusterBuilder(&stgc_cb);
  reco.SetTrackCandidateSelector(&tcs);
  reco.SetEventsFname(rootfile);
  reco.SetOutputFname(output_fname);
  reco.SetOutputLogFname(output_log_fname);
  reco.SetOutputProcessedFname(output_processed_fname);
  reco.SetTrigSignal(tbch->par("TRGELINK"), tbch->par("TRGVMM"), tbch->par("TRGCHAN"));
  reco.SetMaxEvents(tbch->par("MAXEVENTS"));
  reco.SetBCIDWindow(tbch->par("BCIDWINDOW"));
  reco.SetDoFast(tbch->par("DOFAST"));
  if (!reco.Init())
    {
      std::cout << "--- Problems when initializing the RecoManager. Exit" << std::endl;
      return -1;
    }
  cout << "---- Reconstructing events" << endl;
  reco.Loop();
  cout << "---- Finishing the reconstruction" << endl;
  reco.Finalize();
  delete gp;
  return 0;
}
