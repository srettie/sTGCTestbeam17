#ifndef TESTBEAM_TRACKER_H
#define TESTBEAM_TRACKER_H
#include <iostream>

#include "TestBeamConfigHandler.h"

int TestBeam_Tracker(TestBeamConfigHandler* tbch, std::string filename="outDecoded");

#endif
