/*
 * Track.h
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov and Luca M
 *
 *	Modified: Sep 13,2017 - Luca
 *
 */


#include "Cluster.h"

#ifndef TRACK_H_
#define TRACK_H_

class Track {
public:
	Track();

	virtual ~Track();


	float GetA() const {
		return a;
	}

	float GetB() const {
		return b;
	}

	float GetChi2() const {
		return chi2;
	}

	
	const std::vector<Cluster>& GetClusters() const {
		return clusters;
	}

	int GetNClusters() const {
		int nClusters = -1;
		nClusters = flags[0] + flags [1] + flags [2] + flags[3];
	  return nClusters;
	}
	
	void SetClusters(const std::vector<Cluster>& clusters) {
		this->clusters = clusters;
	}

	const std::vector<float>& GetGlobalResidualsY() const {
		return global_residuals_y;
	}

	const std::vector<float>& GetBiasedResidualsY() const {
		return biased_residuals_y;
	}

	float GetSigmaA() const {
		return sigma_a;
	}

	float GetSigmaB() const {
		return sigma_b;
	}

	void SetA(float a) {
		this->a = a;
	}

	void SetB(float b) {
		this->b = b;
	}

	void SetChi2(float chi2) {
		this->chi2 = chi2;
	}

	void SetFlags(std::vector<bool> in_flags) {
		this->flags = in_flags;
	}
	std::vector<bool> GetFlags() const {
		return flags;
	}


	void SetSigmaA(float sigmaA) {
		sigma_a = sigmaA;
	}

	void SetSigmaB(float sigmaB) {
		sigma_b = sigmaB;
	}

	void SetGlobalResidualsY(const std::vector<float>& globalResidualsY) {
		global_residuals_y = globalResidualsY;
	}

	void SetBiasedResidualsY(const std::vector<float>& biasedResidualsY) {
		biased_residuals_y = biasedResidualsY;
	}

	float getSelfResidual12() const {
		return self_residual12;
	}

	void setSelfResidual12(float selfResidual12) {
		self_residual12 = selfResidual12;
	}

	float getSelfResidual13() const {
		return self_residual13;
	}

	void setSelfResidual13(float selfResidual13) {
		self_residual13 = selfResidual13;
	}

	float getSelfResidual14() const {
		return self_residual14;
	}

	void setSelfResidual14(float selfResidual14) {
		self_residual14 = selfResidual14;
	}

	float getSelfResidual23() const {
		return self_residual23;
	}

	void setSelfResidual23(float selfResidual23) {
		self_residual23 = selfResidual23;
	}

	float getSelfResidual24() const {
		return self_residual24;
	}

	void setSelfResidual24(float selfResidual24) {
		self_residual24 = selfResidual24;
	}

	float getSelfResidual34() const {
		return self_residual34;
	}

	void setSelfResidual34(float selfResidual34) {
		self_residual34 = selfResidual34;
	}

protected:
	std::vector<Cluster> clusters;
	float a;
	float b;
	float sigma_a;
	float sigma_b;
	std::vector<float> global_residuals_y; // in mm
	std::vector<float> biased_residuals_y; // in mm
	float self_residual12;
	float self_residual13;
	float self_residual14;
	float self_residual23;
	float self_residual24;
	float self_residual34;
	float chi2;
	std::vector<bool> flags;
};

#endif /* TRACK_H_ */
