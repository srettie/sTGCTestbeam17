/*
 * TrackBuilder.cpp
 *
 *  Created on: Sep 28, 2017
 *      Author: ekajomov and Luca M
 */

#include "TrackBuilder.h"
#include "Track.h"
#include "TF1.h"
#include "TGraph.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TCanvas.h"
#include <math.h>

TrackBuilder::TrackBuilder() {
	// TODO Auto-generated constructor stub

}

TrackBuilder::~TrackBuilder() {
	// TODO Auto-generated destructor stub
}
void TrackBuilder::ResetTrackVariables(){
  track_A = -999;
  track_B = -999;

  track_clY_l1 = -999;
  track_clY_l2 = -999;
  track_clY_l3 = -999;
  track_clY_l4 = -999;

  track_res_biased_l1 = -999;
  track_res_biased_l2 = -999;
  track_res_biased_l3 = -999;
  track_res_biased_l4 = -999;

  track_res_global_l1 = -999;
  track_res_global_l2 = -999;
  track_res_global_l3 = -999;
  track_res_global_l4 = -999;

  track_res_self12 = -999;
  track_res_self13 = -999;
  track_res_self14 = -999;
  track_res_self23 = -999;
  track_res_self24 = -999;
  track_res_self34 = -999;

}
bool TrackBuilder::Init(){
  ResetTrackVariables();

  tree->Branch("track_A", &track_A);
  tree->Branch("track_B", &track_B);

  tree->Branch("track_clY_l1", &track_clY_l1);
  tree->Branch("track_clY_l2", &track_clY_l2);
  tree->Branch("track_clY_l3", &track_clY_l3);
  tree->Branch("track_clY_l4", &track_clY_l4);

  tree->Branch("track_res_biased_l1", &track_res_biased_l1);
  tree->Branch("track_res_biased_l2", &track_res_biased_l2);
  tree->Branch("track_res_biased_l3", &track_res_biased_l3);
  tree->Branch("track_res_biased_l4", &track_res_biased_l4);

  tree->Branch("track_res_global_l1", &track_res_global_l1);
  tree->Branch("track_res_global_l2", &track_res_global_l2);
  tree->Branch("track_res_global_l3", &track_res_global_l3);
  tree->Branch("track_res_global_l4", &track_res_global_l4);

  tree->Branch("track_res_self12", &track_res_self12);
  tree->Branch("track_res_self13", &track_res_self13);
  tree->Branch("track_res_self14", &track_res_self14);
  tree->Branch("track_res_self23", &track_res_self23);
  tree->Branch("track_res_self24", &track_res_self24);
  tree->Branch("track_res_self34", &track_res_self34);

  return true;
}
bool TrackBuilder::Finalize(){
	return true;
}
void TrackBuilder::PreEventProcessing(){
  ResetTrackVariables();
  return;
}
void TrackBuilder::PostEventProcessing(){
	return;
}
std::vector<Track> TrackBuilder::FitTracks(std::vector<TrackCandidate> candidates, bool verbose)
{
  std::vector<Track> tracks;
  int n_candidates = candidates.size();
  for(int i_trk_candidate = 0 ; i_trk_candidate < n_candidates; i_trk_candidate++){
    if(candidates[i_trk_candidate].GetNClusters() > 2){
      Track trk = FitTrack(candidates[i_trk_candidate], verbose);
      tracks.push_back(trk);
    }
  }
  return tracks;
}

/// not used at the moment
void TrackBuilder::SetTree(TTree* in_tree){
	tree = in_tree;
}

/////////////////

Track TrackBuilder::FitTrack(TrackCandidate in_TrackCandidate, bool verbose)
{
  //define track and fill the clusters
  Track track;
  track.SetClusters(in_TrackCandidate.GetClusters());

  //importing clusters from the candidate (it is a 4-vector)
  std::vector<Cluster> cl_vec = in_TrackCandidate.GetClusters();
  std::vector<bool> flags = in_TrackCandidate.GetFlags();
  int n_cl = in_TrackCandidate.GetNClusters();
  std::vector <float> cl_vec_y(4);
  std::vector <float> cl_vec_z(4);
  std::vector <float> cl_vec_yRes(4);
  std::vector <float> cl_vec_yRes_global(4);

  std::vector<float> cl_point_y;
  std::vector<float> cl_point_z;

  if(verbose) std::cout << "number of clusters in track: " << n_cl <<std::endl;
  if(verbose) in_TrackCandidate.PrintFlags();

  for(int i_cl = 0; i_cl < 4; i_cl++){
    if(flags[i_cl] == 1){
      double ypos = cl_vec[i_cl].GetY();
      double zpos = cl_vec[i_cl].GetZ();
      // Correct for relative strip shift between layers here...
      //if((i_cl == 1) || (i_cl == 3))
      //ypos += 0.5;
      cl_vec_y[i_cl] = ypos;//GetYaligned
      cl_vec_z[i_cl] = zpos;
      cl_point_y.push_back(ypos);//GetYaligned
      cl_point_z.push_back(zpos);
    }
    else{
      cl_vec_y[i_cl] = -999;
      cl_vec_z[i_cl]= -999;
    }
  }
  
  if(verbose) std::cout << "number of points in graph: " << cl_point_y.size() <<std::endl;

  //Fit the clusters with "pol1" function
  float fitmin = 0;
  float fitmax = (cl_point_z[3]>cl_point_z[2]) ? cl_point_z[3] : cl_point_z[2];

  TF1 f1("f1","pol1",fitmin,fitmax);
  // https://root.cern.ch/root/roottalk/roottalk02/0077.html
  TGraph gr(n_cl, &cl_point_z[0], &cl_point_y[0]);
  if(verbose) gr.Print();
  TFitResultPtr fit_results = gr.Fit("f1", "RSQ", "0");
  Int_t fitStatus1 = fit_results->IsEmpty();
  Int_t fitStatus2 = fit_results->IsValid();
  
  if(fitStatus1 == 0 && fitStatus2 ==1){	  
    //Fill the fit resuts y = az + b (a=par[1], b=par[0])
    track.SetChi2(fit_results->Chi2());
    track.SetA(fit_results->Value(1));
    track.SetB(fit_results->Value(0));
    track.SetSigmaA(fit_results->ParError(1));
    track.SetSigmaB(fit_results->ParError(0));
    if(verbose) std::cout<< "track parameters: a = " << track.GetA() << ", b = " << track.GetB() << ", Chi2 = " << track.GetChi2() << std::endl;

    //Calculate residuals
    for(int i_eval = 0; i_eval < 4; i_eval++){
      // Biased residuals
      if(flags[i_eval] == 1){
	cl_vec_yRes[i_eval] = (cl_vec_y[i_eval] - f1.Eval(cl_vec_z[i_eval]));
	if(verbose) std::cout<< "biased residual layer " << i_eval + 1 << ": " << cl_vec_yRes[i_eval] << std::endl;
	if(verbose) std::cout<< "cl_y = " << cl_vec_y[i_eval] << ", trk_y = " << f1.Eval(cl_vec_z[i_eval]) << std::endl;
	
	// Global (3/4) residuals
	TF1 fGlob("fGlob","pol1",fitmin,fitmax);
	std::vector<float> cl_point_y_glob;
	std::vector<float> cl_point_z_glob;
	for(int i_cl = 0; i_cl < 4; i_cl++){
	  if(i_cl == i_eval && flags[i_cl] != 1)
	    continue;
	  double ypos = cl_vec[i_cl].GetY();
	  double zpos = cl_vec[i_cl].GetZ();
	  // Correct for relative strip shift between layers here...
	  //if((i_cl == 1) || (i_cl == 3))
	  //ypos += 0.5;
	  cl_point_y_glob.push_back(ypos);//aligned
	  cl_point_z_glob.push_back(zpos);
	}
	// https://root.cern.ch/root/roottalk/roottalk02/0077.html
	TGraph grGlob(n_cl-1, &cl_point_z_glob[0], &cl_point_y_glob[0]);
	TFitResultPtr fit_results_glob = grGlob.Fit("fGlob", "SQ", "0");
	Int_t fitStatus_glob = fit_results_glob->Status();
	if(fitStatus_glob == 0){
	  cl_vec_yRes_global[i_eval] = (cl_vec_y[i_eval] - fGlob.Eval(cl_vec_z[i_eval]));
	  if(verbose) std::cout<< "global residual layer " << i_eval + 1 << ": " << cl_vec_yRes_global[i_eval] << std::endl;
	}
	else{
	  cl_vec_yRes_global[i_eval] = -999;
	}
      }
      else{
	cl_vec_yRes[i_eval] = -999;
	cl_vec_yRes_global[i_eval] = -999;
      }
    }
    
    //fill the residuals
    track.setSelfResidual12((cl_vec_y[0] - cl_vec_y[1])/sqrt(2));
    track.setSelfResidual13((cl_vec_y[0] - cl_vec_y[2])/sqrt(2));
    track.setSelfResidual14((cl_vec_y[0] - cl_vec_y[3])/sqrt(2));
    track.setSelfResidual23((cl_vec_y[1] - cl_vec_y[2])/sqrt(2));
    track.setSelfResidual24((cl_vec_y[1] - cl_vec_y[3])/sqrt(2));
    track.setSelfResidual34((cl_vec_y[2] - cl_vec_y[3])/sqrt(2));
    track.SetBiasedResidualsY(cl_vec_yRes);		
    track.SetGlobalResidualsY(cl_vec_yRes_global);
    track.SetFlags(flags);


    track_A = track.GetA();
    track_B = track.GetB();

    track_clY_l1 = cl_vec_y[0];
    track_clY_l2 = cl_vec_y[1];
    track_clY_l3 = cl_vec_y[2];
    track_clY_l4 = cl_vec_y[3];
    
    track_res_biased_l1 = track.GetBiasedResidualsY()[0];
    track_res_biased_l2 = track.GetBiasedResidualsY()[1];
    track_res_biased_l3 = track.GetBiasedResidualsY()[2];
    track_res_biased_l4 = track.GetBiasedResidualsY()[3];
    
    track_res_global_l1 = track.GetGlobalResidualsY()[0];
    track_res_global_l2 = track.GetGlobalResidualsY()[1];
    track_res_global_l3 = track.GetGlobalResidualsY()[2];
    track_res_global_l4 = track.GetGlobalResidualsY()[3];

    track_res_self12 = track.getSelfResidual12();
    track_res_self13 = track.getSelfResidual13();
    track_res_self14 = track.getSelfResidual14();
    track_res_self23 = track.getSelfResidual23();
    track_res_self24 = track.getSelfResidual24();
    track_res_self34 = track.getSelfResidual34();
    


    if(verbose) std::cout << "----------------------------" <<std::endl;
    return track;
  }
  else{
    std::cout << "ERROR: Empty track fit result!!" << std::endl;
    return track;
  }
}
