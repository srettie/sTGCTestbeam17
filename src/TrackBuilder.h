/*
 * TrackBuilder.h
 *
 *  Created on: Sep 28, 2017
 *      Author: ekajomov and Luca M
 */

#ifndef SRC_TRACKBUILDER_H_
#define SRC_TRACKBUILDER_H_

#include "IRecoTool.h"
#include "TrackCandidate.h"
#include "Track.h"

#include "TTree.h"
#include <vector>


class TrackBuilder : public IRecoTool{
public:

	TrackBuilder();
	virtual ~TrackBuilder();
	void SetTree(TTree* tree);
	bool Init();
	void ResetTrackVariables();
	bool Finalize();
	void PreEventProcessing();
	void PostEventProcessing();
	std::vector<Track> FitTracks(std::vector<TrackCandidate> candidates, bool verbose = 0);
	Track FitTrack(TrackCandidate, bool verbose = 0);

protected:
	TTree* tree;

	float track_A;
	float track_B;

	float track_clY_l1;
	float track_clY_l2;
	float track_clY_l3;
	float track_clY_l4;

	float track_res_biased_l1;
	float track_res_biased_l2;
	float track_res_biased_l3;
	float track_res_biased_l4;

	float track_res_global_l1;
	float track_res_global_l2;
	float track_res_global_l3;
	float track_res_global_l4;

	float track_res_self12;
	float track_res_self13;
	float track_res_self14;
	float track_res_self23;
	float track_res_self24;
	float track_res_self34;

};

#endif /* SRC_TRACKBUILDER_H_ */
