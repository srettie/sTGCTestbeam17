/*
 * Candidate.cpp
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov and Luca M
 */

#include "TrackCandidate.h"

TrackCandidate::TrackCandidate():
a(-999), b(-999){

}

TrackCandidate::TrackCandidate(std::vector<Cluster> in_clusters, float in_a, float in_b, std::vector<bool> in_flags):
	clusters(in_clusters),
	a(in_a),
	b(in_b),
	flags(in_flags)
{}

TrackCandidate::TrackCandidate(const TrackCandidate &candidate)
{

	clusters = candidate.clusters;
	a = candidate.a; // slope with respect to z
	b = candidate.b; // intercept with respect to y with origin at (0,0,0)
	flags = candidate.flags;
}

TrackCandidate::~TrackCandidate() {
}

