/*
 * Candidate.h
 *
 *  Created on: Sep 12, 2017
 *      Author: ekajomov and Luca M
 */

#include <vector>
#include "Cluster.h"

#ifndef TRACKCANDIDATE_H_
#define TRACKCANDIDATE_H_


class TrackCandidate {
public:
	TrackCandidate();
	virtual ~TrackCandidate();

	TrackCandidate(std::vector<Cluster> in_clusters, float in_a, float in_b, std::vector<bool> in_flags);
	TrackCandidate(const TrackCandidate &candidate);

	float GetA() const {
		return a;
	}

	void SetA(float a) {
		this->a = a;
	}

	void SetFlags(std::vector<bool> in_flags) {
			this->flags = in_flags;
		}
	std::vector<bool> GetFlags() const {
			return flags;
			}

	int GetNClusters() const {
			int nClusters = -1;
			nClusters = flags[0] + flags [1] + flags [2] + flags[3];
		  return nClusters;
	}
	void PrintFlags(){
		std::cout << "flags: " << flags[0] << ", " << flags[1] << ", " << flags[2] << ", " << flags[3] << std::endl;
	}

	float GetB() const {
		return b;
	}

	void SetB(float b) {
		this->b = b;
	}

	const std::vector<Cluster>& GetClusters() const {
		return clusters;
	}

	void SetClusters(const std::vector<Cluster>& clusters) {
		this->clusters = clusters;
	}

protected:
	std::vector<Cluster> clusters;
	float a; // slope with respect to z
	float b; // intercept with respect to y with origin at (0,0,0)
	// Flags are true if layer is assigned a cluster, and false otherwise
	std::vector<bool> flags;

};

#endif /* TRACKCANDIDATE_H_ */
