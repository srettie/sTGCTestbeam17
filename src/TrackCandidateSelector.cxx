/*
 * TrackCandidateSelector.cpp
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov and Luca M
 */

#include "TrackCandidateSelector.h"
#include "TCanvas.h"
#include <cmath>

TrackCandidateSelector::TrackCandidateSelector():
  h_accumulator(0), tree(0), deltaY()
{
  // TODO Auto-generated constructor stub

}

TrackCandidateSelector::~TrackCandidateSelector() {
  // TODO Auto-generated destructor stub
}


bool TrackCandidateSelector::Init() {
  h_accumulator = new TH2D("accumulatorY", "accumulatorY",
			   n_slopeBins, minSlope, maxSlope,
			   n_interceptBins, minIntercept, maxIntercept);
  return true;
}


TH2D* TrackCandidateSelector::GetAccumulator(){

  return h_accumulator;
}

bool TrackCandidateSelector::Finalize() {
  return true;
}

void TrackCandidateSelector::SetHoughTransformParameters(float in_minIntercept,
							 float in_maxIntercept, int interceptBins, float in_minSlope, float in_maxSlope,
							 int slopeBins) {
  n_slopeBins = slopeBins;
  n_interceptBins = interceptBins;
  minIntercept = in_minIntercept;
  maxIntercept = in_maxIntercept;
  minSlope = in_minSlope;
  maxSlope = in_maxSlope;
}

void TrackCandidateSelector::SetDeltaY(float in_deltaY)
{
  deltaY = in_deltaY;
}
std::vector<TrackCandidate> TrackCandidateSelector::SelectTrackCandidates(std::vector<Cluster> clusters, bool verbose) {

  //TODO Re-insert discarded clusters and loop again to look for second track.

  std::vector<TrackCandidate> tcs;
  h_accumulator->Reset();
  // Initial call to fill accumulator
  FillAccumulator(clusters);
  // pseudo code
  int binMax = h_accumulator->GetMaximumBin();
  int maxValue = h_accumulator->GetMaximum();

  if(verbose)
    std::cout << "accumulator maximum: " << maxValue << std::endl;
  //h_accumulator->Write();

  std::vector<bool> flag_layer {0, 0, 0, 0};
  bool flag_multiple = 0;

  while (maxValue > 2) // we have at least three hits in this track candidate
    {
      int binX, binY, binZ;
      h_accumulator->GetBinXYZ(binMax, binX, binY, binZ);
      // Get best guesses from previous FillAccumulator call
      float found_intercept  = h_accumulator->GetYaxis()->GetBinCenter(binY);
      float found_slope  = h_accumulator->GetXaxis()->GetBinCenter(binX);

      if(verbose)	  std::cout << "This is the maximum we found (intercept, slope) " << found_intercept << ", " << found_slope << " max: " << maxValue << std::endl;
      TrackCandidate candidate;
      candidate.SetA(found_slope);
      candidate.SetB(found_intercept);
      std::vector<Cluster> candidate_clusters(4);

      if(verbose) std::cout << "have " <<  clusters.size() << " clusters "<< std::endl;

      for (int i_cluster = clusters.size() - 1; i_cluster >= 0; i_cluster--)
	{
	  float y_hat = clusters[i_cluster].GetZ() * found_slope + found_intercept;
	  float diff = y_hat - clusters[i_cluster].GetYaligned();
	  if(verbose) std::cout << "for cluster " <<  i_cluster << " the difference is "<< diff << std::endl;
	  int cl_layer = clusters[i_cluster].GetLayer();
	  if(verbose) std::cout << "the layer of cluster " <<  i_cluster << " is "<< cl_layer << std::endl;
	  // Only use clusters that are within one strip pitch from the found slope/intercept line
	  if (std::abs(diff) > deltaY){
	    if(verbose) std::cout << "cluster " <<  i_cluster << " out of range. the difference is "<< diff << std::endl;
	    clusters.erase(clusters.begin() + i_cluster);
	    if(verbose) std::cout << "cluster " <<  i_cluster << " removed from available clusters " << std::endl;
	    continue;
	  }
	  // Assign cluster to track candidate
	  if(flag_layer[cl_layer-1] == 1){
	    flag_multiple = 1;
	    if(verbose) std::cout << "!!! multiple candidate clusters for track. skipped cluster. " << std::endl;

	    // among the multiple clusters in the layer, keep the cluster with highest charge.
	    if(clusters[i_cluster].GetTotalAdc() < candidate_clusters[cl_layer-1].GetTotalAdc()){
	      clusters.erase(clusters.begin() + i_cluster);
	      if(verbose) std::cout << "cluster " <<  i_cluster << " removed from available clusters " << std::endl;
	      continue;
	    }
	  }
	  candidate_clusters[cl_layer-1] = clusters[i_cluster];
	  flag_layer[cl_layer-1] = 1;
	  if(verbose) std::cout << "cluster assigned" << std::endl;
	  // Remove assigned cluster from available clusters
	  clusters.erase(clusters.begin() + i_cluster);
	  if(verbose) std::cout << "cluster " <<  i_cluster << " removed from available clusters " << std::endl;
	}
      // Assign clusters to track candidate

      // Don't use tracks with too few clusters
      if(candidate_clusters.size() > 2){
	candidate.SetClusters(candidate_clusters);
	if(verbose) std::cout << "clusters assigned" <<std::endl;
	candidate.SetFlags(flag_layer);
	if(verbose) std::cout << "flags assigned" <<std::endl;
	if(verbose) candidate.PrintFlags();
	// Assign track candidate
	tcs.push_back(candidate);
	if(verbose) std::cout << "track candidate assigned" <<std::endl;
	// Prepare for potential next iteration
	h_accumulator->Reset();
	FillAccumulator(clusters);
	maxValue = h_accumulator->GetMaximum();
	binMax = h_accumulator->GetMaximumBin();
      }else std::cout<< "less than three clusters. candidate skipped." << std::endl;

    }

  if(verbose){
    std::cout << "The number of candidate tracks is: " << tcs.size() << std::endl;

    for (unsigned int i_tc = 0; i_tc < tcs.size(); i_tc++)
      {
	std::cout << "The candiate properties are intercept: " << tcs[i_tc].GetB() << " slope: " << tcs[i_tc].GetA() << " clusters: "  << tcs[i_tc].GetClusters().size() << std::endl;
	tcs[i_tc].PrintFlags();
	std::cout << "--------------------------------------------------------" << std::endl;
      }
  }
  return tcs;
}

void TrackCandidateSelector::FillAccumulator(std::vector<Cluster> clusters)
{
  float slopeIncrement = (maxSlope - minSlope)/n_slopeBins;
  for (unsigned int i_cluster = 0; i_cluster < clusters.size(); i_cluster++)
    {
      for (float slope = minSlope; slope <= maxSlope; slope += slopeIncrement)
	{
	  Cluster& cl = clusters[i_cluster];
	  float intercept = cl.GetYaligned() - slope * cl.GetZ();
	  h_accumulator->Fill(slope, intercept);
	}
    }
}


void TrackCandidateSelector::SetTree(TTree* in_tree) {
  tree = in_tree;
}

void TrackCandidateSelector::PreEventProcessing() {
}

void TrackCandidateSelector::PostEventProcessing() {
}
