/*
 * TrackCandidateSelector.h
 *
 *  Created on: Sep 26, 2017
 *      Author: ekajomov and Luca M
 */

#ifndef TRACKCANDIDATESELECTOR_H_
#define TRACKCANDIDATESELECTOR_H_

#include "TrackCandidate.h"
#include "Cluster.h"
#include "IRecoTool.h"

#include "TTree.h"
#include "TH2D.h"

#include <vector>

class TrackCandidateSelector : public IRecoTool {
 public:
  TrackCandidateSelector();
  virtual ~TrackCandidateSelector();
  void SetHoughTransformParameters(float minIntercept, float maxIntercept, int interceptBins,
				   float minSlope, float maxSlope, int slopeBins);
  void SetDeltaY(float in_deltaY);
  bool Init();
  bool Finalize();

  std::vector<TrackCandidate> SelectTrackCandidates(std::vector<Cluster> clusters, bool verbose = 0);
  void SetTree(TTree*);

  void PreEventProcessing();
  void PostEventProcessing();

  TH2D* GetAccumulator();

  void FillAccumulator(std::vector<Cluster> clusters);

 protected:
  TH2D* h_accumulator;

  int n_slopeBins;
  int n_interceptBins;
  float minIntercept;
  float minSlope;
  float maxIntercept;
  float maxSlope;
  float deltaY;

  TTree* tree;
};

#endif /* TRACKCANDIDATESELECTOR_H_ */
