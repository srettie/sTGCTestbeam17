#include "TriggerSkimmer.h"
#include "TH1F.h"
#include <iostream>
#include <iomanip>
#include <algorithm>

TriggerSkimmer::TriggerSkimmer(TTree* t):Readout(t),
    m_NVMM(8),
    m_NELINKS(8),
    m_TRGELINK(7),
    m_TRGVMM(7),
    m_TRGCHAN(23),
    m_MAXTRGPDO(200),
    m_NCHANSVMM(64){
}

TriggerSkimmer::~TriggerSkimmer(){
    for(const auto& k : m_diff_histos){
        delete k.second;
    }
}

void TriggerSkimmer::setOutput(std::string s){
    m_ofile=s;
}

void TriggerSkimmer::Run(){
  //if (!fChain) return;
  std::cout<<"Trigger ELINK = "<<m_TRGELINK<<std::endl;
  std::cout<<"Trigger VMM = "<<m_TRGVMM<<std::endl;
  std::cout<<"Trigger CH = "<<m_TRGCHAN<<std::endl;
  for(int i_elink=0;i_elink<m_NELINKS;i_elink++ ){
    for(int i_vmm=0;i_vmm<m_NVMM;i_vmm++){
      for(unsigned int i_chan=0;i_chan<m_NCHANSVMM;i_chan++){
	auto name=TH1Name(i_elink, i_vmm,i_chan);
	// Bin range must be 4096 (12-bit clock)
	m_diff_histos.insert(std::make_pair(name,new TH1F(name.c_str(),name.c_str(),4096,0,4096)));
	m_TH1Keys.emplace_back(name);
      }
    }
  }
  TFile *newfile = new TFile(m_ofile.c_str(),"recreate");
  newfile->cd();

  TTree *newtree = fChain->CloneTree(0); 
  Long64_t nentries = fChain->GetEntriesFast();
  std::cout<<"Found "<<nentries<<" entries"<<std::endl;
  Long64_t nbytes = 0, nb = 0;
  Long64_t newentries = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;
    if(jentry%10000==0) std::cout<<"Processing Entry : "<<jentry<<std::endl;
    //std::cout<<"\nNew event with "<<m_elink_id->size()<<" hits (elink\tvmm\tchan\tpdo)"<<std::endl;
    bool trig = false;
    int bcid_trig = 0;
    for (int i_hit = 0; i_hit < m_elink_id->size(); i_hit++){
      //std::cout<<(*m_elink_id)[i_hit]<<"\t"<<(*m_vmm_id)[i_hit]<<"\t"<<(*m_chan)[i_hit]<<"\t"<<(*m_pdo)[i_hit]<<std::endl;
      if ((*m_elink_id)[i_hit] != m_TRGELINK) continue;
      if ((*m_vmm_id)[i_hit] != m_TRGVMM) continue;
      if ((*m_chan)[i_hit] != m_TRGCHAN) continue;
      if ((*m_pdo)[i_hit] < m_MAXTRGPDO) continue;
      trig = true;
      bcid_trig = (*m_bcid)[i_hit];
    }
    if (!trig) continue; // go to the next event
    for (int i_hit = 0; i_hit < m_elink_id->size(); i_hit++){
      double diff = (*m_bcid)[i_hit] - bcid_trig;
      int elink =  (*m_elink_id)[i_hit];
      int vmm =  (*m_vmm_id)[i_hit];
      int ichan=(*m_chan)[i_hit];
      m_diff_histos[TH1Name(elink, vmm,ichan)]->Fill(diff);
    }
    newtree->Fill();
    newentries++;
  }

  std::cout << std::setprecision(2) << std::fixed;
  std::cout<<"Saved "<<newentries<<" entries; lost "<<100.0*(1-float(newentries)/float(nentries))<<"% due to missing trigger signal"<<std::endl;
   
  //newtree->Print();
  //sort histograms so that it will be easier to navigate through the rootfile, otherwise it's a mess
  std::sort(m_TH1Keys.begin(),m_TH1Keys.end());
  for(const auto& k : m_TH1Keys){
    if(m_diff_histos[k]->GetEntries()>2) m_diff_histos[k]->Write();
  }
  newtree->AutoSave();
  delete newfile;
}



std::string TriggerSkimmer::TH1Name(int elink, int vmm, int chan)
{
  return std::string(TString::Format("elink_%d_vmm_%d_chan_%d_diff_bcid", elink, vmm,chan).Data());
}
