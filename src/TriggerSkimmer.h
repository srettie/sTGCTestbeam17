#ifndef TRIGGERSKIMMER_H
#define TRIGGERSKIMMER_H

#include "Readout.h"
#include <unordered_map>
#include "TString.h"
#include <string>



class TH1F;

class TriggerSkimmer : public Readout{
  
 public:
  TriggerSkimmer(TTree *tree=0);
  virtual ~TriggerSkimmer();
  //S.I
  //Create diff histograms
  void setOutput(std::string);
  void setNVMM(int i){m_NVMM=i;}
  void setNELINKS(int i){m_NELINKS=i;}
  void setTRGELINK(int i){m_TRGELINK=i;}
  void setTRGVMM(int i){m_TRGVMM=i;}
  void setTRGCHAN(int i){m_TRGCHAN=i;}
  void setMAXTRGPDO(int i){m_MAXTRGPDO=i;}
  void setNCHANSVMM(int i){m_NCHANSVMM=i;}
        
  void Run();
 protected:
  std::string m_ofile="trgSkimmerDefault.root";
  std::string TH1Name(int elink,int vmm,int chan);
  std::unordered_map<std::string,TH1F*> m_diff_histos;
  std::vector<std::string> m_TH1Keys;
        
  int m_NVMM;
  int m_NELINKS;
  int m_TRGELINK;
  int m_TRGVMM;
  int m_TRGCHAN;
  int m_MAXTRGPDO;
  int m_NCHANSVMM;
  
};


#endif
