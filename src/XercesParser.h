#ifndef XercesParser_H
#define XercesParser_H

#include <string>
#include "ExpressionEvaluator.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLException.hpp"

#include <xercesc/dom/DOM.hpp>

#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>

#include <xercesc/util/OutOfMemoryException.hpp>

class XercesParser {
public:
	XercesParser();
	XercesParser(std::string);
	~XercesParser();
	bool ParseFile(std::string);
	bool ParseFileAndNavigate(std::string);
	bool ParseString(std::string);
	bool ParseStringAndNavigate(std::string);
	bool WriteToFile(std::string);
	void navigateTree();
	static void elementLoop();
	static void elementLoop(xercesc::DOMNode*);
	static ExpressionEvaluator& Evaluator();
	static xercesc::DOMNode* GetCurrentElement() {return s_currentElement;}
	bool Initialize();
	bool Finalize();
private:
    xercesc::DOMDocument *m_doc;
	
	xercesc::XercesDOMParser *m_parser;
	bool m_initialized;
    std::string m_fileName;
protected:
	static xercesc::DOMNode *s_currentElement;
};
#endif
