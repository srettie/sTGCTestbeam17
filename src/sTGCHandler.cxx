/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "sTGCHandler.h"
#include <iostream>

sTGCHandler::sTGCHandler(std::string s):XMLHandler(s)
{
}

const std::vector<std::unique_ptr<sTGCParameters>>&  sTGCHandler::parameters(){
    return m_parameters;
}

void sTGCHandler::ElementHandle()
{
    
//    std::cout<<" this is sTGCHandler::Handle"<<std::endl;
    auto par=std::make_unique<sTGCParameters>();
	bool ret=true;
	std::string subType=getAttributeAsString("subType",ret);
    par->subType=subType;
    m_parameters.push_back(std::move(par));
}
