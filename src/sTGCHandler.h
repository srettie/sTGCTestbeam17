/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef sTGCHandler_H
#define sTGCHandler_H

#include "XMLHandler.h"
#include <string>
#include <memory>
#include "sTGCParameters.h"


class sTGCHandler:public XMLHandler {
public:
	sTGCHandler(std::string);
	void ElementHandle();
    const std::vector<std::unique_ptr<sTGCParameters>>&  parameters(); 
private:
    std::vector<std::unique_ptr<sTGCParameters>> m_parameters;
    
};

#endif
