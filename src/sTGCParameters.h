#ifndef STGC_PARAMETERS_H
#define STGC_PARAMETERS_H
#include <vector>

struct sTGCParameters {
    std::string type;
    std::string tech; 
    std::string subType; 
    double sWidth;
    double lWidth;
    double Length; 
    double Tck; 
    double xFrame;
    double ysFrame;
    double ylFrame;
    double yCutout; 
    double yCutoutCathode; 
    double stripPitch; 
    double stripWidth; 
    double wirePitch;
};
#endif
