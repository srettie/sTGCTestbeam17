/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
//S.I modified to fit a local installataion

#include <iostream>
#include <algorithm>
#include "sTGC_readoutHandler.h"


sTGC_readoutHandler::sTGC_readoutHandler(std::string s):XMLHandler(s)
{
}
 const std::vector<std::unique_ptr<sTGCReadoutParameters>>& sTGC_readoutHandler::sTGCRoParams(){
     return m_roParams;
}
void sTGC_readoutHandler::ElementHandle()
{
    bool ret=true;
    
    auto roParameters=std::make_unique<sTGCReadoutParameters>();
    
    roParameters->sPadWidth           = getAttributeAsDouble    ("sPadWidth", ret);

    roParameters->lPadWidth           = getAttributeAsDouble    ("lPadWidth", ret);

    roParameters->sStripWidth           = getAttributeAsDouble    ("sStripWidth", ret);

    roParameters->lStripWidth           = getAttributeAsDouble    ("lStripWidth", ret);

    roParameters->padH                = getAttributeAsVector    ("padH", ret);

    roParameters->nPadPhi         = getAttributeAsIntVector ("nPadPhi", ret);


    roParameters->anglePadPhi            = getAttributeAsDouble    ("anglePadPhi", ret);


    roParameters->firstPadPhiDivision_A = getAttributeAsVector    ("firstPadPhiDivision_A", ret);
    roParameters->firstPadPhiDivision_C         = getAttributeAsVector    ("firstPadPhiDivision_C", ret);

      roParameters->PadPhiShift_A = getAttributeAsVector    ("PadPhiShift_A", ret);
      roParameters->PadPhiShift_C = getAttributeAsVector    ("PadPhiShift_C", ret);


    roParameters->nPadH               = getAttributeAsVector    ("nPadH", ret);

    roParameters->firstPadH           = getAttributeAsVector    ("firstPadH", ret);

    roParameters->firstPadRow         = getAttributeAsIntVector ("firstPadRow", ret);

    roParameters->nWires              = getAttributeAsIntVector ("nWires", ret);

    roParameters->wireCutout          = getAttributeAsVector ("wireCutout", ret);

    roParameters->firstWire           = getAttributeAsVector    ("firstWire", ret);

    roParameters->wireGroupWidth      = getAttributeAsDouble    ("wireGroupWidth", ret);

    roParameters->nStrips             = getAttributeAsInt       ("nStrips", ret);

    roParameters->firstTriggerBand    = getAttributeAsIntVector ("firstTriggerBand", ret);

    roParameters->nTriggerBands       = getAttributeAsIntVector ("nTriggerBands", ret);

    roParameters->firstStripInTrigger = getAttributeAsIntVector ("firstStripInTrigger", ret);

    roParameters->firstStripWidth     = getAttributeAsVector    ("firstStripWidth", ret);

    roParameters->StripsInBandsLayer1 = getAttributeAsIntVector ("StripsInBandsLayer1", ret);

    roParameters->StripsInBandsLayer2 = getAttributeAsIntVector ("StripsInBandsLayer2", ret);

    roParameters->StripsInBandsLayer3 = getAttributeAsIntVector ("StripsInBandsLayer3", ret);

    roParameters->StripsInBandsLayer4 = getAttributeAsIntVector ("StripsInBandsLayer4", ret);

    roParameters->nWireGroups         = getAttributeAsIntVector ("nWireGroups", ret);

    roParameters->firstWireGroup      = getAttributeAsIntVector ("firstWireGroup", ret);
    
    
    m_roParams.push_back(std::move(roParameters));
    
    
}
