/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef sTGC_readoutHandler_H
#define sTGC_readoutHandler_H
#include "XMLHandler.h"
#include <memory>
#include "sTGCReadoutParameters.h"
#include <vector>

class sTGC_readoutHandler:public XMLHandler {
 public:
	sTGC_readoutHandler(std::string);
	void ElementHandle();
    const std::vector<std::unique_ptr<sTGCReadoutParameters>>& sTGCRoParams();
private:
    std::vector<std::unique_ptr<sTGCReadoutParameters>> m_roParams;
};

#endif
