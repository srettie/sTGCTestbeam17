/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "varHandler.h"
#include <iostream>


varHandler::varHandler(std::string s):XMLHandler(s)
{
}

void varHandler::ElementHandle()
{
    

	bool ret=true;
    std::string name=getAttributeAsString("name",ret);
    Evaluator().RegisterConstant(name,getAttributeAsDouble("value"));
    
    if (name=="NSW_sTGC_Tck"){
        std::cout<<"NSW_sTGC_Tck="<<getAttributeAsDouble("value")<<std::endl;
    }
    if(name=="NSW_sTGC_pcbTck"){
        std::cout<<"NSW_sTGC_pcbTck="<<getAttributeAsDouble("value")<<std::endl;
    }
    
}
