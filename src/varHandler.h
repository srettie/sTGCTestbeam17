/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef varhandler_H
#define varhandler_H

#include "XMLHandler.h"
#include <string>

class varHandler:public XMLHandler {
public:
	varHandler(std::string);
	void ElementHandle();

};

#endif
