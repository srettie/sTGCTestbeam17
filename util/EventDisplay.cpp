/*
 * Gui_example.cpp
 *
 *  Created on: Oct 19, 2017
 *      Author: srettie & lucamoleri
 */

#include <TApplication.h>
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGTab.h>
#include <TTree.h>
#include <TRootEmbeddedCanvas.h>
#include "TrackCandidateSelector.h"
#include "MyMainFrame.h"
#include "Hit.h"
#include "HitManager.h"
#include "RecoManager.h"
#include "DetectorComponent.h"
#include "QuadrupletGeometryProviderBuilder.h"
#include "TGFrame.h"

int main(int argc, char **argv) {
  std::cout<<"Starting EventDisplay..."<<std::endl;

  if(argc!=2) {
    std::cerr<<"Commandline Error. Usage:"<<std::endl;
    std::cerr<<argv[0]<<" <inputfile.root>"<<std::endl;
    exit(8);
  }

  std::ifstream f(argv[1]); 
  if(!f.good()){
    std::cerr<<"Something wrong with the file "<<argv[1]<<std::endl;
    exit(8);
  }

  std::string fname_in = std::string(argv[1]);
  TApplication theApp("App",&argc,argv);
  new MyMainFrame(gClient->GetRoot(),500,500,fname_in.c_str());
  theApp.Run();

  return 0;
}

