#include "TestBeam_Tracker.h"
#include <string>
#include <fstream>

#include "TestBeamConfigHandler.h"
#include "XercesParser.h"
#include "XMLHandlerStore.h"

int main(int argc, char* argv[]){
  std::cout<<"Starting TestBeam_Tracker..."<<std::endl;
  if(argc<2) {
    std::cerr<<"Commandline Error. Usage:"<<std::endl;
    std::cerr<<argv[0]<<" <inputfiles.root>"<<std::endl;
    exit(8);
  }

  // Setup XML parser
  XMLHandlerStore *hs=XMLHandlerStore::GetHandlerStore();
  TestBeamConfigHandler* tbch= new TestBeamConfigHandler("TestBeamTrigConfig");
  XercesParser xp;
  xp.ParseFileAndNavigate("xml/testBeamConfig18.xml");

  std::cout<<"Running over "<<(argc-1)<<" files:"<<std::endl;
  for(int i = 1; i < argc; i++)
    std::cout<<argv[i]<<std::endl;
  
  for(int i = 1; i < argc; i++){
    std::cout<<"Running over file "<<argv[i]<<" ("<<i<<"/"<<(argc-1)<<")"<<std::endl;
    std::ifstream f(argv[i]); 
    if(!f.good()){
      std::cerr<<"Something wrong with the file "<<argv[i]<<std::endl;
      exit(8);
    }
    TestBeam_Tracker(tbch, std::string(argv[i]));
  }
  return 0;
}
