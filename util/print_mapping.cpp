#include "MappingPrinter.h"
#include <string>
#include <fstream>
int main(int argc, char* argv[]){
  std::cout<<"Starting MappingPrinter..."<<std::endl;
  
  if(argc>1) {
    std::cerr<<"Commandline Error. Usage:"<<std::endl;
    std::cerr<<"Choose quadruplet type in XML input and run "<<argv[0]<<std::endl;
     exit(8);
  }
    
  MappingPrinter();
  return 0;
}
