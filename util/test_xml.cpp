#include "XercesParser.h"
#include "XMLHandlerStore.h"
#include "sTGC_readoutHandler.h"
#include "sTGCHandler.h"
#include "varHandler.h"
#include <iostream>
#include "sTGCReadoutParameters.h"
//DetectorDescription/AGDD/AGDD2GeoSvc/src/AGDD2GeoSvc.cxx
//add handlers here
//MuonSpectrometer/MuonDetDescr/MuonAGDDBase/src/sTGC_readoutHandler.cxx
//for stgc

//also take a look at MuonSpectrometer/MuonDetDescr/MuonAGDD/src/MuonAGDDToolHelper.cxx




int main(){

    XMLHandlerStore *hs=XMLHandlerStore::GetHandlerStore();//Singleton
    //stgc handler is automatically registered to the store once created
    
    varHandler *varhandler=new varHandler("var");
    sTGCHandler* stgchandler=new sTGCHandler("sTGC");
    sTGC_readoutHandler *stgcrohandle=new sTGC_readoutHandler("sTGC_readout");

    XercesParser p;//this parser will have access to the XMLHandlerStore just created.

    p.ParseFileAndNavigate("xml/stations-v0.2.xml");
    varhandler->ElementHandle();
    stgchandler->ElementHandle();
    stgcrohandle->ElementHandle();
    //QS1P QS2P QS3P QS1C QS2C QS3C QL1P QL2P QL3P QL1C QL2C QL3C
    std::cout<<"npadPhi************************"<<std::endl;
    for(const auto& k :stgcrohandle->sTGCRoParams() ){
       for(const auto& p : k->nPadPhi) std::cout<<p;
       std::cout<<std::endl;
    }
    std::cout<<"nStrips************************"<<std::endl;
    for(const auto& k :stgcrohandle->sTGCRoParams() ){
       std::cout<<k->nStrips<<std::endl;
    }
    std::cout<<"PadPhiShift_A ********"<<std::endl;
    for(const auto& k :stgcrohandle->sTGCRoParams() ){
       for(const auto& p : k->PadPhiShift_A) std::cout<<p;
       std::cout<<std::endl;
    }    
    std::cout<<"firstPadH ********"<<std::endl;
    for(const auto& k :stgcrohandle->sTGCRoParams() ){
       for(const auto& p : k->firstPadH) std::cout<<p;
       std::cout<<std::endl;
    }
  
  std::cout<<"QUADRUPLETS ********"<<std::endl;
    for(const auto& k :stgchandler->parameters() ){
        std::cout<<k->subType;
       std::cout<<std::endl;
    }    
  
  
    //read attributes of stgcro handler

    return 0;
}
