#include "TriggerSkimmer.h"
#include <iostream>
#include <fstream>
#include "XercesParser.h"
#include "XMLHandlerStore.h"
#include "TestBeamConfigHandler.h"



int main(int argc,char* argv[]){
    
    if(argc!=2){
        std::cerr<<"Commandline Error. Usage:"<<std::endl;
        std::cout<<argv[0]<<" <inputfile>"<<std::endl;
        exit(8);
    }
    
    std::ifstream i(argv[1]);
    if(!i.good()){
        std::cerr<<"Something wrong with the file "<<argv[1]<<std::endl;
        exit(8);
    }
    
    TChain *c=new TChain("decoded_event_tree");
    
    
    c->Add(argv[1]);

    std::string in_filename = std::string(argv[1]);
    
    TriggerSkimmer ts(c);
    
    XMLHandlerStore *hs=XMLHandlerStore::GetHandlerStore();
    auto tbch=new TestBeamConfigHandler("TestBeamTrigConfig");
    XercesParser xp;
    xp.ParseFileAndNavigate("xml/testBeamConfig18.xml");
    ts.setNVMM(tbch->par("NVMM"));    
    ts.setNELINKS(tbch->par("NELINKS"));
    ts.setTRGELINK(tbch->par("TRGELINK"));
    ts.setTRGVMM(tbch->par("TRGVMM"));
    ts.setTRGCHAN(tbch->par("TRGCHAN"));
    ts.setMAXTRGPDO(tbch->par("MAXTRGPDO"));
    ts.setNCHANSVMM(tbch->par("NCHANSVMM"));

    
    std::string out_filename = in_filename.replace(in_filename.find(".root"), 5, "_skimmed.root");
    
    ts.setOutput(out_filename);
    
    ts.Run();
    
    delete tbch;
    delete c;
    
    
    return 0;
}
